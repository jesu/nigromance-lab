var hPos = 1;		// Elevator displacement for the current frame of time
var time = 0;		// Time counter for the move step size increment
var posStop = new Array(40,55,16);
var posInd = 0;
var stop = false;

function nextFrame(){

	if ( !stop){

		// Updating aceleration
		if ( hPos < 10 ){
			if ( time >= 100 ){
				time = 0;
				hPos ++;
			}else{
				time += 20;
			}
		}
		postMessage(hPos);
		setTimeout("nextFrame()",20);

	// DAMPING SIMULATION
	}else{
		if ( posInd < 3 ){

			if ( posStop[posInd] <= 0 ){
				hPos *= (-1);
				posInd++;
			}
			posStop[posInd] -= Math.abs(hPos);
			
			postMessage(hPos);

			if ( posInd > 0){
				hPos++;
			}
			setTimeout("nextFrame()",20);

		}else{

			close();
		}
	}

	
}

onmessage = function(e){
	if  (e.data === 'start') {
		nextFrame();
	}else if (e.data === 'stop'){
		stop = true;
	}else{
		self.close();
	}
}