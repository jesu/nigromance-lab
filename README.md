## Synopsis

This is a HTML5 One Page Arquitecture, experiment of experiments container of Nigromance labs. It´s not very usefull but some funny stuff could be found inside.
Basicly there is a main frame with the page content. This content changes dynamically and is the same for every page.
A frame fill the user screen like the wall you see into an elevator, while the next page is loading.
Every page is used to show Nigromance´s works so don´t worry if you find no sense stuff. 

Libraries:
	- required.js
	- handlebars.js

## Motivation

This project was made to show what things could Nigromance invent, design and develop.

## Contributors

Nigromance - www.nigromance.com 
info@nigromance.com

## License
MIT License.