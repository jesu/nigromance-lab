(function videoInit (){

	
	var TV = function(){
		var self = this;
		this.press = false;
		var initPos = {x:0,y:0};
		var H=0, W=0;

		this.color = {	press:false,
						angle:90,
						value:0,
						element:null
					};
		this.contrast = {	press:false,
							angle:90,
							value:0,
							element:null
						};
		this.bright = {	press:false,
						angle:90,
						value:0,
						element:null
					};	
		this.vhf = {	press:false,
						angle:90,
						value:0,
						element:null
					};				
		this.vu = {	press:false,
					value:38,
					min:24,
					max:42,
					h:0,
					element:null
					};
		
		this.clickDown = function(e){
			
			var element = trigger(e);		
			
			e.preventDefault();

			initPos = mouseCoord(e);

			self.press = true;
			
			switch ( element.id ){
				
				case "color":
					self.color.press = true;
					break;
					
				case "contrast":
					self.contrast.press = true;
					break;
					
				case "bright":
					self.bright.press = true;
					break;	
				case "vhf":
					self.vhf.press = true;
					break;	
				case "Vu":
					self.vu.press = true;
					break;		
			}
		}
		
		this.clickUp = function(){

			if( self.press ){
				self.press = false;
				self.bright.press = false;
				self.color.press = false;
				self.contrast.press = false;
				self.vhf.press = false;
				self.vu.press = false;	
			}
		}
		
		this.updateValues = function(e){
			var actPos = mouseCoord(e);
			var deltaX = actPos.x - initPos.x;
			var deltaY = initPos.y - actPos.y;	

			if ( self.vu.press ){

				// Transform % into px measure for Vu slice
				if ( self.vu.h === 0 ){
					self.vu.h = self.H*0.18;
					self.vu.value = self.H*self.vu.value/100;
					self.vu.max = self.H*self.vu.max/100;
					self.vu.min = self.H*self.vu.min/100;
				}

				//Updating
				var value = self.vu.value + deltaY;

				//Clipping
				if ( value < self.vu.min ) value = self.vu.min;			
				if ( value > self.vu.max ) value = self.vu.max;
				
				self.vu.value = value;	

				//Moving Fader	
				self.vu.element.style.bottom = value+"px";

				video.volume = (value-self.vu.min)/self.vu.h;

				initPos.y = actPos.y;
			}else{
			
				var angle = Math.round(Math.atan2(deltaX,deltaY)*180/Math.PI);
				
				if ( Math.abs(angle)<130 ){
					
					if ( self.color.press ){
						self.color.angle = angle;
						self.color.element.style.transform = "rotateZ("+(angle)+"deg)";
						self.color.element.style.webkitTransform = "rotateZ("+(angle)+"deg)";
						colorCoef = (angle+130)/130;
					}
					if ( self.contrast.press ){
						self.contrast.angle = angle;
						self.contrast.element.style.transform = "rotateZ("+(angle)+"deg)";
						self.contrast.element.style.webkitTransform = "rotateZ("+(angle)+"deg)";
						contrastCoef = (angle+130)/130;
					}
					if ( self.bright.press ){
						self.bright.angle = angle;
						self.bright.element.style.transform = "rotateZ("+(angle)+"deg)";
						self.bright.element.style.webkitTransform = "rotateZ("+(angle)+"deg)";
						brightCoef = angle;
					}
					if ( self.vhf.press ){
						self.vhf.angle = angle;
						self.vhf.element.style.transform = "rotateZ("+(angle)+"deg)";
						self.vhf.element.style.webkitTransform = "rotateZ("+(angle)+"deg)";
						vhfCoef = angle;
						noiseCoef = angle/130;
					}
				}
			
			}
		}
	}
			

	var VHS = function(){
		
		this.led = {
				on: false
			};		
		this.powerBtn = {
				imageOn: new Image(),
				imageOff: new Image(),
				state: "off", //"on" "pressed"
				loadedOn: false,
				loadedOff: false
			};
		var rewBtn = {
				pressed: false
			};
		var playBtn = {
				pressed: false
			};	
		var ffBtn = {
				pressed: false
			};
		var pauseBtn = {
				pressed: false
			};
		var stopBtn = {
				pressed: false
			};
		var slowBtn = {
				pressed: false
			};
		

		this.decMin,this.uniMin,this.decSec,this.uniSec;
			
		this.rewTxt = new Image();
		this.playTxt = new Image();
		this.ffTxt = new Image();
		this.pauseTxt = new Image();
		this.stopTxt = new Image();
		this.slowTxt = new Image();
										
		this.screenMsg = null;	
			
		var self = this;	
		this.videoPlayed = false;
	
		
		this.powerUp = function(){
			document.getElementById("led_on").style.display = "block";
			setTimeout(function (){document.getElementById("pantalla_vhs").style.display = 'block';},1000);
		};
		
		this.shutDown = function(){
			document.getElementById("power_btn").src = self.powerBtn.imageOff.src;
			document.getElementById("power_btn").style.display = "block";
			document.getElementById("pantalla_vhs").style.display = "none";
			setTimeout(function (){document.getElementById("led_on").style.display = "none";},1000);
		};

		this.updateTime = function(){
						
			var digitHeight = parseInt(document.getElementById("minDec").offsetHeight)/10;
			var nCurrentTime = Math.round(video.currentTime);
			var nMinutes = Math.floor(nCurrentTime/60);
			var nSeconds = nCurrentTime%60;
			var nDecMin = (Math.floor(nMinutes/10)*digitHeight+1).toString();
			var nUniMin = ((nMinutes%10)*digitHeight+1).toString();
			var nDecSec = (Math.floor(nSeconds/10)*digitHeight+1).toString();
			var nUniSec = ((nSeconds%10)*digitHeight+1).toString();

			self.decMin.style.top = "-"+nDecMin+"px";
			self.uniMin.style.top = "-"+nUniMin+"px";
			self.decSec.style.top = "-"+nDecSec+"px";
			self.uniSec.style.top = "-"+nUniSec+"px";
		}

		this.stop = function(){
			self.screenMsg.src = self.stopTxt.src;
			setTimeout(function(){self.screenMsg.style.display = "none";},1000);
			video.pause();
			self.videoPlayed = false;
		}
		
		this.clickDown = function(e){
			
			var element = trigger(e);
			//console.log("Elemento: "+element.id);
			if(element.id == 'power_btn'){
				element.style.display = "none";
				
				if ( self.powerBtn.state=="off" ){	
					element.src = self.powerBtn.imageOn.src;
					self.powerBtn.state="pressed-on";	
					self.powerUp();
				}else{		
					element.src = self.powerBtn.imageOff.src;	
					self.powerBtn.state = "pressed";
				}
					
			}else if ( self.powerBtn.state=="on" ){
				
				switch ( element.id ){
					case "REW_btn":

						if ( video.playbackRate > -8  && self.videoPlayed){
							self.screenMsg.src = self.rewTxt.src;
							if ( video.playbackRate == 1)
								video.playbackRate = -2;
							else	
								video.playbackRate *= 2;
						}
						break;
							
					case "play_btn":
						self.screenMsg.src = self.playTxt.src;
						self.screenMsg.style.display = "block";
						video.playbackRate = 1;
						self.videoPlayed = true;
						startPlaying();
						break;
							
					case "FF_btn":
						if ( video.playbackRate < 8 && self.videoPlayed){
							self.screenMsg.src = self.ffTxt.src;
							video.playbackRate *= 2;
						}
						break;	
						
					case "pause_btn":
						if ( self.videoPlayed ){
							self.screenMsg.src = self.pauseTxt.src;
							video.pause();
						}
						break;	
						
					case "stop_btn":
						if ( self.videoPlayed ){
							self.stop();
						}
						break;	
						
					case "slow_btn":
						if ( self.videoPlayed ){
							self.screenMsg.src = self.slowTxt.src;
							if ( video.playbackRate == 0.2 )
								video.playbackRate = 1;
							else
								video.playbackRate = 0.2;	
						}
						break;									
				}
			}
			
		};

	}
	
		var	video,
			videoCanvas,
			videoCtx,
			contrastCoef=1,
			colorCoef=1,
			brightCoef=0,
			vhfCoef = 0,
			noiseCoef = 0; // 0 - 1
			

		function pixelProcess(){
				var nR = 0.299;
				var nG = 0.587;
				var nB = 0.114;
				var R,G,B;
				var data,width,height,average,imageData;
				
				imageData = window.ctxOffset.getImageData(0,0,videoCanvas.width,videoCanvas.height);
				data = imageData.data;
				width = data.width;
				
				for(i=0;i< data.length-4; i+=4){
					//Bright Maths
					if ( colorCoef != 0 ){
						R = data[i] + brightCoef;
						G = data[i+1] + brightCoef;
						B = data[i+2] + brightCoef;
					}
					
					// Color Maths
					if ( colorCoef != 1 ){
						var average = (nR*data[i] + nG*data[i+1] + nB*data[i+2]);
						R = (R - average)*colorCoef + average;
						G = (G - average)*colorCoef + average;
						B = (B - average)*colorCoef + average;
					}	
					
					// Contrast Maths
					if ( contrastCoef != 1 ){
						R = (R - 128)*contrastCoef + 128;
						G = (G - 128)*contrastCoef + 128;
						B = (B - 128)*contrastCoef + 128;
					}
					
					if ( noiseCoef != 0 || vhs.powerBtn.state == "off"){
						
						var noiseVal = Math.round(Math.random()*255);
						var noiseLevel = noiseCoef;
						if ( vhs.powerBtn.state == "off" ) noiseLevel = 1;
						
						if( noiseLevel === 1){
							R = noiseVal;
							G = noiseVal;
							B = noiseVal;

						}else{
							R = R*(1-noiseLevel)+noiseVal*noiseLevel;
							G = G*(1-noiseLevel)+noiseVal*noiseLevel;
							B = B*(1-noiseLevel)+noiseVal*noiseLevel;
						}
					}
					
					if ( R < 0 ) R = 0;
					if ( R > 255 ) R = 255;
					
					if ( G < 0 ) G = 0;
					if ( G > 255 ) G = 255;
					
					if ( B < 0 ) B = 0;
					if ( B > 255 ) B = 255;
					
					data[i] = R;
					data[i+1] = G;
					data[i+2] = B;
			
				}
				window.ctxOffset.putImageData(imageData,0,0);
			}
	
			
			function startPlaying(){
				video.play();
				
			}

			
			
			var vhs = new VHS();
			var tv = new TV();
			
			function clickUpAct(e){
				
			 	if ( vhs.powerBtn.state=="pressed-on" ){
					document.getElementById("power_btn").style.display = "block";	
					vhs.led.on = true;
					vhs.powerBtn.state="on";
				}else if ( vhs.powerBtn.state=="pressed" ){
					vhs.shutDown();	
					vhs.powerBtn.state="off";
				}else{
					tv.clickUp();
				}
				
			}
			
			function updateValues(e){
				if ( tv.press ){
					tv.updateValues(e);	
				}
			}


		function selectMovie(e){
			var element = trigger(e);
			var movieSel = element.id;

			var numTapes = element.parentNode.childNodes.length;
			var tapes = element.parentNode.childNodes;


			if ( element.className.indexOf( 'selected') === (-1) ){

				for (var i=0; i<numTapes; i++){
					tapes[i].className = tapes[i].className.replace(' selected','');
				}

				element.className += ' selected';
				console.log(element.className);
			}

			document.getElementById('videosrc_ogg').src = 'media/'+movieSel+'.ogv';
			document.getElementById('videosrc_webm').src = 'media/'+movieSel+'.webm';
			document.getElementById('videosrc_mp4').src = 'media/'+movieSel+'.mp4';

			video.load();
			
		}


		function getImages(){

			window.nextBackgImg = window.imageLoader.getImage(0).image;
			vhs.playTxt =  window.imageLoader.getImage(17).image;
			vhs.rewTxt =  window.imageLoader.getImage(21).image;
			vhs.ffTxt =  window.imageLoader.getImage(13).image;
			vhs.pauseTxt =  window.imageLoader.getImage(15).image;
			vhs.stopTxt =  window.imageLoader.getImage(25).image;
			vhs.slowTxt =  window.imageLoader.getImage(23).image;

			vhs.powerBtn.imageOn = window.imageLoader.getImage(20).image;
			vhs.powerBtn.imageOn.onload = function(){ vhs.powerBtn.loadedOn = true}; 	
		
			vhs.powerBtn.imageOff = window.imageLoader.getImage(19).image;
			vhs.powerBtn.imageOff.onload = function(){ vhs.powerBtn.loadedOff = true}; 

			// Adding the menu if there isn´t any
			if ( !document.getElementById("menu_container") ){
				addMenu();	
			}	
		}

		function drawVideo(){
			
			if ( window.pageContent.speed !== 0 ) {
				document.getElementById("video_element_container").style.top = (window.pageContent.pageOffset+180)+"px";
				document.getElementById("sky").style.top = (window.pageContent.pageOffset*(-0.5)+100)+"px";
				document.getElementById("building_1").style.top = (window.pageContent.pageOffset*(-0.1)+50)+"px";
				document.getElementById("building_2").style.top = (window.pageContent.pageOffset*(-0.2)+160)+"px";
				document.getElementById("building_3").style.top = (window.pageContent.pageOffset*(-0.3)+120)+"px";
				document.getElementById("building_4").style.top = (window.pageContent.pageOffset*(-0.4)+220)+"px";
			}
			window.ctxBackg.clearRect(0,0,window.canvasBackg.width,window.canvasBackg.height);
			window.ctxBackg.drawImage(window.backgImg.image,window.backgImg.X+0.5,0,window.backgImg.W,window.backgImg.H);

			if ( videoCanvas ){
				window.ctxOffset.drawImage(video,0,0,videoCanvas.width,videoCanvas.height);
						
				if ( vhs.powerBtn.state != "off" && !vhs.videoPlayed ){

					
					videoCtx.save();
						videoCtx.fillStyle = "rgba(0,0,255,255)";
						videoCtx.fillRect(0,0,videoCanvas.width,videoCanvas.height);
					videoCtx.restore();
						
				}else{	
					pixelProcess();
					videoCtx.drawImage(window.canvasOffset,0,0,canvasBackg.width,canvasBackg.height);
					//console.log("Velocidad de reproducción: "+video.playbackRate);
				}
			}
		}

		function initNavPageVideo(){
			videoCanvas = document.getElementById("videoCanvas");
			videoCtx = videoCanvas.getContext('2d');

			video = document.getElementById("mivideo");
			addEvent(video,'timeupdate',vhs.updateTime,false);
			addEvent(video,'ended',vhs.stop,false);
			
			var div_VHS = document.getElementById("vhsPlayer");
			addEvent(div_VHS,'mousedown',vhs.clickDown,false);
			
			var div_TV = document.getElementById("tv_container");
			addEvent(div_TV,'mousedown',tv.clickDown,false);
			
			addEvent(document,'mouseup',clickUpAct,false);
			addEvent(document,'mousemove',updateValues,false);
			

			addEvent(document.getElementById('tape_stack'),'click',selectMovie,false);

			tv.bright.element = document.getElementById("bright");
			tv.color.element = document.getElementById("color");
			tv.contrast.element = document.getElementById("contrast");
			tv.vhf.element = document.getElementById("vhf");
			tv.vu.element = document.getElementById("Vu");	
			tv.H = div_TV.offsetHeight;
			tv.W = div_TV.offsetWidth;		
			vhs.screenMsg = document.getElementById("screenTxt");

			vhs.decMin = document.getElementById('minDec');
			vhs.uniMin = document.getElementById('minUni');
			vhs.decSec = document.getElementById('secDec');
			vhs.uniSec = document.getElementById('secUni');

			videoCanvas.height = tv.H*0.61; 
			videoCanvas.width = tv.W*0.54;
		}

		window.imageLoader.resetQueue();

			window.imageLoader.addImage("background","images/video/background_video.png");
			window.imageLoader.addImage("sky","images/video/sky.jpg");
			window.imageLoader.addImage("building_1","images/video/building_1.png");
			window.imageLoader.addImage("building_2","images/video/building_2.png");
			window.imageLoader.addImage("building_3","images/video/building_3.png");
			window.imageLoader.addImage("building_4","images/video/building_4.png");
			window.imageLoader.addImage("tv","images/video/tv.png");
			window.imageLoader.addImage("video","images/video/video.png");
			window.imageLoader.addImage("digits","images/video/digits.png");
			window.imageLoader.addImage("pantalla_on","images/video/pantalla_on.png");
			window.imageLoader.addImage("led_on","images/video/led_on.png");
			window.imageLoader.addImage("knob_line","images/video/knob_line.png");
			window.imageLoader.addImage("FF_btn","images/video/FF_btn.png");
			window.imageLoader.addImage("FF","images/video/FF.png");
			window.imageLoader.addImage("digSep","images/video/digSep.png");
			window.imageLoader.addImage("PAUSE","images/video/PAUSE.png");
			window.imageLoader.addImage("pause_btn","images/video/pause_btn.png");
			window.imageLoader.addImage("PLAY","images/video/PLAY.png");
			window.imageLoader.addImage("play_btn","images/video/play_btn.png");
			window.imageLoader.addImage("power_btn_off","images/video/power_btn_off.png");
			window.imageLoader.addImage("power_btn_on","images/video/power_btn_on.png");
			window.imageLoader.addImage("REW","images/video/REW.png");
			window.imageLoader.addImage("REW_btn","images/video/REW_btn.png");
			window.imageLoader.addImage("SLOW","images/video/SLOW.png");
			window.imageLoader.addImage("slow_btn","images/video/slow_btn.png");
			window.imageLoader.addImage("STOP","images/video/STOP.png");
			window.imageLoader.addImage("stop_btn","images/video/stop_btn.png");
			window.imageLoader.addImage("VHF_knob","images/video/VHF_knob.png");
			window.imageLoader.addImage("vu","images/video/vu.png");
			
		window.imageLoader.preload(function(){
			var tv = new TV;
			var vhs = new VHS;

			getImages();
			window.nextDrawFuncPointer = drawVideo;
			window.pageLoaded = true;
			window.initNavPage = initNavPageVideo;
			window.pageHTML = videoTemplate();

			if ( !window.isAnimated ){
				
				window.isAnimated=true;
				window.drawFuncPointer = window.nextDrawFuncPointer;
				window.backgImg.image = window.nextBackgImg;
				window.pageContent.sizeElements(window.screenDim.height);

				window.pageContent.pageElements.innerHTML =window.pageHTML;

				window.animationCenter();

				initNavPageVideo();
			}	
		});
			

	}
	)();
			


		



		

		
