(function galleryInit (){

		function onClickDown(e){
			var absCoord = mouseCoord(e);
			var coord = coordIntoCanvas(canvasBackg,absCoord.x,absCoord.y);

			
			if ( proyector.isPowerClicked(coord.x,coord.y) ){
					proyector.powerToggle();
			}
			if ( proyector.isPalancaClicked(coord.x,coord.y) ){
					proyector.clickPalanca(coord.x,coord.y);
			}

		}

		function onClickUp(e){
			proyector.declick();
		}

		function onMove(e){
			var absCoord = mouseCoord(e);
			var coord = coordIntoCanvas(canvasBackg,absCoord.x,absCoord.y);

			if ( proyector.isPalancaClicked(coord.x,coord.y) || proyector.isPowerClicked(coord.x,coord.y) ){
				if ( !canvasBackg.className.match(" pointer")){
					canvasBackg.className += " pointer";
				}	
				proyector.movePalanca(coord.x,coord.y);

			}else if ( canvasBackg.className.match(" pointer")){
				canvasBackg.className = canvasBackg.className.replace(" pointer","");
			}


		}



		var Proyector = function(){
			this.yOffset = 0;
			var body = {image:{},X:0,Y:0,W:161,H:290};
			var power = {on:false,timer:0,sprite:{},pos:240,X:0,Y:0,W:50,H:50};
			var palanca = {image:{},X:0,Y:0,W:34,H:90,ang:0,OX:0,OY:0,vI:0,vD:0,clicked:false,cX:0,cY:0};
			var cargador = {image_bar:{},image_frame:{},X:0,Y:0,W:310,H:74,M:0,inc:0};
			var thumbs = {image:{},W:75,H:70,M:0,pos:[0,75,150],inc:0};
			var screenP = {image:{},X:0,Y:0,W:878,H:638,proyImage:0,proyX:0,proyY:0,inc:0};
			var currentList = {list:[],ind:0};
			var photoList = []; 
			var errorList = [];
			var self = this;

			this.powerToggle = function(){
				power.on = !power.on;
			}
			this.clickPalanca = function(x,y){
				palanca.cX = x;
				palanca.cY = y;
				palanca.clicked = true;
			}

			this.movePalanca = function(x,y){
				if ( palanca.clicked ){
					var dif = x - palanca.cX;

					if ( dif > 5 ){
						self.declick();
						self.nextPhoto();
					}	
					if ( dif < -5 ){
						self.declick();
						self.prevPhoto();
					}
				}
			}

			this.declick = function(){
				palanca.cX = 0;
				palanca.cY = 0;
				palanca.clicked = false;
			}

			this.setPhotoList = function(list){
				
				// Reset proyector.
				currentList.ind = 0;
				errorList.length = 0;
				photoList.length = 0
				screenP.proyImage = -2;

				// Asign the new photoList
				currentList.list = list;

				// Loading new photos
				this.loadPhotos();
				// get into the loader
				this.nextPhoto();
			}

			this.loadPhotos = function(){
				
				var phLength = photoList.length;

				// If there are still some photos to load
				if ( currentList.list.length > phLength+errorList.length ){
					// Load the next photo
					var newImage = new Image();
	
					newImage.src = "images/photos/"+currentList.list[currentList.ind];

					//EVENTS
					//		OnLoad OK
					addEvent(newImage,'load',function(){

						var coef = newImage.width/newImage.height;
						var obj = {image: newImage,X:0,Y:0,W:0,H:0,tX:0,tY:0,tW:0,tH:0};

						if ( coef > 1.33 ){ //Landscape
							obj.W = screenP.W*0.85;
							obj.H = obj.W/coef;	
							obj.tW = thumbs.W*0.85;
							obj.tH = obj.tW/coef;
						}else{	//Portrait
							obj.H = screenP.H*0.85;
							obj.W = obj.H*coef;
							obj.tH = thumbs.H*0.85;
							obj.tW = obj.tH*coef;
						}
						obj.X = screenP.X + (screenP.W - obj.W)/2;
						obj.Y = screenP.Y + (screenP.H - obj.H)/2;
						obj.tX = (thumbs.W - obj.tW)/2;
						obj.tY = (thumbs.H - obj.tH)/2;

						photoList.push(obj);
						
					},false); 

					//		OnError
					addEvent(newImage,'error', function(){
										console.info('ERROR: loading photo '+currentList.list[currentList.ind])
										errorList.push(currentList.list[currentList.ind]);
										
									},false);
				}
				currentList.ind++;
			}

			this.draw = function(){
				var aScreenP = screenP;
				var aThumbs = thumbs;
				var aCargador = cargador;
				var aBody = body;
				var aPalanca = palanca;
				var aPower = power;

				// Screen
				window.ctxBackg.drawImage(aScreenP.image,aScreenP.X,aScreenP.Y,aScreenP.W,aScreenP.H);


				//Cargador
				window.ctxBackg.drawImage(cargador.image_frame,cargador.M,cargador.Y+self.yOffset,cargador.W,cargador.H);

				for ( i=-1; i<2 ; i++ ){
					if ( photoList[screenP.proyImage+i] !== undefined && photoList[aScreenP.proyImage+i].image.complete){
						var crrntPh = photoList[aScreenP.proyImage+i];

						//Proyection
						ctxBackg.fillRect(aScreenP.X+aScreenP.proyX+aScreenP.W*i,aScreenP.Y,aScreenP.W,aScreenP.H);
						window.ctxBackg.drawImage(crrntPh.image,crrntPh.X+aScreenP.proyX+aScreenP.W*i,crrntPh.Y,crrntPh.W,crrntPh.H);
						
						// Thumbs
						window.ctxBackg.fillRect(aThumbs.pos[(i+1)]+aThumbs.M, aCargador.Y+self.yOffset, aThumbs.W, aThumbs.H);
						window.ctxBackg.drawImage(crrntPh.image, aThumbs.pos[(i+1)]+aThumbs.M+crrntPh.tX, aCargador.Y+crrntPh.tY+self.yOffset, crrntPh.tW, crrntPh.tH);
						window.ctxBackg.drawImage(aThumbs.image, aThumbs.pos[(i+1)]+aThumbs.M, aCargador.Y+self.yOffset, aThumbs.W, aThumbs.H);	
					}
				}

				// Screen boundaries
				ctxBackg.fillRect(0,0,aScreenP.X,aScreenP.W+aScreenP.Y);
				ctxBackg.fillRect(aScreenP.X+aScreenP.W,0,canvasBackg.width,aScreenP.W+aScreenP.Y);

				window.ctxBackg.drawImage(aCargador.image_bar,aCargador.M,aCargador.Y+self.yOffset,aCargador.W,aCargador.H);

				// Body
				window.ctxBackg.drawImage(aBody.image,aBody.X,aBody.Y+self.yOffset,aBody.W,aBody.H);
				// Palanca
				if ( palanca.ang !== 0 ){
					window.ctxBackg.save();
						window.rotateFrom (window.ctxBackg,aPalanca.ang,aPalanca.OX,aPalanca.OY);
						window.ctxBackg.drawImage(aPalanca.image,aPalanca.X,aPalanca.Y+self.yOffset,aPalanca.W,aPalanca.H);
					window.ctxBackg.restore();	
				}else{
					window.ctxBackg.drawImage(aPalanca.image,aPalanca.X,aPalanca.Y+self.yOffset,aPalanca.W,aPalanca.H);
				}
				// Button
				window.ctxBackg.drawImage(aPower.sprite,aPower.pos,0,80,80,aPower.X,aPower.Y+self.yOffset,aPower.W,aPower.H);
			}



			this.isPowerClicked = function(x,y){
				ctxBackg.beginPath();
				ctxBackg.rect(power.X,power.Y,power.W,power.H);
				return ctxBackg.isPointInPath(x,y);
			}

			this.isPalancaClicked = function(x,y){
				var pPalanca = palanca;
				ctxBackg.beginPath();	
				ctxBackg.rect(pPalanca.X,pPalanca.Y,pPalanca.W,pPalanca.H*0.7);
				return ctxBackg.isPointInPath(x,y);
			}	

			this.setImages = function(){
				//Takes the images from the global image loader
				screenP.image = window.imageLoader.getImage(0).image;
				body.image = window.imageLoader.getImage(1).image;	
				power.sprite = window.imageLoader.getImage(3).image;
				palanca.image = window.imageLoader.getImage(2).image;
				cargador.image_bar = window.imageLoader.getImage(4).image;
				cargador.image_frame = window.imageLoader.getImage(5).image;
				thumbs.image = window.imageLoader.getImage(6).image;	
			}

			this.updatePos = function(){
				var BI = {W:window.backgImg.W,H:window.backgImg.H};
				var CB = {W:window.canvasBackg.width};
				
				// Screen
				screenP.W = BI.W*0.46;
				screenP.H = BI.H*0.59;
				screenP.X = (CB.W-screenP.W)/2;
				screenP.Y = BI.H*0.12;
				screenP.inc = screenP.W/20;
				// Cargador
				cargador.W = BI.W*0.28;
				cargador.H = BI.H*0.12;
				cargador.X = (CB.W-cargador.W)/2;
				cargador.Y = BI.H*0.78;
				cargador.M = cargador.X;
				cargador.inc = cargador.W/70;
				// Thumbs
				thumbs.W = cargador.W*0.26;
				thumbs.H = thumbs.W*0.93;
				thumbs.pos = [cargador.X+cargador.W*0.09,cargador.X+cargador.W*0.37,cargador.X+cargador.W*0.65];
				thumbs.inc = cargador.inc;
				// Body
				body.W = BI.W*0.083;
				body.H = BI.H*0.27;
				body.X = (CB.W-body.W)/2;
				body.Y = BI.H*0.73;
				// Palanca
				palanca.W = BI.W*0.026;
				palanca.H = BI.H*0.19;
				palanca.X = (CB.W-palanca.W)/2;
				palanca.Y = BI.H*0.744;
				palanca.OX = palanca.X+palanca.W/2;
				palanca.OY = palanca.Y+palanca.H*0.89;
				// Button
				power.W = BI.W*0.042;
				power.H = power.W;
				power.X = (CB.W-power.H)/2;
				power.Y = BI.H*0.93;
			}

			this.update = function(){
				var pPower = power;
				var pPalanca = palanca;
				var pPhotoList = photoList;

				// POWER / AUTOPLAY
				//Auto disconecting Autoplay at the end of photo list
				if ( pPower.on && (photoList.length-1 === screenP.proyImage) ){
					pPower.on = false;
				} 

				if ( pPower.on ){
					pPower.pos = Math.round((Math.random()*2))*80;
					pPower.timer += window.deltaT;
					//Autoplay;
					if (pPower.timer > 3 ){
						self.nextPhoto();
						pPower.timer = 0;
					}
				}else{
					pPower.pos = 240;
				}

				// NEXT PHOTO
				if ( pPalanca.vD > 0 ){
					pPalanca.ang +=0.05;
					cargador.M += cargador.inc;
					if ( pPalanca.ang > 1 ){
						pPalanca.vD = (-1);
					}
				}
				if ( pPalanca.vD < 0){
					pPalanca.ang -=0.05;
					screenP.proyX -= screenP.inc;
					cargador.M -= cargador.inc;
					thumbs.M -= thumbs.inc;
					if ( pPalanca.ang < 0  ){
						pPalanca.vD = 0;
						screenP.proyX = 0;
						pPalanca.ang = 0;
						cargador.M = cargador.X;
						thumbs.M = 0;
						screenP.proyImage++;

						// Checking photos buffer and load the next photo from the current list.
						if ( (screenP.proyImage+2 >= pPhotoList.length) && (currentList.list.length > pPhotoList.length+errorList.length) ){
							self.loadPhotos();
						}

						// Initial List load need another step
						if( screenP.proyImage < 0 ){
							this.nextPhoto();
						}
					}
				}
				// PREVPHOTO
				if ( pPalanca.vI < 0 ){
					pPalanca.ang -=0.05;
					cargador.M -= cargador.inc;
					if ( pPalanca.ang < (-1) ){
						pPalanca.vI = (1);
					}
				}
				if ( pPalanca.vI > 0){
					pPalanca.ang +=0.05;
					screenP.proyX += screenP.inc;
					cargador.M += cargador.inc;
					thumbs.M += thumbs.inc;
					if ( pPalanca.ang > 0  ){
						pPalanca.vI = 0;
						screenP.proyX = 0;
						pPalanca.ang = 0;
						cargador.M = cargador.X;
						thumbs.M = 0;
						screenP.proyImage--;
					}
				}
			}

			

			this.nextPhoto = function(){
				
				if ( screenP.proyImage < (photoList.length-1) ){
					palanca.vD = 1;
				}
			}

			this.prevPhoto = function(){
				if ( screenP.proyImage > 0 ) {
					palanca.vI = (-1);
				}
			}


		}




		// PAGE INIT
		var proyector = new Proyector();


		function getImages(){

			proyector.setImages();
			proyector.updatePos();

			
			// Adding the menu if there isn´t any
			if ( !document.getElementById("menu_container") ){
				addMenu();	
			}	
		}

		function drawGallery(){
			
			if ( window.pageContent.speed !== 0 ) {
				proyector.yOffset = window.pageContent.pageOffset;

			}
			window.ctxBackg.clearRect(0,0,window.canvasBackg.width,window.canvasBackg.height);


			proyector.update();

			proyector.draw();
			//window.ctxBackg.drawImage(window.backgImg.image,window.backgImg.X+0.5,0,window.backgImg.W,window.backgImg.H);

		}


		function selectList(e){
			var element = trigger(e);
			var lista = parseInt(element.id.replace('list_',''));

			var numSlide = element.parentNode.childNodes.length;
			var slides = element.parentNode.childNodes;


			if ( element.className.indexOf( 'selected') === (-1) ){

				for (var i=0; i<numSlide; i++){
					slides[i].className = slides[i].className.replace(' selected','');
					console.log(slides[i].className);
				}

				element.className += ' selected';
			}

			proyector.setPhotoList(listaDisp[lista]);
			
		}

		function initNavPageGallery(){

			
			addEvent(document,'mousedown',onClickDown,false);
			addEvent(document,'mouseup',onClickUp,false);
			addEvent(document,'mousemove',onMove,false);

			addEvent(document.getElementById("slide_stack_container"),'click',selectList,false);
			
		}

		var ph_list_01 = ['landscape/aranjuez_02.jpg','landscape/av_america.jpg','landscape/cerezo.jpg','landscape/cielo.jpg','landscape/Faro_Trafalgar_01.jpg','landscape/guitarra_playa.jpg','landscape/Madrid.jpg','landscape/nieve_01.jpg','landscape/pueblo_01.jpg','landscape/puesta_playa_02.jpg','landscape/puesta_playa_03.jpg','landscape/puesta_playa_4.jpg'];
		var ph_list_02 = ['sst/01.jpg','sst/02.jpg','sst/03.jpg','sst/04.jpg','sst/05.jpg','sst/06.jpg','sst/07.jpg','sst/08.jpg','sst/09.jpg','sst/10.jpg','sst/12.jpg','sst/13.jpg','sst/14.jpg','sst/15.jpg','sst/17.jpg','sst/18.jpg','sst/19.jpg','sst/20.jpg','sst/21.jpg','sst/22.jpg','sst/23.jpg','sst/24.jpg','sst/25.jpg','sst/26.jpg','sst/27.jpg','sst/28.jpg','sst/29.jpg','sst/30.jpg','sst/31.jpg','sst/32.jpg','sst/33.jpg','sst/34.jpg','sst/35.jpg','sst/36.jpg','sst/37.jpg','sst/38.jpg','sst/39.jpg','sst/40.jpg','sst/41.jpg','sst/42.jpg','sst/43.jpg','sst/44.jpg','sst/45.jpg','sst/46.jpg','sst/47.jpg','sst/48.jpg','sst/49.jpg','sst/50.jpg','sst/51.jpg','sst/52.jpg','sst/53.jpg'];
		var listaDisp = [ph_list_01,ph_list_02];

		proyector.setPhotoList(listaDisp[0]);

		window.imageLoader.resetQueue();

			window.imageLoader.addImage("background","images/gallery/screen.png");
			window.imageLoader.addImage("background","images/gallery/proyector.png");
			window.imageLoader.addImage("background","images/gallery/palanca.png");
			window.imageLoader.addImage("background","images/gallery/button.png");
			window.imageLoader.addImage("background","images/gallery/loader_bars.png");
			window.imageLoader.addImage("background","images/gallery/loader_frame.png");
			window.imageLoader.addImage("background","images/gallery/thumb_frame.png");
			window.imageLoader.addImage("background","images/gallery/ch_landscapes.png");
			window.imageLoader.addImage("background","images/gallery/ch_sst.png");

			
		window.imageLoader.preload(function(){

			getImages();
			window.nextDrawFuncPointer = drawGallery;
			window.pageLoaded = true;
			window.initNavPage = initNavPageGallery;
			window.pageHTML = galleryTemplate();

			if ( !window.isAnimated ){
				
				window.isAnimated=true;
				window.drawFuncPointer = window.nextDrawFuncPointer;
				window.backgImg.image = window.nextBackgImg;
				window.pageContent.sizeElements(window.screenDim.height);

				window.pageContent.pageElements.innerHTML =window.pageHTML;

				window.animationCenter();

				initNavPageGallery();
			}	
		});


		

	}
	)();