// NIGROMANCE - INDEX SCRIPT

// INDEX INITIALIZATION	//
(function indexInit(){
	
	//AUDIO
	var ambient1 = new Audio("audio/night."+window.audioFormat);
	var ambient2 = new Audio("audio/night."+window.audioFormat);

	var nigroAdImg = {image:{},X:0,Y:0,W:0,H:0};
	var lampImg = {image:{},X:0,Y:0,W:0,H:0};
	var switchImgs = {images:[],X:0,Y:0,W:0,H:0,shdwX:0,shdwY:0,shdwW:0,SwPos:0,clicked:false,initX:0,initY:0,sparks:[],
				sparkSound: new Audio("audio/spark."+window.audioFormat),
				createSparks: function() {
					if ( this.sparks.length<20 ){
						var x1 =this.X + this.W*0.18;
						var x2 = this.X + this.W*0.82;
						var y = this.X + this.H*0.75;
						var col="rgba(255,255,255,1)";
						
						for(var i=0,x=x1,end=10*Math.random()+5;i<end;i++){
							var vx = (Math.random()-0.5)*50;
							var vy = (Math.random()-0.5)*150;
							this.sparks.push(new Spark(x,y,vx,vy,col)); 
						 }
						 
						for(var i=0,x=x2,end=10*Math.random()+5;i<end;i++){
							var vx = (Math.random()-0.5)*50;
							var vy = (Math.random()-0.5)*150;
							this.sparks.push(new Spark(x,y,vx,vy,col)); 
						 }
					}
					//Audio
					this.sparkSound.play();
				},
				drawSparks: function(ctx,time){
					if (this.sparks.length > 0 ){
						ctx.save();
						ctx.shadowBlur=10;
						
						for ( var i=0;i<this.sparks.length;i++ ){
							if ( this.sparks[i].Y > window.backgImg.H ){
								this.sparks.splice(i,1);
							}else{
								this.sparks[i].update(time);
								this.sparks[i].draw(ctx);
							}
						}
						ctx.restore();
					}
				}
			};



	var shadowGrdL,shadowGrdR;
	var shadowGrdT = {grd:{},X:0,Y:0,W:0,H:0};
	var lightGrd = {grd:{},Y:0};
	var dustParticles =[];
	
	var Particle = function(R_MAX,R_MIN,X_MIN,Y_MIN,X_LONG,Y_LONG,V_MIN,V_MAX,COLOR,OPACITY){
		this.OP_MIN = 0.3;
		this.LUMI_MIN = 210;
		this.R = Math.round(Math.pow(R_MAX-R_MIN,Math.random()))+R_MIN;
		this.X = Math.round(X_LONG*Math.random()+X_MIN);
		this.Y = Math.round(Y_LONG*Math.random()+Y_MIN);
		this.Vx = (Math.random()-0.5)*2*(( V_MAX - V_MIN )/( R_MAX - R_MIN ))*(this.R - R_MIN)+V_MIN;
		this.Vy = (Math.random()-0.5)*2*(( V_MAX - V_MIN )/( R_MAX - R_MIN ))*(this.R - R_MIN)+V_MIN;
		this.Red = this.LUMI_MIN + Math.round(Math.random()*(255-this.LUMI_MIN));
		this.Green = this.LUMI_MIN + Math.round(Math.random()*(255-this.LUMI_MIN));
		this.Blue = this.LUMI_MIN + Math.round(Math.random()*(255-this.LUMI_MIN));
		this.color;
		this.opacity;
		this.borX1=X_MIN;
		this.borX2=X_MIN+X_LONG;
		this.borY1=Y_MIN;
		this.borY2=Y_MIN+Y_LONG;
		
		if ( typeof(OPACITY) !== "number" || OPACITY > 1 || OPACITY < 0)
			this.opacity = Math.round(((( this.OP_MIN -1 )/( R_MAX - R_MIN ))*(this.R-R_MIN)+1)*100)/100;	
		else
			this.opacity = OPACITY;	
		
		if ( typeof(COLOR) !== "string" )
			this.color = "rgba("+this.Red+","+this.Green+","+this.Blue+","+this.opacity+")";	
		else
			this.color = COLOR;	
		
	}
	Particle.prototype.draw  = function (ctx){
			  ctx.save();
				  ctx.beginPath();
				  ctx.arc(this.X,this.Y,this.R,0,2*Math.PI,false);
				  ctx.closePath();
				  ctx.fillStyle = this.color;
				  ctx.fill();
			   ctx.restore();	
	  }
	Particle.prototype.move = function(dT){
		  var deltaX=0;
		  var deltaY=0;
		  if ( this.X<this.borX1 )	deltaX = Math.round(this.X-this.borX1)/500;
		  if ( this.X>this.borX2 )	deltaX = Math.round(this.X-this.borX2)/500;
		  this.Vx -= deltaX;
		  
		  if ( this.Y<this.borY1 )	deltaY = Math.round(this.Y-this.borY1)/500;
		  if ( this.Y>this.borY2 )	deltaY = Math.round(this.Y-this.borY2)/500;
		  this.Vy -= deltaY;
		  
		  this.X += this.Vx*dT; 
		  this.Y += this.Vy*dT;	
	  }
							
		var Spark = function(x,y,vx,vy,col){
				this.X = x;
				this.Y = y;
				this.Vx = vx;
				this.Vy = vy;
				this.color = col;
				this.shadow = "rgba(255,200,100,200)";
				this.life = 3;

		};
		Spark.prototype.update = function(deltaT){
			var colorLevel;
			this.life -=deltaT;
			
			if ( this.life < 2 ){
				colorLevel = Math.round(this.life*255/2);
				opacityLevel = this.life/2;
				this.color="rgba(255,"+colorLevel+","+colorLevel+","+opacityLevel+")";
				this.shadow="rgba(255,200,100,"+opacityLevel+")";
			}
			this.Vx -= 0.1;
			this.Vy += 6;
			this.X += this.Vx*deltaT;
			this.Y += this.Vy*deltaT;		
		}
		Spark.prototype.draw = function(ctx){
			ctx.shadowColor = this.shadow; 		
			ctx.beginPath();
			ctx.arc(this.X,this.Y,1,0,Math.PI*2);
			ctx.fillStyle  = this.color;
			ctx.fill();	
		}
		
		var Lamp = function(){
			this.angle=0;
			this.catched = {is:false,x:0,y:0,angle:0};
			this.over = false;
			var b = 1.95;			// (kg/s) Viscosidad del aire a 50º 1.95*10^-5, amortiguación.
			var r = 0.9;			//(m) Alto real de la lámpara
			var m=2;		 		// (kg)
									/* Theta - Angulo de inclinación (rad);*/
			var ang = 0; 
			this.vel_ang=0; 		// Omega w (rad/s)
			var acel_ang2=0;		// Alpha (rad/s^2)
			var f=0;				// Tau (N/m) - fuerza = j*acel_ang	
			var j=m*r*r;			// Momento de inercia (kg*m^2) - J = m*r^2
			var max_angle = 3*Math.PI/8;
			this.points=[];
			var sparks=[];
			this.update = function(){

				/* Velocidad Verlet */
				/*Cálculo de posición actual en función de la anterior posición, velocidad y aceleración*/
				this.angle += this.vel_ang * window.deltaT + ( 0.5 * acel_ang2 * window.deltaT * window.deltaT );
				/* Cálculo de fuerzas actuales = la componente tangencia al arco descrito de la fuerza de la gravedad - la fuerza amortiguadora de la viscosidad del aire Fb=-b*Vlineal (Vlineal = w*r) */
				var f = m * 9.81 * Math.cos(this.angle+Math.PI/2) * r - b*this.vel_ang*r;
				/* Cálculo de aceleración */
				var acel_ang= f / j;
				/* Cálculo de la actual velocidad a partir de la que velocidad y promedio de aceleración del anterior frame*/
				this.vel_ang += 0.5 * (acel_ang2 + acel_ang) * window.deltaT;
				/* Actualización de la aceleración */
				acel_ang2 = acel_ang;

				// Control de recorrido, hace que la lámpara choque a partir de cierto ángulo.
				//	Básicamente Limita el ángulo a PI/8 de 0 y PI, para la lámapra, vel_ang=0
				//	y se habilita para que vaya en sentido contrario, acel_ang en sentido contrario.
				if(this.angle > max_angle){
					this.angle = max_angle;
					vel_ang	= 0;
					this.createSparks();
				}else if(this.angle < -max_angle){
					this.angle = -max_angle;
					vel_ang	= 0;
					this.createSparks();
				}
			}
			this.createSparks = function(){
				if(sparks.length<20){
					var r = lampImg.H;
					var CX = lampImg.MX;
					var CY = lampImg.Y;
					var col="rgba(255,255,255,1)";
					
					for(var i=0,end=10*Math.random()+5;i<end;i++){
						var ang = lamp.angle+(Math.random()*0.3-0.15);	
						var x = Math.sin(-ang)*r+CX;
						var y = Math.cos(ang)*r+CY;
						var vx = (Math.random()-0.5)*50;
						var vy = (Math.random()-0.5)*150;
						sparks.push(new Spark(x,y,vx,vy,col)); 
					 }
				}
			}
			
			this.drawSparks = function(ctx,time){
				if ( sparks.length > 0 ){
					
					ctx.save();
					ctx.shadowBlur=10;
					
					for ( var i=0;i<sparks.length;i++ ){
						if ( sparks[i].Y > window.backgImg.H ){
							sparks.	splice(i,1);
						}else{
							sparks[i].update(time);
							sparks[i].draw(ctx);
						}
					}
					ctx.restore();
				}
			}

			this.strokeL = function(ctx){		
				ctx.beginPath();
				ctx.moveTo(this.points[0],this.points[1]);
				ctx.lineTo(this.points[2],this.points[3]);
				ctx.lineTo(this.points[4],this.points[5]);
				ctx.lineTo(this.points[6],this.points[7]);
				ctx.lineTo(this.points[8],this.points[9]);
				ctx.lineTo(this.points[10],this.points[11]);
				ctx.lineTo(this.points[12],this.points[13]);
				ctx.quadraticCurveTo(this.points[14],this.points[15],this.points[16],this.points[17]);
				ctx.lineTo(this.points[18],this.points[19]);
				ctx.lineTo(this.points[20],this.points[21]);
				ctx.lineTo(this.points[22],this.points[23]);
				ctx.lineTo(this.points[24],this.points[25]);		
				ctx.quadraticCurveTo(this.points[26],this.points[27],this.points[28],this.points[29]);		
				ctx.closePath();	
			}
			this.strokeR = function(ctx){
				
				ctx.beginPath();
				ctx.moveTo(this.points[30],this.points[31]);
				ctx.quadraticCurveTo(this.points[32],this.points[33],this.points[34],this.points[35]);		
				ctx.lineTo(this.points[36],this.points[37]);
				ctx.lineTo(this.points[38],this.points[39]);
				ctx.lineTo(this.points[40],this.points[41]);
				ctx.lineTo(this.points[42],this.points[43]);
				ctx.quadraticCurveTo(this.points[44],this.points[45],this.points[46],this.points[47]);
				ctx.lineTo(this.points[48],this.points[49]);
				ctx.lineTo(this.points[50],this.points[51]);
				ctx.lineTo(this.points[52],this.points[53]);
				ctx.lineTo(this.points[54],this.points[55]);
				ctx.lineTo(this.points[56],this.points[57]);
				ctx.lineTo(this.points[0],this.points[1]);
				ctx.closePath();
			}
			this.strokeC = function(ctx){
				ctx.beginPath();
				ctx.lineTo(this.points[2],this.points[3]);
				ctx.lineTo(this.points[4],this.points[5]);
				ctx.lineTo(this.points[6],this.points[7]);
				ctx.lineTo(this.points[8],this.points[9]);
				ctx.lineTo(this.points[10],this.points[11]);
				ctx.lineTo(this.points[12],this.points[13]);
				ctx.quadraticCurveTo(this.points[14],this.points[15],this.points[16],this.points[17]);
				ctx.lineTo(this.points[18],this.points[19]);
				ctx.lineTo(this.points[20],this.points[21]);
				ctx.lineTo(this.points[22],this.points[23]);
				ctx.lineTo(this.points[24],this.points[25]);	
				ctx.quadraticCurveTo(this.points[58],this.points[59],this.points[34],this.points[35]);		
				ctx.lineTo(this.points[36],this.points[37]);
				ctx.lineTo(this.points[38],this.points[39]);
				ctx.lineTo(this.points[40],this.points[41]);
				ctx.lineTo(this.points[42],this.points[43]);
				ctx.quadraticCurveTo(this.points[44],this.points[45],this.points[46],this.points[47]);
				ctx.lineTo(this.points[48],this.points[49]);
				ctx.lineTo(this.points[50],this.points[51]);
				ctx.lineTo(this.points[52],this.points[53]);
				ctx.lineTo(this.points[54],this.points[55]);
				ctx.lineTo(this.points[56],this.points[57]);
				ctx.closePath();
			}
	}
	
	
	//-----------------INITIALIZATION ---------------------------//
	var lamp = new Lamp();
	
	addEvent(window,'resize',updatePosDim,false);

	addEvent(canvasBackg,'mousemove',mouseMoving,false);
	addEvent(canvasBackg,'mousedown',mouseClickDown,false);
	addEvent(window,'mouseup',mouseClickUp,false);
	
	addEvent(canvasBackg,'touchmove',mouseMoving,false);
	addEvent(canvasBackg,'touchstart',mouseClickDown,false);
	addEvent(window,'touchend',mouseClickUp,false);
	
	window.imageLoader.addImage("indexBackg","images/index/index_backg.jpg");
	window.imageLoader.addImage("nigroAdvert","images/index/nigromance_cartel_450.png");
	window.imageLoader.addImage("lamp","images/index/lamp_400.png");
	window.imageLoader.addImage("base_switch","images/index/base_switch.png");
	window.imageLoader.addImage("piezes_switch","images/index/piezes_switch.png");
	window.imageLoader.addImage("switch","images/index/switch.png");
	window.imageLoader.addImage("switch_shadow","images/index/switchShadow.png");
	
	window.imageLoader.preload(function(){ 
									getImages();
									updatePosDim(); 

									// ANIMATION ACTIVATION
									window.drawFuncPointer = draw;
									if ( !window.isAnimated ){
										window.isAnimated = true;
										window.animationCenter();
									}
									window.pageLoaded = true;

								});
	

	//-----------------INITIALIZATION ---------------------------//
	

	
	
	function mouseClickDown(e){
		var absCoord = mouseCoord(e);
		var coord = coordIntoCanvas(canvasBackg,absCoord.x,absCoord.y);
		//LAMP ACTION
		if ( coord.y < window.backgImg.H/2){	
		
			ctxBackg.save();
				rotateFrom (ctxBackg,lamp.angle,lampImg.MX ,lampImg.Y);	
				lamp.strokeC(ctxBackg);	
				if ( ctxBackg.isPointInPath(coord.x,coord.y) ){	
					lamp.createSparks();
					lamp.catched.is = true;	
					lamp.catched.x = coord.x;
					lamp.catched.y = coord.y;
					lamp.catched.angle = Math.atan2( coord.y - lampImg.Y ,coord.x - lampImg.MX )-lamp.angle;
				}
			ctxBackg.restore();
			
		}else{ // SWITCH ACTION;
			if ( coord.y > switchImgs.Y && coord.y < (switchImgs.Y + switchImgs.H) && coord.x > switchImgs.X && coord.x < (switchImgs.X + switchImgs.W) ){
				switchImgs.clicked = true;
				switchImgs.initX = coord.x;
				switchImgs.initY = coord.y;
			}	
		}
		
		e.preventDefault();
	}
	
	function mouseClickUp(e){
		if ( lamp.catched.is )
			lamp.catched.is = false;
		if ( switchImgs.clicked )
			switchImgs.clicked = false;		
	}
	
	function mouseMoving(e){
		var absCoord = mouseCoord(e);
		var coord = coordIntoCanvas(canvasBackg,absCoord.x,absCoord.y);	

		if ( switchImgs.clicked ){
			// UP
			if ( switchImgs.initY - coord.y > 5 && switchImgs.SwPos < 10200 ){
					switchImgs.SwPos += 300;
					switchImgs.initX = coord.x;
					switchImgs.initY = coord.y;
			}
			// DOWN
			if  ( switchImgs.initY - coord.y < -5){
				
				if ( switchImgs.SwPos > 0 ){
					switchImgs.SwPos -= 300;
					switchImgs.initX = coord.x;
					switchImgs.initY = coord.y;
				}else if( window.location.hash!=="#home" ){

					switchImgs.createSparks();

					//Start the elevator
					window.pageContent.startElevator();
					window.pageLoaded = false;

					// Change the location
					window.location.hash="#home";
					window.locate();
				}
			}
			
			
		}else if ( lamp.catched.is ){
			
			var angB = Math.atan2( coord.y - lampImg.Y ,coord.x - lampImg.MX );
			var ang = angB - lamp.catched.angle;
			if ( ang < 1.18 && ang > -1.18 ){
				lamp.angle = ang;
			}
			
			
		}else if ( coord.y < lampImg.Y+lampImg.H ){
			ctxBackg.save();
				rotateFrom (ctxBackg,lamp.angle,lampImg.MX ,lampImg.Y);
				
				lamp.strokeL(ctxBackg);
				if ( ctxBackg.isPointInPath(coord.x,coord.y) ){
						lamp.vel_ang -=0.1;
				}
				lamp.strokeR(ctxBackg);
				if ( ctxBackg.isPointInPath(coord.x,coord.y) ){
						lamp.vel_ang +=0.1;
				}
			ctxBackg.restore();
			
		}
		e.preventDefault();
	}
	
	function getImages(){
		window.backgImg.image = window.imageLoader.getImage(0).image;
		nigroAdImg.image = window.imageLoader.getImage(1).image;	
		lampImg.image = window.imageLoader.getImage(2).image;
		switchImgs.images.push(window.imageLoader.getImage(3).image);
		switchImgs.images.push(window.imageLoader.getImage(4).image);
		switchImgs.images.push(window.imageLoader.getImage(5).image);
		switchImgs.images.push(window.imageLoader.getImage(6).image);
		
		//Audio
		ambient1.volume=0.3;
		ambient2.volume=0.3;
		ambient1.play();
	}
	
	function updatePosDim(){
		
		nigroAdImg.W = Math.round(window.backgImg.W*0.234);
		nigroAdImg.H = Math.round(window.backgImg.H*0.22);
		nigroAdImg.X = Math.round((window.canvasBackg.width-nigroAdImg.W)/2);
		nigroAdImg.Y = Math.round(window.backgImg.H*0.25);
		
		lampImg.W = Math.round(window.backgImg.W*0.21);
		lampImg.H = Math.round(window.backgImg.H*0.241);
		lampImg.X = Math.round((window.canvasBackg.width-lampImg.W)/2);
		lampImg.Y = -Math.round(window.backgImg.H*(0.05));
		lampImg.MX = lampImg.X + lampImg.W/2;
		
		
		switchImgs.W = Math.round(window.backgImg.W*0.057);
		switchImgs.H = switchImgs.W*2.75;
		switchImgs.X = Math.round((canvasBackg.width-switchImgs.W)/2);
		switchImgs.Y = Math.round(window.backgImg.H*(0.5));
		switchImgs.shdwX = switchImgs.X+switchImgs.W*0.05;
		switchImgs.shdwY = switchImgs.Y+switchImgs.H*0.5;
		switchImgs.shdwW = switchImgs.W*0.87;
		switchImgs.SwPos = 10200;
		
		shadowGrdT.X = lampImg.X-lampImg.W*0.7; 
		shadowGrdT.Y = lampImg.Y-lampImg.H*2;
		shadowGrdT.W = lampImg.W*2;
		shadowGrdT.H = lampImg.H*3;


		lightGrd.Y = Math.round(lampImg.Y+lampImg.H*0.9);
		lightGrd.grd = ctxOffset.createRadialGradient( lampImg.MX, window.backgImg.H, Math.round(lampImg.W*1.7) ,lampImg.MX, lightGrd.Y, Math.round(lampImg.W/3) );
		lightGrd.grd.addColorStop(0,"rgba(0,0,0,0)");
		lightGrd.grd.addColorStop(0.90,"rgba(255,240,200,0.5)");	
		lightGrd.grd.addColorStop(1,"rgba(255,240,230,0.7)");
		
		shadowGrdL = ctxOffset.createLinearGradient(window.backgImg.X,window.backgImg.Y,Math.round(window.backgImg.W/2+window.backgImg.X),Math.round(window.backgImg.H*0.43+window.backgImg.Y));
		shadowGrdL.addColorStop(0.65,"rgba(0,0,0,1)");
		shadowGrdL.addColorStop(0.8,"rgba(0,0,0,0.1)");
		
		shadowGrdR = ctxOffset.createLinearGradient(window.backgImg.X+window.backgImg.W,window.backgImg.Y,Math.round(window.backgImg.W/2+window.backgImg.X),Math.round(window.backgImg.H*0.43+window.backgImg.Y));
		shadowGrdR.addColorStop(0.65,"rgba(0,0,0,1)");
		shadowGrdR.addColorStop(0.8,"rgba(0,0,0,0.1)");	
		
		shadowGrdT.grd = ctxOffset.createRadialGradient(  lampImg.MX ,Math.round(lampImg.H*1.5) ,lampImg.W*0.75,lampImg.MX ,Math.round(lampImg.H*2) ,  lampImg.W*1.2 );
		shadowGrdT.grd.addColorStop(0,"rgba(0,0,0,0)");
		shadowGrdT.grd.addColorStop(0.8,"rgba(0,0,0,1)");
		
		dustParticles=[];
		for ( var i=0,end=30,W=window.backgImg.W,H=window.backgImg.H; i<end ; i++){
			dustParticles.push(new Particle(10,1,window.backgImg.X + window.backgImg.W*0.25,window.backgImg.Y + window.backgImg.H*0.2,window.backgImg.W*0.5,window.backgImg.H*0.5,2,20));
		}
		
		lamp.points = [lampImg.X+lampImg.W*0.5,lampImg.Y+lampImg.W*0.218,lampImg.X+lampImg.W*0.46,lampImg.Y+lampImg.W*0.218,lampImg.X+lampImg.W*0.46,lampImg.Y+lampImg.W*0.240,lampImg.X+lampImg.W*0.387,lampImg.Y+lampImg.W*0.245,lampImg.X+lampImg.W*0.387,lampImg.Y+lampImg.W*0.255,lampImg.X+lampImg.W*0.355,lampImg.Y+lampImg.W*0.29,lampImg.X+lampImg.W*0.35,lampImg.Y+lampImg.W*0.355,lampImg.X+lampImg.W*0.2,lampImg.Y+lampImg.W*0.37,lampImg.X+lampImg.W*0.13,lampImg.Y+lampImg.W*0.435,lampImg.X+lampImg.W*0.11,lampImg.Y+lampImg.W*0.44,lampImg.X+lampImg.W*0.025,lampImg.Y+lampImg.W*0.625,lampImg.X,lampImg.Y+lampImg.W*0.629,lampImg.X,lampImg.Y+lampImg.W*0.635,lampImg.X+lampImg.W*0.45,lampImg.Y+lampImg.W*0.65,lampImg.X+lampImg.W*0.5,lampImg.Y+lampImg.W*0.635,lampImg.X+lampImg.W*0.5,lampImg.Y+lampImg.W*0.635,lampImg.X+lampImg.W*0.55,lampImg.Y+lampImg.W*0.65,lampImg.X+lampImg.W,lampImg.Y+lampImg.W*0.635,lampImg.X+lampImg.W,lampImg.Y+lampImg.W*0.629,lampImg.X+lampImg.W*0.98,lampImg.Y+lampImg.W*0.625,lampImg.X+lampImg.W*0.89,lampImg.Y+lampImg.W*0.44,lampImg.X+lampImg.W*0.87,lampImg.Y+lampImg.W*0.435,lampImg.X+lampImg.W*0.8,lampImg.Y+lampImg.W*0.37,lampImg.X+lampImg.W*0.65,lampImg.Y+lampImg.W*0.355,lampImg.X+lampImg.W*0.645,lampImg.Y+lampImg.W*0.29,lampImg.X+lampImg.W*0.613,lampImg.Y+lampImg.W*0.255,lampImg.X+lampImg.W*0.613,lampImg.Y+lampImg.W*0.245,lampImg.X+lampImg.W*0.54,lampImg.Y+lampImg.W*0.240,lampImg.X+lampImg.W*0.54,lampImg.Y+lampImg.W*0.218,lampImg.X+lampImg.W*0.5,lampImg.Y+lampImg.W*0.65];
	

	}
	
	function drawOffset(){
		//CLEAN THE CANVAS
		window.ctxOffset.clearRect(0,0,window.canvasOffset.width,window.canvasOffset.height);

		// DUST PARTICLESs
		for ( var i=0,end=dustParticles.length;i<end;i++ ){
			dustParticles[i].move(deltaT);	
			dustParticles[i].draw(ctxOffset);	
		}
		
		if ( !lamp.catched.is )
			lamp.update();
		
		window.ctxOffset.save();
		
			rotateFrom (ctxOffset,lamp.angle,lampImg.MX ,lampImg.Y);

			// SHADOWS & LIGHT
			window.ctxOffset.save();
				window.ctxOffset.fillStyle=lightGrd.grd;
				window.ctxOffset.fillRect(window.backgImg.X,lightGrd.Y,window.backgImg.W,window.backgImg.SH);
				window.ctxOffset.fillStyle = shadowGrdL;
				window.ctxOffset.fillRect(window.backgImg.SX,window.backgImg.SY,window.backgImg.SW,window.backgImg.SH);
				window.ctxOffset.fillStyle = shadowGrdR;
				window.ctxOffset.fillRect(window.backgImg.SX,window.backgImg.SY,window.backgImg.SW,window.backgImg.SH);
				window.ctxOffset.fillStyle = shadowGrdT.grd;
				window.ctxOffset.fillRect(shadowGrdT.X,shadowGrdT.Y,shadowGrdT.W,shadowGrdT.H);
			window.ctxOffset.restore();
			// LAMP
			window.ctxOffset.drawImage(lampImg.image,lampImg.X+0.5,lampImg.Y,lampImg.W,lampImg.H);
			//Sparks
			
		window.ctxOffset.restore();

		// TO THE RENDER CANVAS	
		window.ctxBackg.drawImage(canvasOffset,0,0,canvasBackg.width,canvasBackg.height,0,0,canvasBackg.width,canvasBackg.height);	
	}
	
	function draw(){
		
		//	BACKGROUND
		window.ctxBackg.drawImage(window.backgImg.image,window.backgImg.X+0.5,0,window.backgImg.W,window.backgImg.H);

		// ADVERT
		window.ctxBackg.save();
			window.ctxBackg.shadowOffsetY = 10;  
			window.ctxBackg.shadowOffsetX= 0;
			window.ctxBackg.shadowBlur = 10;
			window.ctxBackg.shadowColor = "rgba(0,0,0, 0.9)"; // Transparent grey  		
			window.ctxBackg.drawImage(nigroAdImg.image,nigroAdImg.X+0.5,nigroAdImg.Y,nigroAdImg.W,nigroAdImg.H);
			
			window.ctxBackg.drawImage(switchImgs.images[0],switchImgs.X,switchImgs.Y,switchImgs.W,switchImgs.H);
			window.ctxBackg.shadowOffsetY = 5;
			window.ctxBackg.shadowBlur = 2;
			window.ctxBackg.drawImage(switchImgs.images[1],switchImgs.X,switchImgs.Y,switchImgs.W,switchImgs.H);
			
			var switchH
			if ( switchImgs.SwPos > 5400 ){
				switchH = 2*switchImgs.H*(1-switchImgs.SwPos/10200);
			}else{
				switchH = switchImgs.H/2+switchImgs.H*switchImgs.SwPos/5400;
			}
			window.ctxBackg.drawImage(switchImgs.images[3],switchImgs.shdwX,switchImgs.shdwY,switchImgs.shdwW,switchH);
			
			window.ctxBackg.drawImage(switchImgs.images[2],0,switchImgs.SwPos,109,300,switchImgs.X,switchImgs.Y,switchImgs.W,switchImgs.H);
		window.ctxBackg.restore();
		
		drawOffset();
		switchImgs.drawSparks(ctxBackg,window.deltaT);
		lamp.drawSparks(ctxBackg,window.deltaT);

		// AUDIO AMBIENT
		var mainVolume = window.config.getMainVolume();
		ambient1.volume = mainVolume;
		ambient2.volume = mainVolume;

		if ( ambient1.currentTime >= 1.4)
			ambient2.play();
		if (ambient2.currentTime >= 1.4)
			ambient1.play();
	}
	

})();



