//////////////////////////
//						//
//		VARIABLES		//
//						//
//////////////////////////

window.cssAdded = ""; // CSS async loading parameter (addCSS and removeCSS)
window.jsLoaded= false; // Shows is the first load or not.
window.imageLoader = new ImageManager();
window.screenDim = window.viewPortDim();
window.lastTimeCheck = +new Date();
window.pageLoaded = false;
window.pageHTML = "";

/* 	TIME VARS	*/
window.deltaT = 0;
window.lastTimeCheck = +new Date();
window.drawFuncPointer = 0;
window.nextDrawFuncPointer = 0;
window.isAnimated=false;
window.timeWindow = 0;

/*  CANVAS Vars */
window.backgImg = {image:{},X:0,Y:0,W:0,H:0};
window.nextBackgImg = new Image();

window.canvasBackg = document.createElement("canvas");
window.canvasBackg.setAttribute ('id','canvasBackg');
window.ctxBackg = window.canvasBackg.getContext('2d');
window.canvasOffset = document.createElement("canvas");
window.ctxOffset = window.canvasOffset.getContext('2d');


/* AUDIO FORMAT COMPATIBILITY DETECTION	*/
window.audioGlobalParams = {volume:1,mute:false};
window.audioFormat = "mp3";
window.audioTest = new Audio();
window.mp3Suported = (typeof window.audioTest.canPlayType === "function" && window.audioTest.canPlayType("audio/mpeg") !== "");
if ( !window.mp3Suported )
	window.audioFormat = "ogg";



//////////////////////////
//						//
//		FUNCTIONS		//
//						//
//////////////////////////


//////////////////////////////////////////
/* %PAGE LOCALIZATION - FILE MANAGEMENT	*/
//////////////////////////////////////////
function replaceJsFile(newJs,oldJs){
	var scriptList = document.getElementsByTagName('script');
	var newScript = document.createElement('script');

	newScript.setAttribute('type','text/javascript');
	newScript.setAttribute('charset','utf-8');
	newScript.setAttribute('src','script/'+newJs+'.js');

	for ( var i=0,end=scriptList.length; i<end; i++ ){
		if ( scriptList[i].src.indexOf(oldJs) !== (-1) ){
			scriptList[i].parentNode.replaceChild(newScript,scriptList[i]);
			break;
		}
	}
}

window.locate = function(){
	var location = window.location.hash.replace('#','');

	if ( window.jsLoaded !== false ){
	
		replaceJsFile(location,window.jsLoaded );

	}else{
		if ( location === '')
			location = 'index';

		require([location]);
	}
	window.jsLoaded = location;
}
function addCSS(fileURI,callback){
	if (  typeof(fileURI) === "string" && fileURI.length>0 && window.cssAdded.indexOf(fileURI) === "-1" ){
		var linkEl = document.createElement("link");
		linkEl.setAttribute("rel","stylesheet");
		linkEl.setAttribute("type","text/css");
		linkEl.setAttribute("href",fileUri);
		if ( typeof(callback) === "function" ) 
			linkEl.onload = callback;
		if ( typeof(linkEl) !== "undefined" ){
			document.getElementsByTagName("head")[0].appendChild(linkEl);
			window.cssAdded += "["+fileURI+"]";
		}
	}else{
		console.info("ERROR: adding CSS file");	
	}
}
function removeCSS(fileURI,callback){
	if (  typeof(fileURI) === "string" && fileURI.length>0 && window.cssAdded.indexOf(fileURI) === "-1" ){
		var candidates = document.getElementsByTagName("link");
		for ( var i=0,end=candidates.length; i=0 ; i++){
			if ( candidates[i].getAttribute("href") === fileURI ){
				candidates[i].parentNode.removeChild(candidates[i]);
				window.cssAdded
				break;
			}
		}
		
		window.cssAdded.replace("["+fileURI+"]","");
		if (typeof(callback) === "function" ) 
			callback();
	}else{
		console.info("ERROR: removing CSS file");	
	}
}

//////////////////////////////////////
/* %PAGE CONTENT MANAGE	- ELEVATOR 	*/
//////////////////////////////////////
PageContent = function(){
	this.elevOffset = 0;
	this.pageOffset = 0;
	this.speed = 0;
	this.element = document.getElementById("page_content");
	this.pageElements = document.getElementById("page_elements");
	this.container = document.getElementById("displmnt_container");
	this.newPageInserted = false;
	this.wEngine;
	var self = this;
	//AUDIO
	this.aStartElev = new Audio("audio/elev_start."+window.audioFormat);
	this.aRunElev1 = new Audio("audio/elev_run."+window.audioFormat);
	this.aRunElev2 = new Audio("audio/elev_run."+window.audioFormat);
	this.aStopElev = new Audio("audio/elev_stop."+window.audioFormat);


	this.clean = function(){
		
		var childs = this.pageElements.childNodes.length;

		for ( var i=0,end=childs ; i<end; i++ ){
			if ( (typeof  this.pageElements.childNodes[i] !== 'undefined') && this.pageElements.childNodes[i].id !== 'canvasBackg' ){
				this.pageElements.removeChild(this.pageElements.childNodes[i]);
			}
		}
	}

	this.sizeElements = function(height){
		canvasBackg.width = window.screenDim.width;
		canvasBackg.height = window.screenDim.height;
		
		// The background image width is according 
		// 16/9 proportions from the window´s height	
		window.backgImg.W = Math.round(1.78*canvasBackg.height);	

		window.backgImg.H = window.canvasBackg.height;
		window.backgImg.SH = window.backgImg.H * 4;
		window.backgImg.SW = window.backgImg.W * 2;
		window.backgImg.X = Math.round((window.canvasBackg.width-window.backgImg.W)/2);
		window.backgImg.SX = window.backgImg.X -  window.backgImg.W * 0.5;
		window.backgImg.SY = window.backgImg.Y -  window.backgImg.H * 1.5;

		// TOPS
		document.getElementById("displmnt_container").style.top = height+"px";
		this.elevOffset = height;

		// HEIGHTS
		document.getElementById("page_content").style.height = height+"px";
		document.getElementById("viewport_container").style.height = height+"px";
		document.getElementById("wall_1").style.height = height+"px";
		document.getElementById("wall_2").style.height = height+"px";
	}

	this.evaluePageLoad = function(){
		// Is the previous page passed??
			if ( this.elevOffset <= 0 && this.elevOffset > window.screenDim.height*(-1)-50  ){
				
				// Is the next page loaded??
				if ( window.pageLoaded && !this.newPageInserted){


					// Then we put the new page into de content and delete the old one.
					this.clean();

					window.backgImg.image = window.nextBackgImg;
					window.drawFuncPointer = window.nextDrawFuncPointer;

					// Adding the aditional elements into the page
					if ( document.getElementById("page_elements") ){
						this.pageElements.innerHTML = window.pageHTML;
					}

					this.newPageInserted = true;

					this.pageOffset = window.screenDim.height+75;

				}
			}

			if ( this.elevOffset <= window.screenDim.height*(-1)-100 && !this.newPageInserted ){
				// If the next page is not loaded??
					this.elevOffset += screenDim.height+50;
			}


			if ( this.elevOffset <= window.screenDim.height*(-2)-150){

				//this.speed=0;
				this.stopElevator();
				this.newPageInserted = false;
				this.elevOffset = window.screenDim.height;
				this.container.style.top = window.screenDim.height+"px";
				window.initNavPage();
			}
	}

	this.startElevator = function(){
		// Creating worker for the elevator movement
		if ( typeof (wEngine) == 'undefined'){
			this.wEngine = new Worker("wElevEngine.js");
			addEvent(this.wEngine,'message',this.moveElevator,false);
		}
		// Audio
		this.aStartElev.play();
		this.aRunElev1.play();
		//Worker
		this.wEngine.postMessage('start');
		this.speed = 1;
	}

	this.stopElevator = function(){
		//Audio
		this.aStopElev.play();
		this.aRunElev1.pause();
		this.aRunElev2.pause();
		//Worker
		this.wEngine.postMessage('stop');
		this.speed = 0;
	}

	this.moveElevator = function(e){
		var mainVolume = window.config.getMainVolume();
		self.elevOffset -= e.data;

		self.aRunElev1.volume = mainVolume;
		self.aRunElev2.volume = mainVolume;
		self.aStopElev.volume = mainVolume;
		self.aStartElev.volume = mainVolume;

		self.container.style.top = self.elevOffset+"px";
		self.pageOffset -= e.data/2;
		self.element.style.top = self.pageOffset+"px";

		if ( self.aStopElev.paused ){
			if ( self.aRunElev1.currentTime >= 1)
				self.aRunElev2.play();
			if ( self.aRunElev2.currentTime >= 1)
				self.aRunElev1.play();
		}

		self.evaluePageLoad();

	}	
}

//////////////////////////////
/* %AUDIO API MANAGEMENT 	*/
//////////////////////////////
window.audioCxt;	// Is init into the global init function

function loadSound(url){
	var soundBuffer;
	var ajax = new XMLHttpRequest();
	ajax.open('GET',url,true);
	ajax.responseType = 'arraybuffer';

	ajax.onload = function(){
		window.audioCtx.decodeAudioData(ajax.response, function(buffer){
			soundBuffer = buffer;
		},onError);
	}

	ajax.send();
}



//////////////////////////
/* %CANVAS MANAGEMENT 	*/
//////////////////////////
function canvasResize(cnvs){
	//Trying to keep 16/9 resolution
	screenProp = window.screenDim.width/window.screenDim.height;	
	if ( screenProp < 1.78  ){
		cnvs.height = screenDim.height;
		cnvs.width = screenDim.width;	
	}else{
		cnvs.width = screenDim.width;
		cnvs.height =Math.round(screenDim.width/1.78);
	}	
}
function rotateFrom (ctx,ang,ax,ay){	
	
	var ha=Math.sqrt(Math.pow(ax,2)+Math.pow(ay,2)); //Distancia al punto A desde 0,0
	var angA = Math.atan2(ay,ax);
	var angB = angA+ang;
	var by = ha*Math.sin(angB);
	var bx = ha*Math.cos(angB);
	
	var recx = ax-bx;
	var recy = ay-by;	
	
	ctx.translate(recx,recy);
	ctx.rotate(ang);
}
function coordIntoCanvas(canvas,absX,absY){
	var cnvsDim = canvas.getBoundingClientRect();
	return {x: Math.round(absX-cnvsDim.left*(canvas.width/cnvsDim.width)), y: Math.round(absY-cnvsDim.top*(canvas.height/cnvsDim.height))};
}
function canvasRemoveSmooth(ctx){
	ctx.webkitImageSmoothingEnabled = false;
	ctx.mozImageSmoothingEnabled = false;
	ctx.imageSmoothingEnabled = false; // future	
}

/* ANIMATION CENTER */
function animationCenter(){
	var newTime = +new Date();

	// Time management
	window.deltaT = (newTime - window.lastTimeCheck)/1000;
	window.timeWindow += deltaT;
	window.lastTimeCheck = newTime;
	
	if ( window.isAnimated ){
			
			//////////////////////
			//	FPS CONTROLER	//
			//////////////////////
		/*if ( window.timeWindow > 0.030 ){	// Limit to 25 fps max

			 window.timeWindow = 0;

			// Elevator MOVEMENT
			window.pageContent.moveElevator(deltaT);

			//DRAWING function CALL
			if ( typeof window.drawFuncPointer === 'function'){
				window.drawFuncPointer();
			}

		}
		*/

		// Elevator MOVEMENT
		if ( window.pageContent.speed !== 0 ){
			//window.pageContent.moveElevator();
		}
		
		//DRAWING function CALL
		if ( typeof window.drawFuncPointer === 'function'){
			window.drawFuncPointer();
		}

		//Draw Config panel
		config.draw();

		requestedFrame = requestAnimFrame(animationCenter);
	}
}



//////////////////////////////
/* %MENU HEADER MANAGEMENT 	*/
//////////////////////////////

function addMenu(){
	window.imageLoader.resetQueue();
	window.imageLoader.addImage("menuScreen_on","images/menu/pantalla_menu_on-24.png");
	window.imageLoader.addImage("menuScreen_off","images/menu/pantalla_menu-24.png");
	
	window.imageLoader.preload(function(){
			var titles = {titles:[{name: "HOME"}, {name: "GALLERY"},{name: "VIDEO"},{name: "WORKS"},{name: "APPS"}]};
			var header = document.getElementById("header_menu");
			header.innerHTML = menuTemplate(titles);

		//Menu initialization
		window.oMenu = new Menu();
		window.oMenu.draw();
		
		addEvent(document.getElementById("menu_container"),'click',window.oMenu.clickOption,false);

	});
	
}

var Menu = function(){
	var optionEl = {img: new Image(),
					imgLoaded: false,
					hoverMask: new Image(),
					hoverMaskLoaded: false,
					imgWidth: 0,
					imgHeight: 0
	}
	this.background = new Image();			
	var self = this;
	this.actualPos = "";



	this.draw = function(){		
			//BACKGROUND
			var menu_op_00 = document.getElementById("menu_op_00");
			var menu_op_01 = document.getElementById("menu_op_01");
			var menu_op_02 = document.getElementById("menu_op_02");
			var menu_op_03 = document.getElementById("menu_op_03");
			var menu_op_04 = document.getElementById("menu_op_04");
			
			this.actualPos = window.location.hash.replace('#','').toUpperCase();
			var container = document.getElementById("menu_container")

			for ( var i=0, end=container.childNodes.length; i<end ; i++){
				if ( container.childNodes[i].innerHTML === this.actualPos){
					container.childNodes[i].className += ' menu_selected';
				}
			}
			
			menu_op_00.style.animationName = "slideDown";
			menu_op_00.style.animationDuration = "1s";
			menu_op_00.style.webkitAnimationName = "slideDown";
			menu_op_00.style.webkitAnimationDuration = "1s";
			menu_op_00.style.display = "block";
			
			setTimeout(function(){
				menu_op_01.style.animationName = "slideDown";
				menu_op_01.style.animationDuration = "1s";
				menu_op_01.style.webkitAnimationName = "slideDown";
				menu_op_01.style.webkitAnimationDuration = "1s";
				menu_op_01.style.display = "block";
			},250);
			
			setTimeout(function(){
				menu_op_02.style.animationName = "slideDown";
				menu_op_02.style.animationDuration = "1s";
				menu_op_02.style.webkitAnimationName = "slideDown";
				menu_op_02.style.webkitAnimationDuration = "1s";
				menu_op_02.style.display = "block";
			},500);
			
			setTimeout(function(){
				menu_op_03.style.animationName = "slideDown";
				menu_op_03.style.animationDuration = "1s";
				menu_op_03.style.webkitAnimationName = "slideDown";
				menu_op_03.style.webkitAnimationDuration = "1s";
				menu_op_03.style.display = "block";
			},750);
			
			setTimeout(function(){
				menu_op_04.style.animationName = "slideDown";
				menu_op_04.style.animationDuration = "1s";
				menu_op_04.style.webkitAnimationName = "slideDown";
				menu_op_04.style.webkitAnimationDuration = "1s";
				menu_op_04.style.display = "block";
			},1000);
	}
	
	this.clickOption = function(e){
		var element = trigger(e);
		
		if( window.pageContent.speed === 0 && element.className.match("menu_op") && !element.className.match("menu_selected") ){
			
			var container = document.getElementById("menu_container");

			for ( var i=0, end=container.childNodes.length; i<end ; i++){

				if ( container.childNodes[i].tagName.toLowerCase() === 'div'){
					container.childNodes[i].className = container.childNodes[i].className.replace(' menu_selected','');
				}
			}

			element.style.animationName = "menuClickAnimation";
			element.style.animationDuration = "1s";
			element.style.webkitAnimationName = "menuClickAnimation";	
			element.style.webkitAnimationDuration = "1s";
			
			element.style.background = 'url("images/menu/pantalla_menu-24.png")';
			setTimeout(function(){
				element.style.backgroundImage = 'url("images/menu/pantalla_menu_on-24.png")';
				setTimeout(function(){
					element.style.backgroundImage = 'url("images/menu/pantalla_menu-24.png")';
					setTimeout(function(){
						element.style.backgroundImage = 'url("images/menu/pantalla_menu_on-24.png")';
						setTimeout(function(){
							// element.style.backgroundImage = 'url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAABwCAYAAABbwT+GAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MDhDMkYzMDREQjNFMTFFMzk0MDRDMjk4M0Q1RTkxQzYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MDhDMkYzMDVEQjNFMTFFMzk0MDRDMjk4M0Q1RTkxQzYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowOEMyRjMwMkRCM0UxMUUzOTQwNEMyOTgzRDVFOTFDNiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowOEMyRjMwM0RCM0UxMUUzOTQwNEMyOTgzRDVFOTFDNiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PsxmefoAAIZXSURBVHja7L0JuCXXVR66dk3n1JnvPPXtuVvqbnVrliVbni3LsmVijG0IEAMJAUOI4xATvveAvMR8L3khZjAGk5d4ABvIA2MQHjGWLcsaLaml1tStnofbdx7PfKpOVe231tpV59QZbrdkyVjKx9V31Hc4Qw17rfX/a/1rbfHUt38HNE3A9OQw/NS//jjc9bVH4GX2peNjLz4+gI+fwkf+Bbw2wMccPv4IH3+Bj3OZgWk//gQhBEj8t1ach8BvXuoYrsLHb+Djh/BhwivvaxEf/xEff4aPO3RN+xwIsOykDW7ThWbTk/j1Lfzbj+6eHFm1EibsObAXrEQCPv+Fr78Jr9Nf4aOg4WJJp2y/XKloQSCL+PyfxMdXX3fjIflSHSguR2jiXVqtNkHKgO/RD+rLeIXc3Aw+xoWmJTWh8QXD7/H+CgjwAspAAv9H30u8T7J1r+jKerT+8VEF+osMOt5YSnXxbdtGc8LLge/f/VWtVukN6aGFj1fiVxIfWTJu0zTWMnYyoGv32tffGszNzIvnjh8TTtM3fT/w624TTDSctJ0G3eAlUk/ZScsPAjEwMCBNQ9fr9Tq4gUceZS5jgDxx8sxLdqA+vq2dH4RMYQidlvyBXrRXgoHwgtR0PZHKDgjPdcBzG+A3m6FRSAoD6HXoofOz2VCC0FjUwm7Qdafvhab3fIBEO7JTWTA1Dw1ObGYgNfSe9Agsy0IHrOHNC8DzvVYk4k8S6nv6jyIz2yuERisliJjp0tMD3wfb0MBIJKGObyXdGv8haDkBdQZsodFn0BGHXjUInYHk9xbqs8gKDBN0XePPwehAxyod13HxT823vvH1x0+ePLbk+nLrm974Bvnogw9rK4sXYL3iLpYrNUcGAnxcmAtLG/gvOxR3aCCnLa2sw9josMTDFRvrq2BZJlSq9fVMZgit7qVZShKvRzKTRQMZgCDwf+CL75VgIHSVSriAhMSbZaBHyw1sgQR6/ISVwhvoQ6NWwUXgguPUwHPQs+EjYLvhVYTYAep46b0tV7yqrzsSugVBbQWalSX8vhc9jY2Ny8XFhWomk8lNT23BpxiwUSqiQRno7Tz+nnEYGh+6Z1zYAgz8XhmNMmJewHjDdU2StQMfGf4vJ13YNZQGf3gbnF11wK4vQDpowoWyC4FpsQGQDaC9I14UkUVL9Xp8S3xjWsQBencyVvxXXSNcZBKPI5/P8XEtLy2bMxdnbFrs73j7W7XPfnomCMxEcPWhQ+LCiZMwVEh7wkgeRgNp5HIpWFnfgBP3zqjwnUlNDw3ltfnldZgaG4ZyuQR20kQHYNbRQNYSBsEg76UwD3QMAZiZPDsH6Xn/aCDP4yuBj7WknVub3n2NFngNSX6xUStD4GGUQE9nagkw0ilIpQu4oDwobSxDvVLCxevS4vKEkFX8t7m+cBqgD55F5wqphA5ZNDr6fpOveiGbG9kytcXwcKE3PR88NErX8XgB0qLU0GNbuKjJc1OEIehHn0bfo3WDTkai4yLAnw2MVDr+7sqBFGwfKcBiMgfLThUCR4ftwzZGFBeWXDzo8PgEnnUA/J6MCtnoVPBDo8DnEdpxVKCy8FgSyB3oeYXCIBmrLK6tp/Gt6OFurC+PNV13++TWHZBEGEUOZiCf8uueW7EtIWtuFWoYqaOvbDp1KGFY5tDgYDA2PqTNzy/g83P4mcxBqnYy8aJvMjkTglYSI6mJj+BlYByvFAOhNbKeHx5LL5x/1lhduODjYtTsbA7JYgbDQwCuiySzWgvhRJOBhkaL1s4ECMlqnt+s4MrxqsWVzW8QGsdIYZojkpR9jcTJ5fJ6NpWGtXKRIwcuMs4CCF3xIoJ5xDANNBL6nhYtvRUZkMYYyWfXn8DnJ3SDjWQonwKp62DhIsvg8p2bw+foAkYLNpTWXbDwbGpkjOgIdORH+BLRpEgR2jlBKaQN/KBjIFiSsJOQxjerIU+g8/FdVyQsXsSpvVdeITPZ3GSlUsXIkAGn4SD8avi5dLKxuC7KlrRB82w0/mrrxLdPTw6USmVt967tfjZbgOXlFfzdKMwvl55B4/fniuUXfZMp8pHFT04P/kBJ+SvRQDwrlc5XSqUbUlYabnr9u4Vpp5As5iFvpmGlug61Wh3qpQo0mg6U1uZgfXUeGtUieI2KTNj5mnRqVS9wLgloPTQsukk6Lla+Wb1fLnrlGnnlRqPB3MNDI/F89bYUPZgzMHcQbBT0s4dGEoSJAXpO2sZoh783cTUMWTokUykQloVwwufFTMbtCBNyKROGkTFsoHvQMRrUXQ8hpMfchGCXhgZGx0osxfVDQ6RjJ7jFhopRCn8ul8vg4vEyH8EvhIhQyGcPFDFqDA0hd9CIubhN0zLXUgm4kEwgRBSSDY6+0ikb8tnUzWcvXIR9Bw7oTaQxjUY9SKVtrTm/cS9dq1K5+pLc6Fy+gLA5idfM/UcDeWHEzYPxbXtWrQB2zJ59WpufOUsBASI/TwvDNJKIudEzJ22GOUZ2GIhCNx3HRcLuhCnfzcMUhXRcVwQXmv3De4CLxTWQf6RwUWcR4685Tpg1A16MQldQy0AegisMjQRhEXl9MpCQvGMQYVI1WUjC1lwSDPw8WvC6xliJQBT4AiMKRrTdySQ8t1yF1arHR6/Iv+Iu9N5aSOTpmGhBm2hoFNkogUHPtW2CnRQl6lBWPMlbWlqC544+91oDnzu1dSvkshkoFdfNSq1per6zmLAxIvsYeUIngaT8Gss0b94oVmB8Ygw0vBfoALRa3fHX1opPvWSZGDyPyYlxRbAk/KOBvKD8ZHbgKoRW+5bOHYOhiS1w5dU3Qn54AnF6CiPGKjTKG1BFTkIZKqdext8tcERIpHJBbnhKrM2fc32/ednLbqBXNgmK9En1Hti3H9e5DCwk8bSgHYRXZBDAmU7Fmtkg8G8ufja9g5BqMWuaWtjk3Yu4WFMJ5AjDKbALOX5tpVIDlwxIhQHwiavgcdjozc2VKlTKNTYqEyOOABWpyDA5eqHxRQZNiyziWD7BTPyPop2NhlYpMQxqzsych9OnRnblESrt2bUDjUvHCFzVdcOSlUZ5RU/Y/HnR18TY6I11jBrpXBa2TY3AM0eeQX5jQbFUe65cqz70Ut3jyclJSOJxUvr4HyHWC0vxinqtlB4c3Z4+eNM7EJIg+XQbSMSRB7hIxksbUHdq7HXsdBZSdh7koIZ/a8hGdR1R1oZtWAnhOtXgsumyMAPk+71oTNM5PSyZfONREXTymh4bpdBUfpderynyzAuY4I+JryMmTZkmig4JS2P726gj0ccjsnGBup6EBi5pj0m8zhkrgmlVN+CHYQg2NhnBpzC9SzCPDJNSxfSI6kP0AfSZtXoNCW8CiXgSwvpP+eoD+3dkM6ndGp7nwYMHYOH8BahVEW6NbysH1bnlpKnBUqNdMB3IZ1596vRZuP66g+jhh+HTn34aIygaUc19+KW6yYVCAYaHhxFCOi8r42gZCC0uw9ARNtgvNwOhVdlMpbLr6VwOThy5F+FCBTQ81nRuAFLJNN948rgYIaC2tAKB6wDXljRNDo5Mo2dMyOLa0fXn82EIJThN24+D0ILP4jF40mfPnQoJLoI4fj5XfMkkqBYTZpY0Js1M4xVsYCKPZAb/5qkEFKejyXSojkDQjMoORPopAxbgZzUCglyqlsJ2i8ZINR5+o7BQytyHjCdMJ3O6F/+lTJaFi9nHnzm64EcND+beuIZRl+DVxOgkPPiNr7PBYhCa96QDFkJVN4xIU5MTgwlTv3NmbhHe92Pv4WA5Oz8Pu7ZvgdmN4rdfkhuMDmFiYoKdEl2rl6mBICxwm/Cm1x2Ez/3VvQAvHwzIrszzgvGZ40/DwMgkjGzdjWRuALKpQVxIBhSX5rj2UWmUuCpe2liEcnEFcbgnVxfPWgNju4btdCFZK688LxxMnlj2I+l0jdDDOU6DvTnX3ELCzDcXF6qO/6k6Y1Syw8VKUYXJO55HoKKLiVGD1jfSI+QMSOTx2RS5LDwfMhbk6wgZkVgbpio24ovxGnARk96XIVgAYaEU2Fjoc3VopX85g6YLg485kU7j+1Jx0cj5nrNvcWkZrnvNm9iATj13FPmbARuVxpmqg1ERDbwZRtChgewNtUZlmODVgb174InHn0ReY4JuGqvLaxt/+1Lc4KmpKYZWtVrtZWccHRDr3IUleNPrD8HTD/0+HDkyAzOzqzC3uASLS+tw7PhFOHp8ZrPszvf/IBPJ8rZ9rwGvsgbLM6fhxCPnwWk2egsmiJ8pu0MLJJMb1gojU8Jv+pPSbw48n88hrJ7FyNBwnP5pSKFqEknkB+SPyZDUkgWGULrB1swwiSQxHFm4ZB6oUAEhVML/XHx+HaEMRY4GWkSCQgctalzsFGFquFgNIvRRoRCUMUJojGwRnFUWKgMXQiwuSGoEMT3wMNqkLJOPkzJ942PDY4mEedXyWgkO7N8P9UoZTpw4inxuCC7OuIezeFYblTb/GC7k3nf27By89jWvgq1bJuBjv/8J2DI5ikev3yOlX6aM1/da82g0XMgjtBoYGGCe9HI0jg4DoSzMymoJoUMC7rjtekiYKbzRRDZ9wptw/Lk5+MqjS/CFJ9ahjnz3iu0j7N2C4PsbbhKpNKzOzmaf/Npd0CgtwsDwCIxMb8fHbkgj31iaOYVYvknsAJoNJLTFZWggR1lfnRHJWh7Gtx1wixvzzytRf/T4c30hFi06wsfXX3OtNoZwoF5vMCfxEQJpbBx+hyyCX8+EXRkQyTbIiiiSUH0mCMjjc01D1UpUigujiMYwjfkFGg3xB6p/KDWN4ChAi52MASitLNsRTIukK4HiI/STi9dlgAqGGH0q1Tps3zp+JQav3fT5hw5dDbMXLyJP2YCtE7vg2LNn70tmUtBYXuVzGB0ZGrEM7T3zi2vwK2+9DVwkTGfPnobdu3fAxYXSPSpOfm/GQXWrdCoJW6enGa6+HKFVL0kPj69SdaBRW8GbQuTSRcNBLIue9bprr4Rb33wL3Pb4efjMl5+Gz//twzAxVoCR4TzfzO/Xl5VKwcLpE0MJ04aDb3wvem8T6kgqTfy5vLEGpfUVXFgY9i2qviJ8SWQhmR1kz9uslWF94byJcEt/Pp9FNYNLfSHn0NN2Cla1db5eBFEIrzP2J86AN1tHfkQGRQVLLVzUFGOoyEcknoqCZH604BxcuBZbmFr0AcE2MiopVaYMnxPBMg4eUolNOBmgKf7RWlxC6ZgiXpJK2xA0Pag16kDFzQCteef2yaurlfXcGGUCr9gD3/zKlxFecergwuLy4jEDD4aEivS1ZXzkPSsra/ltu7fCG97wWviT//k5vOZ8JutnL8z+OUsLnOb3TswHRzjB4Tv1l61xPL8slhQsq5idXwWxUIJbJiVc8892QW7hSfjEH/8tnCNMHcKM70uVEN95fPdBb+v+Q3DmqfthdXW+Re4SaYJUFhqxCc31BpCQUbCH9hlqjW+5knRWq+srM0VQEvVL3tH9V+6DNOJ1Wtw9x4GLL5vNNjfWN9gYFOlV5FuLUqskkvRD4SLpilgQpsi5H8Ii1yM9FnlNjx0Lycp1dEK+U4MyQh5TV0SeEJdselx9p88mH0TvR0U8KdqVZ1UzCPhvZISK72A05ayZ4EQCcZtcLqOPDxVy9z33LNxw61tgsDAATx95hDNSK6vVZ1zPlUGoQ8PzBCTzH3zk8FPwi7/0AShksvDNe+7mOkXD8e7Cz9sooKOQ38NdJ3nOwMgoFymJz31vcehlmOYNxXFwer6C0EaHP/zMb8DNN18F7//AR4FBgugfTl9chVCGhLh+4Nij34RsOguHbngzJHOIXccmMEJUYHVxHgm5A5XSBpFHELiQyqV19L51WJh5Fuz0sGNatnu5NC+R5J07dnARkCBADweRpPnSuShHKV6uRQhlOMxNcEGa4c2OiHLEEaICH10OLgqy0hfJOD7JCcl3AiOjZqdBIORhOoL8wQjlK34YRbiYGF3X0AgpJxyERqo0+Ur2T7zETiS5Ql/cKCFkGsDrWIO1Yg1uuulVMH9xBk4dfxIGhtBQji49msD3Xq8qXrd1avxdtXLtShON94fuvB0e+u6jcOH8Kdh7xV6YXSr91cRonouxLxBccb+NMDKQKwyxuPSV8PWC6yA6ergaEqxTR07Aj777DYzNf+7ffLxv9VO+RCXRZrWc2H/Nm6EwPA4uEvWFCyfgzDOPQBUNwSWyHi5IErmRwjaRSEGqMIbP30JVZLtcWjFAFbA3h3KWxcS6iRDD7wMZyWMnUxZHl5W1NfLZyNMszr6ACHNWqoChFq+UbdJMVW2Ss9ddxUuQL9UaHi58xeGkyjDhokbu0fR5gZPRoO1gxAla1zKZsNjo3Gq1w++KMKK01PC+gmKUti7X6zC/uAx3Xv1qKBaLyDHzcMvNr4LTJ48jOa6ikY/D4sKpv6SKfw2jjY7nlM+l/u3pM+fh3e96JxzAqPpzH/0FGBsbBM1MPHFxZuGrhUKWj/UF+Tqq9CMUzuSG2aD9MKnwv52BRB7MQfx54eIy/OwH38uZmA/+H38G46M59oxE+KnSfOr0mZfESAaHJvVGdRXu++4X2RATGN4zuTzYeXzIQcb99JnknRvoJRsItYoYPRJWGqYP3DpWrqwOVVbqFy71GSl8z5HhEdZX9SsU6qxrKsEq8p4wpLAh6WiQSuZucHFPC1O/6rwF66/UAqGim02UHV8nGc+DHnB1nIp2ku3Xxe9VZGAoJnx+r6ShRfL9UC8jWs1irMsKOQg1N9F7UVQj4/XZ2D00rARMTw3Dd+49BlchOd+1fTs88Hd3cV2nWPafWFvbOOqHK2EbRg+v0XgdFRff9973wbFnTsKRxx+GPVfshJV1729t/L2hv8Blw1k45GQJm5MTAclVXgHG8aIq6SR+a6K3q88twK6dW2D79q2QSZlccOSUqW1DqVyBxcXFF3uMYmV1YTt5xYnpfVAYmYJEKoP4vAEb68vg1GsMMxBAg2YZkDJTeAOT7EkrpUVYnzudQFd82QooRcJ77/8ObKCXDYtqHZGQIsc1hw6JyYlJ/M08bCChp4UYQRpW85JzQC9JLazMTbQoDYtwyfZgYggXl2HBVVfvhIW5DZifW8YoNwx21oZquQhToy6s4SJCnA+1ehMa5ZoqOOphHcatqewW10u4GSRsGFNOSdeVJosiGSXOmp7LxcVbb7kOjaUGZ87Owo+9/+fh4vlz8MB3/p6Nfm55/cu+7sBGxYV0JlvYPjX+hyeeOwFvvP2NsGPbNvj93/0YJJICuVK6ujR/5n+gAb2gdL9DSgBcKwEijzrByZdBE9Q/iIG0QjveiI21EtQRapg6KTG1Fsl96+23I6lehW/efTcvwO/x+HKp/MDq1NarYWXmBJxCaOU6JV4M5BlNy1RFNoRI64sbfBzRS6f23oJwYeDkyhNfPXzZDBYRZIwCJHno1gPRIiTDJ8k4FQi5d89QyQDPdcOuQWByHC0eyjJx4ZGMF3+PgQLcWgPWGgE8+MAJ7mVpNJpQLC3g+kayrksu7DUDwbCq6XoqAuDrm76CUJRW5gKh9FjQGLUfR5Vd1VPhMx8ix0HHTNm9Q/t3wFNPfBemtu6AQ4cOwde/9newtLIE2/bsg3OPPfanjRBS3nD1vv9aqxQntaQBd77jTrg4Mwv3fvvvYHx8Co+z+T986c3XncbzWxc+1V48GBywYGAgAeeXXHQoGktt+kXoy9VwI6T7ijKQS0GwyEhIZ/Oqm26CBYwka4jdLSsJz6dUT++BRuCtLM6uNb3g2WeP3AuWtwbXX38Itk9Po8HVwcEFurJagWqtCtu3TSLJ3g0NNJDSxgZcnJ2Hxx57DMr5Xd61N7/RsaGMZPMxhlCb8pAE1YBuVzL2rufRYq9WqnJ5bYUXAMvZw+MUYWstQT2CNAEXDY2wkzA0FjZoE+qNBjjlBsINVfNw8HhtW0DGLnCXHoJXNBCCT+hsHB9KyFtIxu8jBqIUMRH11VKNlAJopIZqzMLPk5oMkV+bs5Ctjo8OoqFV4ZnnzsPb7nw3NNFR3Xfv38PIyBA+N/HIwtz8iQZCvm3btt40Usj83NcfOwzvet+74NqDB+H3fvf3oYrHayTy9WceO/o7VkKHRrV22cSKZHmMBjt3FmDvzgw6ySacXWjyNaV1QQ7tB1V0flkYSLTAKdtTRphFXzt2bIOf+ul/Ca+99S14wZYv+1rCuXNL8/DII/e8ecfU+E+8/tWvgY//90/AzTffCG+7/a3wO//vn8KxU3OwXkVesF6HRrACE1NXwBW79nAPg+ML2Ll7Fzx2+PCeN7zhlt/LmPp/efChRwnvUVW9yoC/62thYQEePfwYXLH3ij41EVUzJ5l7A29yvVHjGghFEi0szrFRhX0ZEbHnijolEDQln/KRfFtJXTU44aLO5dNQSCXATlksXAyaAlIWZcZ08NBw0ukEVOoawjayShPWKyHsAtGCK6q3PQCP2l4Zkmks/Z9fXIGbb9iHMHceaq6EG25ERzU3A7XiHGxHA7jnvmOfGBwswHRhBHZum/wPx0+cASNpw9vf+mY4efw4fOUrfwtDoyN4Xdb/AKPHRTqmqAW4X9Rgh4EniSaAkDIJu3flMCpKqNa9VpaTquZUP6IHXZ/naSjft2o0SXgaiAroXwhVDvH09fddzcuddejtTAzjZ86coGYdpUtCDuFx77jsS9iotnHi1LN/n88nbvvh974XBkb2waz7N/BHXzoDn/zmX8OzD59Sd8U38aoXYHVBIvz6FleYtcIwpHMFuGLnKJi5HcYXv/TFf+OUS0VclP8XqLbTTSuCtPi4n8Qw+iQqAfKFPN7wOsMsqqYHzaB1D/2wp4SiZAQjyFiaLpF54NoEpXaNQPVypHMmDOdSkEBeomkEXer4Ajuc70CZL+RUNllWEoo1n9O9PrUZ03sg36KORh2vk6QGpzjxlaoGMzo2hNdgCu7666/AGHIn6gs5/sQjMIj3oO4Yzz115Jk/uek118K11137vqcef/Idx06eg3e883YYGx6Bj/63j7EwsjAwsHj6xMXftNMphqCkqqjWnFYDVnTNMmjgiaSFh4/8EGGiJpv4eegEzLBOFCYZ6HueiILHThqsKJpcJpkjX7zDVnewXGlABR+cRcNTyGVTMDqcg0zaDrscREeG8B9G7k6NSHYKZmfRe1WL7IkLw5MQaDYbT699SEgjDj6+nMqcPZ2BT/3MZ2Hj5GkQCYws6HEzcBKu2leA9dWL3L46PLIF4U8FYYcPuYEBqHkJ9J5FeOyhI3iGWdCMCSgEtYJlEL6Xc5th2WQyBVNTW6FarYRyjd7IVqlWoVgu8Q1mL8g9GUr/xDxEKm2UDGGZchJ4GJrBEhO6MR7+3sbFlEJOQ+SanmBKi9O6pAZW2it8HhUTEWYJSfBMh3JV1V904hqhgfrhoAYlcVeFSbrF6xsV+OE7Xwf1YhGOnrwI737fq2F25gI8+sh3YM/eafj2g89+PJFKwvS2CXvm3Lnfe/boSRgeG4Z33vEWeOLxZ+CJJw7D9u3TsL5W+XXdTpTTmQwsrxbh+oNb4ZorJ/FYnNbCSyE0fOTpWXjmzBrkCUHTpBfRf01ThOE0d63OKXUboz3Je4hTShm8pNCLjJGygaVyHT9PSel3bBuBG6/bCcNDOTSMPGydHoDJiSFuf5aGYJ5Ja0r+gxoIp+Z9XIA2ZLN5WF5egsbCIrz1jh9BLDzC+fl4FDFZrhHAsaWnSwN4qLfsy8D2d+2DG6/aBiMZCyYm0TjwNR/44C/D+NgI3HLzQe4Dz+UG+WLbmRTM42J48P4HYXHdhZlFD4qL8jWvfd1tYmBoKJjBv1GhinRRLNHABVgpr8GBrQ4M5DSoN/GmddXBaLFThKgiZ6jUa63ioerfkEp9DoqPBNwfroVVfSWFp54OyuawAgC9PxkrS9jJeAKP/9VJFUB9kIIiABJ0SdIPHxw0fI2mpZDWl97D0Dia8KLyXWWgeljZx99Tg9PUtnHYv2sC/uJ//Q1ceeAA3HzjDfDYw/fjNUpgNLKeWl04/Yk33XYDSC/xkSOHn5xYWC/Cv/rAv4Dpia3wu7/za3ivEPZlBv7+3Mm5TwIlQaQJa2UHJsdycNute2F+udxyGoN5G07PlaH27AIUMonLgiUBqr2Czj+KJhRJ6N5RsuUScwGeF7SnxEaxVOdIkcsmYf+eCdi9cwK2bSXj2AHTWwY4mYRXEjy/gcbuQr3kcK8NNb3put6CWf/ADVOSFw6FVhLKzZ9+FN722p9ETiLYa0fFoxR6thMnT/6naycWbn/TrbfAW9/5XlBiboBjjz0IR545AWeRhBeR7OKlgPseepy5wTSS95WVNU6lkncYm5xCjrCCEGQF1lfK11cc78NbcuP/bXwq2dIw8UIzU3DVVBmC8iNw/OJjkMhei8Q0hxGp2hEGSVhIsIZqDBt+SbW2hufFRsFeS6NwEdYpFNShzE3O1oGkS7QIyPNTpOD2XI3SswFyFKrMYzRF52sFlH1KcCExmcTPDJCoOwhNmj5zDYNwvlRzskwqHnJmTXlMMrRauQq3XHsFzJy9AE88cwb+3a98GGqVMjzwnW/BG193DUJE/8s/dud2GJyc+OnvPHz+w5pRgTvefAO8+x2vge/c+02Q7hJcc80VMLfS+O2h4SxyOrPguv6Gi/yH0s8ra1VY26i1FiRFUZfUx2GPyvNFRCoRo5wIFVzJCdHaICN5ntArBp+Ai6/reFwWcrkrdo3ClXsn4SY0iCt2jiHKGOIo67gOrK1XEQngtSaoq/uhNs5jWKuJJium/8EjSKeRCJY6f+PbD6CXysMh9HCDg4OcCl5dW4MGa/aMGYF3+1v3HYZj5+ZgrVTGqHAennn6aXjiyFFeZMPDQ1AqleD0mVOc+yd9ETUyNdAr1RBy0QWmguL4SB72otfYWDy1s14oQS5hspehajV56UYNb7hGXiyBF6kEawvfhUTmSkjYJImotkCwFfISyhRRlbrZUFg2ggcUJekm67zYBatoqVaSti1IUn2EuIhQN12GBVULPampJyCp+8wlmgSV8NgskYQm8hJcQtB0UrioXeYwStMVqhRCOKWEiwrLbRTLcOX+XTCYNuEv/uprcPV11zOU+dyf/hlCIUDjDrzjp2d+cTWV+j/1i8cghbD1mqt3Mwb/X3/+KSiur8Idt10DjaaoCel/OpPNDqQSQapScz6WzWz/0LapAYxQflgM9sOBdTTuyGgNsXvBUIjrR6qTkwyF4SsaCWUVdYaRfivpEbT6azq0BGgcTTw/A256w164+ZppOHjVdsgPDuBxabC8VILZuXU2BNs2wg7QH3AW63JfBLfsRB0++7k/59TnDddfB+9994/AxPgEzM0vwMTU1k+ePH1m7c8+//kv5PIknhvilPGW6Z1w9cFrceEFcOTpZ3gh7ty2FdJIIqvlMkMRgk7l9Q04fX4WhjI+DGAEMQcNWNJTH9g9nfvk4IB9uN7weKEn0btX6wZ6lZJS4OJi1XFhFlePwsjkdTA0NIGGWw6lHohP8TmLq6tIrA2oaWoIA4/nDKXtQai0BR49JBSMI0k6691pMSCUQDhEBDwZpnqpp1xHUltzcHHUPUhQ16CZxijh4+cILhRSlyDdV12oaYlsFNBW+mpCdSHS+b/mxoPw1NPHgMSHB/bvw/fQEJpSJvFAHSGndu5isZBOuZBDYs099oJkLwiRqnVONVeri2iUXkoTRsqtVGGVZTXJD04VjB/KpLT/ODyY+ezs4gZYgIsSIyN1Iq5sVBkav9jMZ5QBpDVB442IE0QtCPQ30spxTSgmky9WHLhyxxC85217kV9NIuRLwdK6BzOza2j4FquaDet76135gRkIz2/CBZfP5TBqrMLXv/ENoFlNv/Avfx5uufEmmFucm7Qzyd985x23w6vw5y3TW/FG6rC+vgZ79+6FBx58CB5/+lm8YBlc5KpCTT19tORsvKhju3ZBYXQSGutn4eRzx2GpKBCGWfDV+86xdJCg1fiwhAk0HDtdYLEgDS4DHqmDkQRqMD/7BGvPMpkxXDAVLgqaCK/optENpMVPRbvIC4pQ9KhGukWe3WepiECySAVAkpQTQU+nNfZuNl6D4YIBNJ0wP3oNjG7ZBqW1JVg4exJ0WqwNB44em0VjUVJE5jFkeBxBdJZtkIHSZ69tlNA4DkF5g5zDAlx/w82ItSUcfeoJFkVu37nrGzOnj954YM/4BF0LrxFAA3E6oddGExc6GosgeOGbGPVsNDgXz5mKlRpFcKGvBjtOmjOv+pO/fvSzaVvAR375zQwfZxcrMLNQZG714gGGSnhEI4wootD8LhKJjo6Osp5rfX291b/OMBmd1OQYIoXRLCwsljl75guT9W0vVtDyAx3awIMN8GJk0hnuS15ZXYFP/cln4Cd//Edhz+6dU1OTU/sH8gNoHNMwuXUKnnj0cVhcWoGhkVH0DnOMJy1crMv4ulKxhAsuwVGGFjJlhBp+AkqINSuImysV6svGZV91LfrMdEpHw7K4eEdG0DnUmirnNkKTOsK6wzA+eS1YyUFcOA6MDBdIkoFeq8ItsT56T+rfiAqDBK/8sE+cDIh62Kn3o8Y8AQm6TmSdRpNaXE1PD5kwWrDQE5swtHULbFSrsFJusiR+/+794OBiPX5yhg2D3t9GGEGNfLV1BxqewHNVGax6rQ478RqNoPf85j0PwNT2vfgeATi1JaiV5mD7lhF/xK/tGBq0hnbjsTWQxPgj6EgmdsLshXV4+MgZTkVn0gj3SPRpalyzIafjIOYN8HiG8hl48rnF5vFTs/CmWxHGDaShWnER2uB1tHTE9c2XdH1wr32oa2O1BDonqjVF3CTiJ4S2qDelUiO1sAkvpczrBz7VRHKxxmMIsWvnTgzvNfjN//xf4R1vv/25fVfsXRW6OZQeHuHiXDaHngQ97r4r98DCwiL8zV3UmoAQKp+HTCrNHtv1NdioowdKAIymKlB2SjA2PAa5AsDJM+fpRr49kzYfJDig61okvO12YswDdANBOxrAwvwRGBjeD4WBLXJ2/iKsrS6CUy2zfCSNXn51w0UIpfNAOPCCEIuTYFDAWM6CoawBy7h48mkdo5vJUMQIvZ+UGp+/YaZgcW4GHn/0UVhc96GQ8mB8bCueRw6fG/BoVAcXyVDegBIaerJC521Ciowcvbgb4DXIDsP9jzwLmfHdcN2BPbAtQbN1kVgjlMokAv3sc0cP+jTVkRTB9QChHhpuqYqQscjp0CwaVy5nKYkM1RxJbUxDKDA66xhxKw4RWv39V+0aP/iWG7YGJ04vfXx8JPPFUrmBvNFvyfG/T970e5GovPIMRMbwpnoApNIprpxT6EQeoKG3DO79zv0VjAazkxMTQ9PIS2jx7di6i2sQjzxyGL7x99+EDZr1hF6a5uOS+lRH3D6Q8OEq+wyM51zQ/DJk3CIcuOZGmNy7Bz787z+JEcj7NbyRH0Iv9Pma4x9G+PAH1KREeXtNyKuQwE3omnQx2uBRBBgYLN33XFlcP1lsuqWx1aWLeKwBbBm2EGYJyCOXyCZsrksQP6AIQl2ADCERNo0VEtDE75dWHZbiE3Qw0HAoopAWZJQJP8It5GROXeeIaCdd/JcINUYSi2BdGuGoDwQiK04Ac2sNGEROcWB7GnhGHUYREehwZn4etgxlYf8110JaW8I/1FlRkMRjLNIeILbOma6lUgNWa3gnyj5Uj1/k/ndKZowMJFkuU8PrSfITluILVezUaV4uHmsmnRyY3DH8phXkIKvr5fueNJJfvO/xZajWPIRlr5TdNF5WBhK6aNYk6YwhyTxoBHOtXtPJSEZHhg2ECKnl5WViaHVd0/X1tfWNxw8/+a2FqaVDDzz2NEYOGxoNRy4tLIqnjhxB6IJGgdxhcaUEpY1jsGcXEvWMgLRehrHkeSgXJazXkOAhqTt27Gl44LFnYXp8ABe3Q9EhjdHmp+sN/ceKFR2duDeDEGtHrQofQZ6fa3qS8+TcD07zFoIENBCmFYvnYKSQgXw2wV2ADSdgvdOkTpvQ+JxJIbGgZKmMxZh8ZMCG87NlKK1XMOpUceHT0GodjcZUCw4h09RABiobFVirIuFEOEMNU0lc+Q2/CTZGmIWFMjx1eh6y+Lkefmap1oDpQ6NUnmBFNQskMApdMYmcLk8L+RmGH2XE4VS5T1B6kyI1RleKeGZCgoukulZHmNYwoNLAcKH74GQRVuH31C5Q96kXhYrpHu/RQR+TxNciXD3/l189/Es7p0e+s3fnltKjR88iv5EY2ZI0zJoS3szvNBGEyYSQc7Yyb+L55Wz/dzEQcZn0XfQEwpCNet3EKJEQQm9UKzWfEIZpmcJx3PFy1fXLFWcRvZWXSpm+i57vscOP/vaN8mB+tVjcd/bs8XXbsq6Zmtoy8Z63Xo1wy4Z5hjuSp7in7FX03iXwqj48WRlB/Izk21Ip0zp7WAmjA3Qjqf+bBiRISrEm7ZT+BwRfAP1zNdWEgQIuGq5eq47BVFLnujstIkpo5tJqkXFfAyemJHOOGq4omlJCatxUQuMMUQKxjIYEfWwoBzunCghpgJMBVA+hhWQjRstlbVhcdGBOW1De3kBYtn0YF6YPZ07N4HkW4carx2D3tjTPA/ORzzTofAziER7n/NN4jJLEk3jADnUiUp0F34uHSWK0qzoSPbxkjL4sXMgi/9o2kccFTSljnQuQ1P8uuN3XhIzEa8GS+wCNROeUON4QPl/8PDNfyO5saraYWXGt4UICqZjeUPo2Uc+mzDo6jnIghUOZjmZTVqh9iHhXwHAy4AwkfTZ3QUpVXI2atqMGMxEzGN77JZyg32/AA88C84LvS/PuizYQwv0Ouppyta5Sfp7qFqMROiGR0pK2beLvfFz8Qb1eDyqViqQtvxJ2zltaXoNMxr64f9/QDfistyPfO5ROGIVk0tiSSYpR/F0zaQ3n4ab8vkCIDAYR9JbLxMBh30QCtOlJvIlNJqsySNL9xkWqwUA6Cdl0wAR5rRywaC5pGdy4Y6FxMKwCkxuMhPDVwDaaiWs0cQFKJoc0v4r6uSndSGlh2qDHQo+P9sFq3kCqpiXKbgYkrfBVN2HSJLgFvNdFvYnem1LKNJCZpqAQiZSq2q6RxQRUzUci7ztQq1C0SkE6b0PKtHmIM02V2bt9kqc5Rvsd0PVdRpJOkSSBESmdMNngGujpDXQMPACCUso0lpq2gCDhI57TRkVl3hKGarAUPFNYVeFpLhrVbPRQMUD30Wl6YS88QcI6Z+EszZg8uHf4Y9xZWnOhVCUhZQSfyemQ46DUdkIuLrvI7716veHP2RZcSFqi6DTFmgyMuaYl1lzXv4jvcxKN5BSN5Ar8dp0DDREPRZBwBQ+tSSV3c31jg+ojvCMEPkdGz200/LAfR7zkevgXbSAkXJueGoLt27YyOR0spLnQU61WtNmLs9I0LT2JsRm5xZDQtEogrFIFPRPNdx0ZTGzZv2vbW3Ztyd8xOWT+sO83TIwiPCYngavO0tSwAppenkAY08AbZNuIy3UleExYVIXVubuRFgjPmUXvT6K6pK765DXE92MWwgbqGUfYk0aoZhjEGGiKouRFTxkdj4dNIwS0MpDFhU21A04zOgQRPRB4X6RHz7fUjk26ylahY+XpijSrRBekUHWgQj3hvuAULGWCiFuamoKaDVll7uHhTcVrwXdANxQk8RxVyyhXaGpLVY0VCkhaEigJhhuwwRh47ZokqqRzxPNvuDrXYAy6XtzairwIORm10CbIYInEI4fJZylqIA/iWlGN/0aLmSKszEgefKfjtTJ40opQcNBQ14d+FjrVIigoqap+sVqDUtFlqQbp4ChdzP30aIzoeEiFk6xWm8mNsjPguM0Dnq9S1QTdCJI2adSrxoXrWccNHm00g7P4vEfxXjzmuPKU73pVWlNoYGgwTsJxGngZEmrMt5QO7ac3M1+Bg1cOwQ/dtocjpJ02Xl4GsorYev+eMfiNX34PfOavn2ZCmM/ocOrkiVS5VDYWl9c2QEs08/lCJaeL1NYtqUM3XbX72pG89t6hjPkO1r7gjXWcIkIwvDl6gotVCQWB8IbTDUIugQuIVLCUjm02G0DZTYJPhMEdj+oCGnfcEUyhaSBVr4GvNdSOToSLfSqmlaGsIY536+pOUh843SDC3URM8X014hzUsIQLkaKV2/A4/elRNKjV0EgMvskU0qlXgop1Or5506GRPganVom/0P4dqrmpyTs9kQxDwwjjsMzd4zQ01TEoXaQmoZgcdag4SOUYXaMJ7VQ5V9EhhUZBC0yjFKeBhoXXzEAjJyNmxEE1GUGzgdGJIGSklLbjUL8IbejjcL+IYSF3EjRIjkSPLjodNAh8Lz/wuKiYMBN8bjSADwKXo7GN3wuaGon3IoVGR5kvqv0Q3HLZg+uc5jVSGtd8aHyq5H1QgMWYZFB0j9hRhBMpibMQtCKn5HOHpDaFsWOK+vHLNXygYRfL7qPFUvM7GIEewfO7f73kzBUrftXQ63T8GkWOpTUHrts/AB/6FwcQ+tpw9vwaix9fdiS9WHLgtTdMoGduwH2PnIMnjy5gOIdKrjAGk5Mj5o6p9HUjw/k3DmYT7xrNwasytpoRVSrXYK3UZMl4Axc3knAg7kFFMBKaUfea76kOO6KZhL+JC3DvBW2ohDhA0gRQhkmSm6jolFSg5c0PuMKtayngAUK4MBw0OBo9Y6Cn9wLKC+Hz8XtWMxHBJr5KU9F5YauUKEElLdAVTqapiYb6Xrq0W5THBSmGVLKh2m81+syE2mBUSJaL8GCHsNZi4MrTSXTlqUJiU6hIQceHR42R0+L3p+ig9hn0QkGfxjxBJwUv2QReEzpbijR03DTLjNwrNVg10BBJGqMjhyhHW8AhLSCrpYVJhmTQKROk8lk2qYpzTeCsGyUamjwJ0mPHRPUcbmsmmEfGS7UcAx2XQY1meigy1Dki66bPO0RR14puqboP3QMzqbGB6ZrJ3Ix6ZNKpNIgkHgtegxHkleNDaGB4zA2vduNGxbmxRqphXBBl1797vSgfxFcfXi/WHlorOis/fuc0vP+f7IEAP2N2vq4UDOJlR9LV3KxS2YHbf/hX6Ff2/it3DN72lltv3TGZfkPg1t81kjHHRws0Awq5CtK5hx6fgaePXuSNV6iBxm8E3OfgUL+D2+CFSuGXeyd4zAHwjaWFxQkx6vmgKikuHNK40rgf1kAJ8s5qj0D0/wgF8HIiJvdxsQfkQVnyESivCbRFmQsmV27VFHZhJPnvGi9i9OigtEYOTyJBqILRLQjHjwb4nr4u+H2oiMYjehi4WbzI615TTTtE2EUeWMk5cKEJhGMerzI8ZlqoVGswwh2jdDWogYAU/t0ApRrnKaPEI3xKIZMchsaS6syT0P9CjSAg6Ay5XPoOr0GS1LKCtSmQpKwa50sM1e2n0aIn5bDk8UJEnnW8FtzCqwmuSVGmjWogkiT3FHHAVpGT0FZgqHvB2SnkT+jYKrUqH7+a0Cr4OOhv3CITqOvnhRutcjNd2DtvcPVdOQ5SKAwOp2BqZBiGx9JcOZ8czUMuZ4ukqd1WLa7edv/D52A5o9VTVwzdvXdH9rvPnSs9NDadfWBgMO2UNmj4hsVFVRop9lK0WQnaJz3KRrk8zAYXh0i2JysS2SNMb6IXxvDc5N3MfN74kpqaBgsJGN71T7UfuuOW16PD+ie2Zewbzdu3rKytZtdWSvDaQ9tgdDoPq1UfVpBYnp9Zg6efOQ2ryxsIx2zOwtjICRIEEYwU3vwGe3DSCPFANV6ceDxCGQ3hZEHycF0NLSCBYB0fBmeUKPybXHyjmxNogdL54+/UKPUmY3fgRZgIh043GXYQnAHP4BtMQwxobBORXukp40mllN7J4x1jVRaMMkmSWmGpoxAXqW7q3FdOEc5BL77RUEgOKMqQd+fB1AEfHxuxhueKx2bx94TtdbWXCHpZwRhMZ+eg62gGtFhBdTA6zAkY3Ck1ArWU0HlqSsDICmGh4I+KgqoXPJlospGwRIVSwODxADsjUBBNB3I0vmoGI3IuaHsFg+GaZG8qGZYihuXrpeYOW2o4BUYVCAdMkIGTwzDROaltHfBqC1XkI5hIl5oAIkEuup7kOBAQo3N0WBpDh5hNpiGdRSPZMgw7p4dhamwMJqZygDgXjh87B/c9dgYqxMnw3hRyyaqdSj6g6dpTQ8OFJ8qV+n1//oUHZumO3/OV/wK1ch0NMclydlIikFixVvM71bw1j/tBWG8X6wd5YQYCNLGjQvJnnpH7oV/9wyvStv7+SrH0zkateXB+fgVKa8u8gJoIh7Yieb/62m1wfLYEDz9+Dirrdcjhoh8dTHG+3tTUoAO6AU1O/ynhG4VqFucRPJEq40IQxyHPStMT8WLSYqM+7YYWMIYm/EqcQ7BKl26yxVBL6B53HXJqEBcq3XjiL4SJ2dh0IicBpzdZYEgzfum/JhXKTCDOl0dIqBmSq/QaKD5Bi5F0gxoXBQUfK8/FpXfBn52mgDJGEQPDP0EdD3++au84w75nT68CdxxIqsCj9/ZVRkxqAb8nFRxFbCojxSmaW2WRmA9vCBmIqtYTR9GZ9xjkLMgpEHEQSmFLBkxRxqDnG2pQdgrPh9pqCRtRKpmPGK9tvY4wl/c8cYHGCdBuv3TtyNM7+DeHBm1TURT5oUVjVE3BME0LqFeehubparICXic2ULrWlIQiTkaZQzpPEfIkvt6SU9O86Wi4x4oeDmr18PoJj5IHDiwXq1BG2EHXJTs0ANdesw0mBnQ4c2KeB1fYyHlqFeKcDp4GsjKEfdlsws3lMw/mhwbuueGmKz7387/08bMP3f37yGtqyK80NuKX1EDIa6WsDBQKQzC8+53mf/jVf/qOxcWVDy7Nzr9+bn5VK61W1CaTlsFzohxfKVvJSy6ulnjm5/hgHvLpBJDIjbwtSRvIS1NGgzwaQSvXV9klgQTR4QKVqUK9pnZxUrUJ6remzS2JByTU+BtqOWWjQcNCT0VZFl2Lpp9rii0L6r2Q6NUUZKNXGMQdmB5IxsuBrq4E9wUQVtRIryUYIjU5nSvCCYZh1j703qywDTSeB0xXkBw8rW/q8aC9xNV2B5LDPxltjQfIhbn+MONFDoIIOhdUpdbq8ZbhqFEyOpDRkHh+EsM0nkBKKeNQIEkGQ0ZfyKegMJBCR1aAkfHt4NTKMHPhHDiVKvX+co87RRc7jVcSF/v8hWWeyzo6PsL34OLcIjcdpewEjA5lYQQX5+L6Cly8uIzGpDH8oujeaKo+/ATv7Y7OBaOORhoxUFk8wo60s68Muyop0mmWgpZ0TqpuHHCanPiWIUivJrlhLYlQUedLq/PYP9fxYX5jg2s2djbJqCOJEYHBqK5EjgavPI8hIMFHzU7C5MTY/Xhen/7Ep778/1Eh+oG7fxs2ig47yBdlIAEZCB54ARf36+741eS//9CP/szRZ5/75TMnz++ev7iCVtjA6GIxhvY4mxIwlCDZSMrU2LNR5xzaBZPWIldpaxC4Gu/cymI45gnKe5DHQ9oXdt+p3oqEpoavUcLHpOPT1Oxa8kYsJydDEcoriLDNU2tVMNWITvJOtLA5Ygi16HQt3L6K5S5aW/4SDSAIZTCRPiYqAmuaMr52sSrMvQtobV6uhTl59TxVH4iGTtPfdV10am+iY1WtHrGhASKmz5Gxp4v2rzlaiPAThII7RKBNriXwhHiLdGo0B8CpcG8IVeN9MkoyZkcNsmOSzg1LNsPZcqUIRAcpszU2XoDprWOwulqGUyfPw3rVxb8JjDgIRQNS3RJqpVQ6cCKCuIVLsJcKowjFyTocdH5lt85FTuJ3pPVihxj2x3icVNHYcZD0X7IEzIJckqKE4DYBDGBoXCqKU1sz9aM0mhpvH8Ejlyghg4aCMJ8bydLUFYreqorRz8JFOLVlYnlievxPqm7zt/74s19f/voXfgvfsMn37QUbCMEkMyng0Kt/UfvIr/3E+1aX1z72zNPPjVbXynjCJqw3VGsnwRlaIknd5qJVhoaModGYGJ5TBC3wJNbrPmw4HuezGXhi+DU1C08EbyR5Fl3d8qQZcKEpjWQtlRSsECWdkppRq1a0aB1t5/xbaNViY/OsOhaaaDUZdS68TjmAuMSUABGXQojYnNzYcUQzjOMGIDfVpInWW0VbI4gWrGq/b/QGnRVmZayqtRdCowzNJ1L3qP8pT86aKkMpZDUVdaPrFr1/JBzl/d01tY0bDZyookHQltIUDW07Acq+/VDWryrt0hdcs6Jo5oeNXaTl8pXpoVHSdJMa8gaElY7gbskSIQXH56n0hjD5NU20HJ5q75JUjLEwZ8rofzY63JyNhmLjcSCUs3F9OR61SDdZO0bGp/aUV81jRkLH51uQxXUkET5SHzyFpqkdE9XR8fH//MnPff236JS/8oX/G4/Jf34GQieEjAMGCsPwV1++e7S0unrXqZMzt5w6foEzTXU8kKonuUUxwCtCvRcFvGjZlMZVZ87rJMgQdCTmEh81PDBVuc7aBosOCYdnuA3VZJUqGQJXQrnYp6rUFIW49yFqYg6754ToXsKx1J7oXoKx1RoahuxZ/aEZBZvrZkRMYBkdR/w9ozUc/T3+t/5G1mnk6scIVolYCBTtebstAxHt6yHa0UoZSehEZO/lEB3OoC3r6J7i0W2XanKkZD1W6/myN/jFr7faDkJ1ADb9iJgrT0HGQ41pVAuhrCUN1646DfydplqLMWwRX6v7ikjX6mgwxP0osWCoFD+uNF43A2l0qrbGvUAer0ma3uixTEbSZwqPgbeNRkJrlIb76/h5pPyQGGUmp7ecXinVPnT4iZPfItnzXX/xEVaAbGogXMTCP17/+n8nfvPXf/oXTx599ncvnp81K1UNStU6HrR6cwptxA0KGROGshaHcqJiNrVJ4k1aLTswt+pwJTeDRkFhMgr/JPcewJ8zKcGEyeQGID0cxKa172a4DrQI8kDX4mjt+yfbkwCiqRnRou3yuPFFFy04TcTevzM49F/eMY8uu4xAhlPXo+HrQnStuPhybOVvIbZYN1G5iQ4ZdDuSRZBQE+FcYC3sZmzFTPakdF1b01Sl+r3e2iZOeQc1SUW2J9NL0driTaoh8tARamKQT7UHQGtmFn3PqfDwfaLYHtqI+htvBKTxZaCp7ySrIXhG0iDiNbWm0rbRTK1ijWpmRMRVfcY0lIaMdumi4Q+DWZN7/inNjcEOar6SO1H0cxjKAxedia8k0ZjIcAKMKC467pHpQciPDP7Pr3/j8Mfw0E6RGXz7yx/tNRA+M1zoh279kPWvfvZtdz3x3afvaGJIqgaSxXUkqSBBHhEluhHjAxaM5k2V18bXZvAAEdvBhaU6rFeaaOEAWZrE4Rss7bAxwhRSFuQpC0KDmyOPyfxB5em52twi1+Gi5f3Aob3otWiRaTFvLVucIRogHT2/Z+ZWjF9oscUcN5xumBaNFo3DJQm9gSy+xjuiTdzDdhtfa/FfrsglO45LxAw2QkpaxI2iqNKKMlprZ+vIEfB0SDKcyMHETiUy1igRIWM8LNoXvmVa0RZxVJMhw/AkFx4p4qh6kVCJhOh9WtGrHZ04kRbtjBVyGIJnZABuU225XcJVv7DmwHKlgcajs/rY1EyVoPBdhn/ZAQ0G7BQ+X0LZ8dSuYxitKKLQZBieJpPQOeWdplFDeBFK5TIkkgnYunv7PX/3zcO/jodGW1vLe9BI+Ho9fe/v8kFSKvLKWz849FM/8dq/e+rBZ2/waQYr4rEGWnnV9dk4SBBo4pUmefZgQWcRDbHnQjaAjaoPx2dqGB4DjCwGcwuqBJMadjivQT6NhoH4iZWmkXGE0wYJO2sh+daiBR4WrbTwOW28Lzq9p4jUoCGW71BNi5jj78X00IdvdBhJyFmkfHE1J7npWhct8i9j9iNlN4+5tPHEeQ9vshteE04SaBLiQLB9DaNJiGrOFl/ryKlsojzvbC5rR5gOVS0VeJsqbU+L3GNvrsaRynDLONlpjUyueYiGUkbGiKCacuh7StpDUqTlUgMurjdhpezyZ3FChwqTaCjUljyct7mFWQaa2m4C/0ZaMxJ2kqiSjASXNTet0ZAHgweTO1yo3bF395Gv3f34z+AnH6GPv+dLHwVjo1LnE3vdnb+W/PEfufXzRx977gZaxF4yg8THY+LTrAmeyEFkfHQwgQdB42UCbsvM4MKfL9Xh1EwVrdKAkZzJqlADjWNoAH8eTLKRULMQKEahcv2abBlEZCytDFA8GoQEVC3UKDooEqZFUELImEeNe+q+m5Z03nnZhkOim113cXm5mVH14fuyS23QHTlkB02X7UPvg+06PPpmLCl8sYggTQBhIS96bS+0i467GXMomuiIT+1sXgRHod3oFo9iMoSY/NAVrBOByiqJcHcshmm+mjwfydppZWrcmCW4WBm09lZRUE1yikzyOiCd33jBhnxWh/lVE86vVrgHR1Cx1VIp89V1kiwlYHokwa0J5XrA7QdoDrCByIaErcozG6y1S5kBZ7zIiJdOnb/mbW++/o8xkrwT322G9Gfiwa99FF59x4fFz77/rf/9yHef+rlayQU9bfHY+pobzi1t+KyGnRi0YeugGrNPnW8DKQNOL9fg5MUKpLn9NJST4/c7hgowNqx6IgRP3NBai1uIduiW0Hb7URpVA+X9SHukaVpnLkrE5sB2wJLYIrykwxWX3A1LXsLzd3j2OEPtxx+i50VIUOvmQ+3zgfj5dEWEFpzpOrDWcyKHEUWZMO0bN8HYRFKQfYy09Tmiv5So8zvZjtbKw7HjInjVbPGPXkgKERQLwmgS/pFT874MJ5UAb+8Q+EGLIRHsaoY9JEH4IMOj1O4SrtUzK3UoVhuQlBZI0+dGOs+jDCnA1skE7wpWrinjJVn8SsnhgipxY4u6Oy3B2VaS+FACgZQEe689+KW//Ov7f4R8hzj1yB/Bg4+feNvn/uxrXzt//CLPqWoESLCpQaaBRhKGtpwtYO+WHBQQrxEPoG3YLq414NkzJa7OJk1LcWskUFNDFmwbTfNeGXTFiF8E4f4ZJk+va/vdVloz1oLLe+3xpjiCC2Atpy9EF1cWHVBAi1mPgHbkYe8HcRIf5UnjvjJaTLJvOkeKXojUxtV9SLVop4U7ubwMCXCfRFfEi3qgoOx4WkTEo7Rub7Yu/lrRYQqyta+h3HQom+w63I5vY6lyhj8kFwmUpJ8jRJzH9xKt1ssjeCZkdH6B0onR/GJPafOCkPgH3GDlq9/7Cm4RyaY+nYWKC3NrLmyUfC6eUjqIRrZKQR2dANPjSUiikVTrlEYCJPwObBQ9bntOkHgSr2XCRIpBGxkh8TcDFxL5LAxt2fqzX/rKA58ydt/0C+JHf/hVH5k7PwNmMgnUfenSgROmo+qlKznTtHUkDYO5JFfK07hw15w6nJgvoWFoXMDhLY3x+VvxOVuGSFNF7Z4CP9BguTaRKtbiqNS2MgRdNRZpofFExTX+XlccRFXQZW8ECBd99DotTHNyE6MWgwFxIi66OIboikKXwPqt6NHhzEUrkyU24dktvC26iYzozJa1hhIK6JcqgEukYrv/GI90MsYYYJOF34Kl8vLdoxGlD1gBEHIMJuOwSbYO4im/1mfFnUC4FxfDMR05hUl7iHiq65A4bRAKHGkfFUG9K6Fno8U9mDJYmiSCMqxXBDRJ6oL/USRpOjrMrbiwY8zgdUqqjYKNxN0RUGmQ/J+3P2W9HamTSVbURMOqL62BYed+EQ/rM8ZffubDW//gj/7m2sBD/EcTw/0mS8x9j9JoHu9DMTFkw/RYgYt3NDM2m8rAc0tV0PDvNNeJMlVNfE3OBpgapH4Otb8el5p0P/QQakhaxDfUgAMVTdgwSGagi3Cxi3BioGLorfEuYZccE1BdtLrINK2TqMfhx6VuesuviqiOIbs8qGhXKgR0EH7ZlRESPQtfdEHA3lpItPmN6CoMio4lLTuq+a2jljGPDp2QRog2b+motnQVgdoBSmul1kFCXyuXUcYKFEzyuftPKMVLeE+lbF/Mtq2rVkgWUspYej4CUaKVPggH4EluF9AUJeEMZ9MNQsPHFaWHqWhpMHRPCQeyaKxlj/ZjVDL7gOsYASu+a1Uf1osWTI0koVSvQQKS0MxI7rsnpTG18VKbNLHjqAcnkUqB55SvvfPtN77R+NLXHv23NEKP9v/mPDOTKSWio9BJ3ZmjQ0nIcG+2D2kriaS8AbMbdR6nqeFzyFppvuxEIQ3ZNEUPtf+3DNOLegifNBFlrbT27zT1GYautdOvrGjVuKgYyUw0aBP3DilIn6q1FJdumhebfRfzfm1OIHphVC+aapN80TaQHvvoIsAQNyTYvKrfiQK1DmgkpYhlfHqqd53vHUvPRmlWaO3DHlpHGOmCMHUbyDYcigqAMlyskZGTVEjGqZjs4oYMKVVvjIxBYhm/zrGPZ84KrNRXIkb8KFfnCUysd9M56wVczbF0ypDaXDykUUyrZY33a+GJlbguCf1s1B0Y9ZJI1pNcoS+kTDSWJpQcwS3H9J5NRDomcpeM1WRZi+b4olKs/XNjbXX92nq1rlSo4UQKdVFI/u1BNmHCQNZmaEVNM2Tdcyvr4DsBcg+TizvkHBJJk3rLeXIgRMW6UAclopMVKpSR2lMLo4ceDlIT4Uxb+pm8BkUUrQWVZGeBrA/skP3Xbw8pj79a63KZvc8Tl9WfxIuOIqo6yk3giWzzidZ6jJXrOvOochMrl/2HgIiwwi1EN5LrojmiM7GgxzFkq/SnDCQIowWnazvrQR2OI0rFx6JcVGhs26wGnb+JC27CGBJmLGXI0QIVrlijp4eoQYb7rICIDJjUxRIG0j4UshasV5W2Twaqz4RkNjShpVRvwChNhaEGsAQN3qCxTa7qFg1UX0uThd/UYuCCQONZmlu9HhFfMNxwaTszgwWKKuRL7r/28INI2s7bD5PeCkFbGYl7sRKAKZU0mad7IDakgQhUgAGIBhqL1pABCHdn5WihKShFsm3T0EID0Rl76roWQqf+qzCCC/2wt+jKRoke+t0HPYjN4kkbnnWs936wumNhihac7yH03YXoGIwS0LVIRTc5lpuSjg6sL9p+uQd+dXySmgzSv/6jIJEWpeNZwS5pOApYkZiylZFqG1Agu1Jz0Rpg2iFjsCrO42TrKmixtAs3mnGLZrRDcMBQjAbx6SGULpG80YlSy5Id7wgayPIyEvYmTZPhrmRu7koEFhcOh1Ima/2oRJHGtboh1PBy2rQ3ooiUP9MDk9Xho0O5hGGZWo5ClUvtnoFq6pG8LUCTI4NtmzzAQNOV4pb7BlzBi9sXSsNEVpg3ASOKCDdy8VnTpTIyIUjVtFaNwwzL/lRH0Q1VSdfE5nBIXmL4XL/FsslbgeinUIzrF+PwqMXow3x9P2Fjq4TdN9vc8zfRF56FbEbrIyiLahbdvKNP/GxXu2U7O9g30wAhHOrkVrKP04hfZ0uHrvqSOi42EE/yTCz2wC3C3gUxhew4/jAP12pDbjk+GdeZKakMtzgIxXlo3RUyFqONdaBdrNQLCJXkkLDbtoS1JvXYqJ4T3oYbDaXhNMGVFiQ0g1sCDEvNGmhyT7zOa557enyFcpjjgMga1GdPFW+1pbBS83rUZ0AqSo0EhiZLp6nlUwQNaDSr4IgmmLSvBrXBaTxxACyMMgnTVlBDilD+EUGPaLiz4G0D0hmLJ5uraeddEb47LdixDDrlIKIjCdMn0xPTWIlwZL6I6bWgQ2Aou1xpfNVrfY5N6yjytSQvXVEozk+E6EoBx/C36EkAdKZcZR91cLs+Ilrfy1jNAWS/YqPsqK3Etyxo84Mu0t7995hxEZwxacBGoESJ1ChHeipO+8q2PF+ExUwRmUJ4DGr86iayZ9EWRgLzVtWcRoSf6hjkbNdkk6ESK4lpLBNGBhMNx5Q82Ah8ynxReYF2XyYhLTljT7SgPTXGsUSfOzbVpA1uzCbv7/u60Wh6a57vjzNg4nQai545fNIkc4Rq1IvCqksKe2XqTnNUyNNYoii5WYa61ah3k5ZSIGJQR0SSB8GDogv5BCRsTfVLy1jmo+M2yJ4IIfpAoZa3DzsT41ktLaqrRJLwHm2UuJSovSOb1FmNkD34SbR4qYghGdmjJBZC9Kpsu1w9X7t4ka/DcCI032ZRcrN4K7tRfqxm0+IHsh1VogyZgC5DauuvoCVeFF1JEXUPuPuUlNimqmVQitbzVQVdxtLiIp58joqFsSKmlLGsdw+fVMiE9/pI6jCIv9vQaHi1w8kc0mTRcIogHKSnUeMJ/lz3aGwSoqGk5OEfFvUTUaQgAxGa6uqR4b4uAXX98v4tTYwe0qGpIAFNE6TddcIIQBeIPANNQOcWSU+V/GkomxGoJiayTuoDMJIaGxIXrmiRyrgkWwlCiGfkcwlIptT2au0JX70MuF1g6yS8UTpURE1UWlvJKkK1sB7qvFgM2WFybc8pA7lJhV20o4CMpVm7iweik0BL6Fcc0zrIuQCI9aPEzEtegvl3pKFlK2JJAf3jixB9RY1R9qh1oC1JvGgJ3USHwcnW8coeCNeOUMpw1DgfEbTHgIgg5JgJtekopWlZm+XHAkVLdyb6Jd7bVRsRl04H7fWhqQYzqm/kMib3jDTcJo+2ZUULz6VH/iFVU11TBGH9LYn/NpToiabv4zOTEITwOszc0mtpPRuaNDRDVGm9qlmqJreXQhCOmbQkj8qkk9Wlmu1aqZfQMKjjW4YbVdINM1murmmyg8HKCNMHaoBy0tbb4X9ThiHCqkk7OigxnUr5RtEhYnOih4hH8CLoqN72i0Rx4iD6RIxOhh2Tssiu4ofcPKXcYV/iUpouGUtktfViMiYtkV1qsc5o2B0B28bT+v8mxyfjSYkeTheDkwL66NOkQhih4USyeIZXBNtx7ViRodCwODaWdk2F17lsqzRF326eSO/Vlrto4YtpP0daHzb12yPiGS0EcH6hDHW3yVva0e7Z1DdPgzCoUY9WLm1MrJypDHvgA+Yh7QygogO6qXmGnc8j0V9T6TRNCydKqTM2NOrcCufPUr646fOm9TwQzSJCo+Yw0YSPgAdtqMyUOuk2AKFpH7SjkiY6WoS6ooZojZ0RseKf1oJJok+fhuztIJTQk0IUPexZ9E3T9paoRX/NVreEQlyi7nzJ1LHokZ90uFDRto7Wco9BxXZUEX0ayEQXHY776Bjvkr3F7nhiQGwiLYjPzpXhB7CYVFeqXS00EJ5fHO6bQiUAK6GG7FHbhOupnpF2djt+Tp3p4ShZIuPgkgSvYXHZV9P5YSCFvDltIuQi+/G5152YtaDMlo+UgVp4GxbLqXieGHc+tiXjZqvlnzdjWqZOjUrKTsHGuoMf5KnBCCRL5mIdz5NRPd0BqIkQhoIomm9wiOMFTDNhhWRhWEQytdC7k9enfeESCb2LiLd/4Ko6yeD1Nn8ALZ5RkiE36ne75CWEEaJvxS0uhY+q2e0W2s713U6XxpKlm0pSRJfyCUISupnCWHQYkuhB3l0tsS09omwtnxaXENAv29GKMrJH0XwpYb5sEWTZN6Eea2aWbbFavBmMf6urlU/DGQJfGQ0PvksGqt7l0ETNIBxE0db8s5F0BG/RugBCdDpVHtJnINn2af/1BpQrNXTkrpqYo6mBd/w8qXhzIZtGB1+HjTot30Z4rmpQuRZmsjSeWUAzxbQZI5lMnikUsnDhwjJ6fys8eTXoiwt5mhJx0aSNuu+pieDhTSFvQQcQBElVCDR81l/xdD1Q6TKa70rCRtPUYkC13TutCkB6LEoIHtXZIT8Xog/8uZxuqLstVkRl2i4RoYi9f59Ucsx4ep4kNqnKd5NxIXoq2iBkjCh3K50u4bNlWxbTkecL+h58R1SNs41ufVtHBBadhraZY+su+nUmAmIiy7CeQoMiQu/JrdXMIYVk2bkfdFZqlZF0wcXoXsqgDcniswF0jdcftd7y+D3izk2VdBKG2r/R0m1IGg2ee5Dg3ai8druEUNV5vjN4wIPDuceMidGRe4vLi/+66XpgJa0QQqgp5Qn8AJrfyvOLDFA9IT7tjqTx0DEa2hV4apqE0iBoPEOXSbimNrZnWQnNZxKyg7waAkIZidZhDDJUv4suT9+v5bXNgEWPcKRDOhJL3QoNOnrJLyXMY31RaFy9oX+TrqIeGX4fsxZRK2q4KEW/hd0vGkCXxKRPn8imev14ibVLhRwj8T2HIrtzi51XoPO6iA6qLbvgouLVYbQRSpxIHX6UVqUuQIoyMi6d7oC6MW1ZVJfSOJfbOjDiFTzHgLJcVMD2VP2EViKNeKKBh5ruhSOLXObdZEQ0jshS84d4DlgST9qy0ZAy9t3GF79673cP7ttaxpiR5QUkozE50Kpw03wsSoXRqB1PUzOkqAZC2wYQvuQGGDWwB/91GPfJVp1NsESZh0drSlelh5IToWl9hHF9Flef1jbRvzLXrivELUQTL2xka88cCAmbyrv6bAoThwh9UgNdshjRznIJuVmRf9PI0k+5uykIlf3/KjryYaJ3S7runhHZOwhDhHoskLL/61ojjeLORmMoQySeRII0J83zO0yhUwkn2hBXdigRZKub1A+1ZRaNPSVuwVP+1ShXMk5LU49IfMl7H9L+kkKteeLYmsXjheY//cffeFw7efLcaiqdXEjYOo/xDCCsQPK6MsIZVMo4dJonS7OvjLDJP4h0UoHq8dCDjuUQ6XM8L8xutEJZVHGPeSfRr8glWhkFEesZ0WJDHCLOwo8oImlae7xNLNPV9yH6PJ6PBfXIYGRsOlUsJyoB2tRSdjR0iVhHZKuvvjOtAC/kUPpy9J5EtOxSWcYFH2JT1UL8vTub1rqdWPu3rSsfLwa3FMlt4k0Q22QCL3iOGlPfUJelHGmkc+vVy0W5FU7v438N1+OmK6IEJESk2WyK0nJFHEiGK8PsFU+15H1elOXz/vY0dM6gh0WjS2nQKjSrNeeJyfGBPbSFLq10igrCVJGE5sVSAREwMkTb8/Jscd3jDELgGSEm9lrhLyo6kWyFGlNIHEYjKbkthiIUG4HfqpGIvhmpXu1U985DvVmlzaTl4jLYXna6qEstwu7pIv26pOJuXYheDZjswIidHX5x5ywvPaerT/6qx0g6NFB9enrl5hcNxGaiUOivpxT9+lBaejwZm5QSU9UJxTVoXVGZgAuBYT97a1pKzKQirtXO5Id8WChXSmpdQjSBFiYQaFwm7TOvNRnWm0A6qzo3YbFGkDsZNR5LlaAJnSYVOhOQyaSpL71OK9TbsWPLX+SHB0A21Q4/vohIoAyzWA5HlmiOq40nQ620tP9DyjJURbJVSRbhhEE1pSSVsXi/D0PTWxMMW6NiYv+2+KqI+bkYMO4Y+6OJmHq2f+4q7l0umeHqAf593qgn4oiWtB1inK2jxN+Rnu5oUoHupld5qYUqnl8Q6ffu0BMhRY8YLB4RWvMAOjhcZ2TovL6iJ5r0aCdjn60uk+jqEIv6hFSNiyIJ9YK3FBFaGAED1ZoLUra2gJbxQiOPNtWU8pi4RthbQQPMSRFFQ8ZJwEjjZ4WXCHVjoWiTFOmaqqjTDOpE0oZyvfYovT1vf/Dwd599YHgghe/t6a2mJlDKXt42gDZhwQhhNT0mVjSxjtSQBg2IFi6THFM32lYeeo4kKYGpxB7i015vJ2NFxRAPS4g10chehawQl9zoUcaHH8Q0P5dPDfd2um0mn7/897EmrL4pr7g2XPaXp1wui33JSBjPKHcaoxCbyqChrQTrLDhKED0tvH2yJWFUlR39OJFKVg36Du+tVA1NIhzEER+fSuvaNNX70UDyqGUiDCOK5Edpu6DdB0O81gUaMuKqUVT4Hgy1qDaHFuIyKrLwGQ44SHaop50m27fkK5ra80U3aAcvY+nue5/5NtOEuTPfgAcfOrKGZOUR3eLZ5fym3HDiq2IPTe62aPq5JlnlSw0mGtqWJcI+YE7ntoeW8Wt0NdFO6XeCPjeovXj7DSWAGEVrTfgTfTx9t7fsUAPGi2eilW4VovsRejAhoCMwxHF0TzjpGhC3mQuHThfbjirEkfT2IzwG0fNZsSaseCNWF4lqN5GFE0Xi00dic79E32OLpVNb1+gyXOeyA1U7IWWrw7EjmnVfVhWJeQ4zDfzW9XanZzytLdu9Ke1WABpX6kHd5do+Qn9VIGQlMHLjpuZCxqKec+TaVECUavwpxzDNUN2stJeKQSpz6yz++kKIgsiKoLn/wI5PnTt79palxUZrY5Rm2LCi05hjlqkbLPKigyh7dWgQmdFMtdcEZwQ0zkHTV4JmDplaGApFJ3UT7el9mujNymzau9Fqve3WGvWTkIh2M88lc66dGiZxqW4TsXl6VzxfLy+7Ob4GHbhT9NOBtDF+O8knobs5d7OI1tlD01lsk/3S3THblLHJiLJP2rUzFSB7M2wy1jIcZffa82y60vexcyLHS7v6cuugMvlAxAWUXcgAnYzraVCluVwYGSze21Fticfr0TAha2n8tyTJp3i6YsBJHeoHMdmQmqAhX87kMk/hSxpsIJal9nS7OLtx39BAQa7MnhW+nlRBjKQlUm240kT8N5TNQDa7AZVSkzfEpN2TaEK363k8hDjK2tJNN5N6u09ZdLakQmzaSOvmia655bJTtCel1sMpRHc9o9eienE3bPaj6MrEPN8MUvckh37m3Tljqz9hj6UxOxrgOwfh9va+9ze8jsOUbSWwvFxgeH4g7hKvFC3VbytJwRBbtqbXQ2cw7Io9suUIqBWb5EykCo52sZC9om81Ioq3oFD7sHDboSFCQqupaSU26QGTzFF8n6YtOlHNkjdGpW3nbJqelcpQT/v9LbuT0oK12Yfh/gefPJ/N5WZpDzktrJLRJja8qaSu8eC3XFZpWWijl1zagLGsDqMF5BmpBE/XDqv2HEloXm9007XYAmz1lWtae4RoSO7jmaBIZh20EhfxXmrYdDg0xMaSio6hU6KjLTYOsUJZcFc2SlyCAndBkw62G+a3NYg9QumMJvrDsSjd2pP/lXEu21Ivt94vfh5hnkB5qRbrVp+vQytdGk+R9/7XN2sMl2zsF5vMD+tbPBVtWi7aSorolESrbUELZ6MBKzC4fhYbR9vuGg7rH5qaelJqNMBt+JyR4gkomqqI0/nTtg428mHako8GZ9dopwFQO5bpQu3bQmvcTCQufuGuB75Jx3z3Xf8PaNR+SFXxU6fOO8lM9tuFgRxnn0jbT+N/aEp2kmbvZtQcoaQZbVrz/3P3JcCWnNV55+/u233Xty/zZp/RjEYIhAxCIIlFkewEnMRxHCcVJzgLuJIyTpyqpExilytOQqUITihsB0ICwYkBg0MsY5WMw2JBbCQ2yQKhdUbSaEazvv29u99e/5xz+u/uv/v2fSOBTLlyVVfz3r333dvd9z//2b7zfbEyFNE4tmo2nwSV5nj6jGI9y0gh5Blrh0gZyVPIskh4eg1tfkPkegkJtDqCAoh2LO7XebEKk0nFEur4Npt/nSwpyegLNT30lIkiXoRa3T6B5RtcwlR5hmGUNELUDpsahPo34V1V10kqbRPQ75Bv4iQePOM5MrINQh8PGDuGQql6jz5LkZs0QRpnZetS2G++D6SG6PQ1QWmyKbKNwFTaiqmeSlJIkPrPrL3HNdZef8SJN8lmQ0QFXZZFYh0Umowl8VgXfxu6A9z8Pf5ODJVDs9yDyUzzz+Ibr9HHuW6XIO82V6nodvLE8c9MzbWYE4uMhLhSR77HLIrNZpXfrFm1FQeWCy5RzMtYEchU/Q9K1OP8w9QSMZHtAOniyqDrOpQ9gYJkoZHMZuxkrt02Piy+R9sCRKFdl0ty5XiFTDMI/S5E5hUy/uCE16twNzXOrrTZZWRaIIapvqRYmctQvwv1c1I4MCAWLWW2DUP9zNc0infKHKleSg2iVaaMHIQoM/R8Ew5yDreAeBYCigwsIgXiQEqskQNQFgav8hh2kV1LnS9YNamlVtWi5iEdY5SM5+pJPZmVjFHmNMkY06gqESZGhsRsn80aQdpDzJ+p1mWpvgtwFz0Z0yA1LTTL+xm8RQYy8Ik9vpKe8NHjx//0qX3LgwvnVuss2IleqDdAL1Hn0hfnIy00ENLBoxnkIUkp+0QFFDMockJHwvXc94ilnpOqiGFk/YtcRSW3Jse3+KQvYqRVqQy4nYeoG2PNsmJCmsdlyVL4uw73GpcvyE/46cmsRlNSkhcJrRcp09r9mFmLpPOeBNyhespUa1pqeayVA4kIyA94FelH87mO8s9S7pFfjDczZY4WSY59PiS5RhG8lTYRxwfB9E9KJhcjjfkxzhMMLhB5bqCMxIjXmyot0+YyCoaw00f/wDJ7hK+KYuIGykdCTLxrdVa7CiITmriWHUFz7SNoOIK1MQlx3ppurd77pUc/nlwFCsssw4oXVm/7u9Ccu3n1r/3lN/9R3bF+3B1EGKsJjtdqFZu5riRm/Xadqlkx4taRJqsBhWgo067DiMx6w2YlUQkZ0E8nUsuXUkV5BxjymI/x7nmhCyjGAY1C0wvJRj11/JIopdkFrQmWX+xaJUuUiM7kWBHFGMNggk3LoFvFurY+FRgp4za00qZIGQVl2kMxc38/XpCIF3Ik88lBVk0qqeTJYhFA5shWxvHSGmhR5GdJNJufQPIltVaQzKpTUs2Jg5orikSG4mC1LEV2LWIMF01ueIpLmhR9LZVbkHgoHRQJ7ExP4YYfxMWluVoA50hekJwDcwsT5CTE9b/0OH4kQUzg2Se+GDe7mT9Xuy0uLf3ixmzzx9u9HZbz9TgOI2WoKn7YCGZaBroi/DA3UNxVMlb3oaaMoGGVKtewaSY5SdCFKOj/GaIAwc7HtUUxmyTBS0MwKIGcaItaaDF3HislcrWc5Is3ChDenPZG4TPG4RhlPLvjA1m53VvCHuVZvQRrQp7sSIMWiiK6V8IkzhMhk5arLExXanhgnQ1RQg6Ve028S2FaX6idQKbfZ9mcfB7IGIHGG6yVclmVit6Lij8Vg0V3WLFKwXctRUk6GgVoJMDhKDWzidiBZpY4J8Yceb5J/Q8PUwQDtoaYpJMWvOXEWpxBDxqtJdi3/+gnk8OaXjyoEMKiAsk9GpyHL97/jReWDhx8dDTow263C7OzTXQ9jkoOAZaaNZidqmK8Fwu2UVMnUTylkVrKP5LRyXxlIpM6mFwYERqdp0a8QE0cSyXABUWoVAjH0FgZU0myRKIM0vcyCkTZ8XBWTK+SkTtkf5/n9hXp7HICkQAovqfIzl2UnJtWuIA4LdUwaUIhGLL3z4kFqRhdiKx5ltyFoTG3aEWPYjgb77pSY4IfL94Vx1/EJExLwYXmUqFcE1WU1gBS9SpNySrLW4RGYwRqwlXw7DmJg1LgU1E1DNOooPdwWZGKcIOk3ssSz2GM7phqGrAw5WAk1GCWno2tHVjf8fB3Mx6kCn2YWz747Ps/8NF76KMunvsG5h8dvltEM5rc3GAEly5dGf6Nv/4jP1GvNX+zWZ+6p96qvttohsf8qxfx4OpQa1iwNFuDtTWli62of0hHJGAWPMEeRaiFmX2pRlauL4GIJ4meUej46mTUoJHCJc1CoaUgQuOhSpNS0Co9KeWM1iMRhdSzNO8QYw1DY69uwFgqJXItO50rSo6lKXK8UShAa7CVgCtFebNCnxnJda6150snKJPQSGr1izIg4jjdb655m3NAZS+WIiWhS89eSSMoGXjFshilYwtEgs6kEJIpqiFgRSoD8w/0CkSNS+BYNfQUkbY9/tlUqwqzrQb0eiN8LIL55aWnZy+7jt8fHveCAbTmWuhdxOcInEgf88zZ0+khWrn8NNYlgP/8oU+cx39+7I47bp1/y63HPtjbHXLpNogGUDfrMN+KqynURQ+prIZXr9314cp6HxYWG/EwjK4AJTQNAVFg80j4XUU2pJR4noQVRd+NUjIOQ+QqLqIAy8iSaENTQtLDJiOXReYI3qXINyGF2NMQck07OT7bDYUCqL7ORbGZX6gRgTbtloeSF8rTIg/uTBLmWFggYYkU6aqXCgGbsV1DfmsXkHJWyVyaJEube/kLEuUh8DLLUXJydnK8867rIuael6B6I1UIhMe5SdxQjGDojmB3tx/PKZkxhU/AyTvmzLjEjy3Ns+wfaX/Qf2//uXf+r3c3au/5tff9l4+f2x3+/er88hP3/N79/zb5qBMnX5EZiCHMse/d7582P/9/vvSKpx558P5Rb8fs9ByYqddh6cgRsP0QTp2y4dtn1mA0GEHFdsAiRdIwhPXtPjdqag0jNn9h5Hu3ySRdQTTG0JJ2Q1/8mtoUpEhQkdMZFDn0afacPsjEAi9Cjk/7FStfaQgkx/KbomfIzQzpHKUF5SW9sS5k2TSWKGmTy/GOvCyvyskcwHAMn5yGaNnsukiFNRMR1JzTlCoBTlhrhMjt6CA0tn2pQfRL2FENbdYnw2aKHLPN2DCXzGA3eh6SHB9R+VgiBM+MmUCtyIEehkKdoQcVEU8Y8tw7VbMwFhuRdjoaisvKuTaX0Neff+zf3fDDd49+6ytPvgvf8mcAnic4STdeK+v5PXC482Ru16rOzMMf3/fbd37u05/44xuuP4VxHlrn9i5cvNQDp7EE55+9BDffdgSeXd2Brz1wGqbRcGgQhcTaZ5s2/IU3HIC5hTqfmWlkeoTpVF+hCiSUlJqh2A+TPMVIQHdGEqqVdI9BjBPCFSo5Mi0XSm2kCQr/H1+kY4YtxpG5Yg+C0z2xH9rzL0378FrvLfO4rlylSEJRQDP3s8zPk0spc5omejKfI0KM8oKfGXpaaB5IpqFUKgIaKfYDmX1elMDYiYs3YZbXgI78p7gRh17Ijb4A4sbrpYtb8IVvrgGpJFCIxaO13CKxMIeuwd23HoaV6SYm4x7sbI/AqwTgV6fxPWruO97xd37qpjv+7r3JqT/39JdyV9SSDPnVbvjBVzf7fdKG62xfheuWGzA1A1Andvf+FTg3XIPdjQXYv7KCL34KmjXaEajCFfAW4w9DqJqmGn3MOsCGXmXSGAI1sElhQMfIe4lE7dYoNLP0hpjQpJ/18UxOxJTjLyxsveSaESGM92VEQUZaXmvqT5RU6nLPZ2N2e/G5j/kGXYxngppULltOVUg1LmJtBj0ZM4hkUuWSeRikSkaKGkPpaxL9loyXNIWuy3FAfL5tFUHKxJm7TkbMbiiyWeScByS2dxFhmE8aJehNdnZ9cDF7sGomtx+Y4NAVMD3dgr/5F2+AZW8bvvvdc1Cpk2bhCKYPX8+4rNOPn3E+f98f/i18YzaQz93zn+D0k9/NG0gxwvKHHWi7xmg3moaZfgdGQBJWDlx/0IaGXYPFKQOe3ZDgtGzYh/lG5AXgEENF1YHAH2J45vObRrYPZmSrkCAqhCsi38ROSrdpE1holRBlHEYmsabDJIQGv8qNYeo7vTRjBnCpN9pEmvNMWpYZ0NIYK90YZaDHXMNQ5FoLpYz1SThTFLsprdZGObgFiAmFhXJ193Sh6zakz84krb94rCCb5ktPNpJ5X5sga3XKVT30VIs6Sz8zIvMkLIvL/QqcGE+UZ+tDaE1ObppmmxhVNA0FsvJdFy5vjpjQqlWPGGNFDUGalZ2frcO0OYQrWz0Y1mbw7yogcbPf3uxBa3cH7OYM+PXZ36L3fejBT5Xuc1aM5tIcSCDh3f/yP6zdfOPR55rHnROvNBpg+h200BDcQQ+mGwDB5atQD+Zh375ZuPTsGlSJotSUTA7c6aEL81ywGibzbBlClCPaNL7aHFWoPi2oeLISSQTDEGkDLVOFMvKVJs2b6EYii2GNhD3CJJ0mVBa0EEWBzgfGq0RFoRzDKCefKMgA5GxDiqwKJYOUiC+HaYJyJEAR8prWzpKcLFXETTPd3PBSGpKaCVNiTBgNOV3BzMCk0vZIOcP03EZCplybNkxFNkRHn50oP8s8CQNv3mG2EUYJkoJHK0yWfd5cJUJ1H6Zxwyb2RvpcohO1bRvmpnHDH+3C1raA5gwm8E4N3F4I89UBdAYebupLj/3yr3zoAe6a11rlBkJNwOKt1+tvzC0ufbc76p2I0JXtPzILjToxJB7mkz186yI8clHA1v99IuYiCkyomAHYlSrsDF1MmgJYathM/Si5UCYyBget7CdEAeWbLG7GMSUahglmy8gl53nEbuK2jbFy8HirXkzE4emxdVp+LJFgzo5fNTBBH/8VCioCqTEXJdn0cnR6DqIEJgsqDmEYD4FBQx6JjgkzIo7V+T8Vs0utmZBxbkVjuYsoIhDSUYPsPZLFzRIE6vEo5eiVad6QVadkgQlejIuE6nIYIoPFJzCiMPfdqAIPUzQaCnmbDBbGeYbv+rC26cLQjzkRdjE3IdQHrcXdfh+WFo7DyuwUnPYugBi4sNnFPNk2WZj2bNuHowemP5vMfWz3tssNREZR6RNT09P/0ZHhT9774BNw1+tugKE3gjpa6eEjC2BWLRjuXoZ9+PvlQRV8zFsqaOkkqzsYhNBrD2FluQahbMT1EyMoiDfmcwHdUNJZZFOxwoucioaWB+R377ScbJSB7USuS5/b/TWDkgVytpS8OfdFG0ozPAMrCm1asUgrIgoLP0d1qgfmk3JuYarmIWTQkvT4oiyxVffYcOKcggyKjYj+xQUVKtUbWab5LKQmIadtZop0QSgKUalKq3ESrYwq0mGgUuMlFxqri9BCOcipoqYebawXIzWaofw1InkO3Mhhe6cDu13yHKESdYo3jGrNgAMH5mH+1E3wlqWTsLGBr0NLWmoa8J3HzoBh2/37/uCrv5fiEA8dnWQgwdiDo93HoTpz00P/+hfe/vNff+BPP7i+HqwevO7I9vNPf/nGt7xyGpxmFfYv74M333YSPvOFxyDouGBUDd7lh+EQ1re6cNCbBcvBEItFeSoZhDsnrFLosKadb+UxIA9yy0ijRV5gXNcSjCDfSNMm1fjLNEoWbVoUMHL6gokxZLMMmYcyhM6TWzYDIfeYNhJjk5LlMhBQIpshNbyZETNYltWiZcaxTMZBE6K0qCKZGFDsgZKfoyi5S+148pQSUvvWctmapipFRiuUAWStGZFj6JCFYpw+9qKHuKlDTPpZackrhMCLYGsngI2uy9ASppUnOWr8uY2e5I0/dAwOz5rwnccfgZkpC84+fQUNpAIP72zCk2cvwOvvuvvi1a88+hR/lnt5Yt3QKptuSVzje/79r3z8yMHFR37jw7+/E251e7/0vl/65xuPff2fDXe2rEq9xgq3U1NNuLzVAdGYwsXnc0Nxe3sE7Z0RLK1QlcHBL6DGEHqhOIry4U+6PPWh3KyES+iaxEgMI8/OIQq1r1Tzr9BVFHkvJYU+RGXwuCaDyhOohmFon2HmOulC5MdYdSrQiWXXSTj8lHI25q3Nr28xYfpIjHXmpdaTEFrpzEjL4Gp4zS7w4CfwcWUkYUhGFLKRxOFcoJVco6wcrAzLkIlxacYYJTgskWJJdF6BogCQ1GcYcuGtVIydoA3UKQHTMIARhkwX13rQ9wVYFZpqFQxxJ842267DTdctgU2z5vhYv415ihvIm2+747/e/8UvvfnGW25bWdx37OeT7XQ06k42kEli8v3N72ASc4r+8hv8wDMvwIrd/sXX3nTsHzoQzlm+B6YdwY0nFmB9tY2ew4MmhkVNTI4GePBrV3YxBtyvKIEGipggGY7Kz4nosXkGMDRSYjhh5sOYdNgqlUmAdDgowSJlQ1hGtuANkYZHaZfeMFLPkAAhQUMBjCUo+eU7QWhHA4nLCT2LgjwBCFEA/OlNu8l9kdKyNBTaIVLf+BRaWGqNUgUL4mII6D0SqREuyLSfAapnARziqVyIfg4jZVjqnvxOvQsZKcNTnFcqlwpZX4R+D+Ncg401UO0VFc4mn68KCcSSuLHTg6vbQ6iasQR5SGhfBz9rGMHJE0tw7PA0eMMR+L02rp8q7NoHfvaf/vKHfrvVrDut6c7os597YJhdqGgPDzLhxh1U9ywI57r0sU996Ux05LV3/sIB/5n/0W13wfVNOLrUgtnZFuxsrsF0pcno3qrtwNW1EezfdmFhxeJYNVKVDgEFBkGN6SPlZVIDSDHDhaEGi4yU/UNH7BoJsE9kRqCD9ZLX5/TLRfmilTr7Y7K4DRU16zPlGitknrVXhRbiGuOqoA+cTBj2klCgTC1XvTVK0LE5PUApNEeUhXJjqN0SeiQhNG+sI6ZVUTZfldPoeGRSNEjkBZJ8KGRoEg03SRb/jJSCrg9R4KvKHRmUFYdqUUWl5gG+zuM5JAqvhp1Ym3BppsYag9tDAS2TRHIM6AUVeM2r5mHKkXB1F/PmuSVYX2/7937+ARqEGnR7A7qnRz3cfXrP1qx1rd7tt778MehtbcOtd98JUwtvCN/7vo9+/X0/+8Zew4Em0Z4I8OHgfBVW1wQM8OQrlQrmHhJ2ex688MIGzM4ZzEfE4ogs6hPGlS0qUymRRlDa6TxRR3zANO1Fk16mNmVnKp1EMPKQEphUepWFe8Y5m1ZwddKInFa3NtgVaekIiLGoJyqrhslxAusxEIgQL65pPtZ4LJFxg0LepcPhDT2mF5ribAFaX8LHm3qQIvs7lGlKljRBwVSbHI21xvUnCxLyBhVCRfFsOP1IjCNcaibDkRihkGwCc+qGPAfi+gElzbiohzBVq8PJQ1Vo42JfDCgUrMCFrT4cv3EBbjq1BMPhEHziv5IV+bsPbt7V7fUuFS/xcPepa2IXrmkgZOmtOUy4K3by0JUL8uDPXScvfSLo98GcqcLxfXU4vzoNgx5NaJGCqAW1VgCXro5g3/IQDp+YgrBix0BEDMuIZ0uR/qYQ70wnnQizbTQQW42Y6pxQsmS7lGP8DVFpyFGCV9JLuCLpZ+ZnRtL6WTgZ4iGUrHIuhxC6OE2RiV2WZu+iZLoi/2kG5DMgWZKXlHVIMrmJ9Dw1/FgmzyEnOCutolVCS8QFYP1PoyJqV2YTgAmkJBmGkvqgmaPKxzRGbGPCTT8HMbMOSf9ZErpbPlzcQK/kWBC4PtRq01BFj7TZ7UG1Xocfue0gOLiZXtrswdALoNMbiEcfffw7+OYMWz//xB+A4UzB/oNHwXf736eBEEGcZUCjOQVVpwHdzW9Da+G13Q9/5DN/9C/efsv2ymxlrtfbgQYm6rccX4KHT29AzyXGkyrMzjRwF6jAes+EeT+CxZUpEg1lcuAKkdARiTAZAJhaYhmHSJxqRjoRVCrLMw65hvLfXwq+ScDkuaU0NNGfEHk0gCxLLQAmGMTkRH6vn4ROTFs4jlKaoZy309mjsxBzDDupnavIlWJLkOoyj83S9Qyjouhnkj9ESludcw5tklAbIEs9HfM4E0mhy2EYbUBuO4ALF7Zh5A3A8ylXp8jCgr43hE1MxN90+0k4uNSA1atXwaWxcNEEywz+KoVW6RZjmvAS6Cr3NhBKpAit6zg1PJhA96ur33pu9NbbDvUfnp2x+Xs7crgBnjENDz++BVt9tOxGAw3H4ATsubNdvijLK/vwtCss1cZya5SokRBoAnngbcWIO68AKVPjGMQbJtDTSG2nLE7FQSn3aUGfU5SK4pRM9Ob7K1AYt5bw8t2SKhBdL6mpZYoMGTvZwsuMZwLAUidT0IURU1uUhVkQNVufk3GQ6d+nTUQdpGioKhuFTiLSZj/CtFoW535GzL4uYzEn6pr3dvtw4eIG9H20DIKSMKMO5hxUENrcgltfdxLedOthWL14DrYx99gcGNHabnDdJ+996HwKoxo8C1deeO4lbaJ7GgglVsRoYjt18EngBBdve+07ML38Gvjat5789k73+tf+5O2Nb1ju0OnCCF5xZIlBYA98+zLI1TYcMZpQb1bAQ6/yzJk2jAYCDuxfBKfhMO0jlxlZIstUC0AH2BmpvIJgtaAxqtncApUaKUIZG++4DnpxxYscObUOcZo8CzIpJv8+jUTokFlVpk0fMrXGSz7kFBOxwvIaByZLpqBgDFZf3AGEAjOm2EmdyV5kxsv8VTJmYzDSvFMwBagI9HK65AiDjUdWWLGWCNU3Vjfh6uoW9IYRI3ZDP1bR3W33YHO3B7ffegpuv+UQrF18ATa2++CK5uiKO3Xs0/f+/mpy/IP20wx1NxVt1Yv9gqxrbF5ZJUiLc9trj6KR/FD01FPPPNltL73pnT/2igdFNHB2t9fh1NEVmJqtw/0PPg9nnt+Co4fm0EiIndGG5y50YGunDwcPzMLC0izXq+NSoJ/2A4SCN/BFUlT5lMBLI18aTr+2EurSvaMZmUvmZcroJMbxUGMzG9rPpUI/MAHiUpLQagG9gMLbJZgkKbRScTSmjlbUji/s8Snrx7XXgt6UiSaDNyE/l5Vq3Uc6wj4JmaLC9UraGXG3m4miuQVgcgEn5CpXzKtL8xthEEJ7uwPbm13Y3BqCq2bRPfQgJMa0vY5JOOYnb7r9ONx0YgXOn30CIxcJvb7pvubut93xb/7Bu1ezNbEDw+56XPqngk8kXvQGJgZbj04solQcB848/QS0phZgYXGZqwLJDQ0kfel1xw/d8tN3HXx4qmmD123DseOHYAOt/cGHLsDVtQ4sTbfgwHILnKrNbpPmR2ZmarC80AIiqqvVGooYO4x3GyUEHZdqVYnXNDTyQlEuHFMQu5yYmBQXctr0k7mN9MXPZ+jFKQ0jkFPmvYaISXEKuYSaJ//yElVbARNkol+MgUC5F8mRuefZSGTaZFQsJMkwVFrb0wwDIg0yEjHCOoKY1ijyiTN3yPrl/khCvzOA7XYftghn5Xkcgo1cn5P1bq8Pg6EHB3DtvPLGFZifiuDpM2chMqswiiruez/20Cyo0Vn2HJ0z6YnZGN2sXT7PkdDKgcOYpA+umbV+zwZCr5hevjn5hbLu/e/5x3een5qqQ2/zEhw+dgiq1SY8hon7k8+tQ7szhEN4UvMLNUycbAjQ+oURwGyjDnNzM+h1mjDVqmNcaYHCceTw04ZhahN68nvIyAvyBtnUyVjrTb7INxfiRZRnJ/VD9rKXFA9VlvXrvqKIpR9HMb8U2VNd/iAJnRRZk2oTRjkvEaONQ5VYG+wNCHnMTT/mITDj/hf5BapKUUhFcHV6LWtzxKXe0cCDDoZLnV4Hupg/7HZ8JiykEDsI41xk4A6hvdmH5fkWnDg2D/sP16G/tQ3b+NrN7iCqVqRYbUcrH/ndx9Yy4zidO8PvxUAs+D5unfVHYWqJPQnFSC9stLtv3OwMP3r94f2n2h3f2lx9Hl594hhcjyf0CBrJ2bO7sHZmAw3BgoX5GfYoOz0PusOrYK3XYAoT+6lmlfOWeqMKjVqd1am4RwIxYRhfbvIomuDO+Jz0+Oyd1BoghXnDPWpKco/wM4OJp+jTCfQ4cuKXIAuEc8pEVTIuxjrmQmuNR1q9aTzkK/Vak8VBMvSAOpdk7DZWlE202hNJ5VgpLLZNWvQWPlbh/IEMi+T6OJeQVKb3mEIhRmVT5cnHhYnfeXcH+sMRDDHSGAzRODou34MwYgmOQHqYa5i4sY6gP3JhZWUZbn/tATh8xOa5o4vPX4ZhYKBH8Xx37pZTv/rBj53Tz6xoHN9zOvj9eJBYwrcNS4fuTB9977vecuoz959e/Ct3HP/KHHoMGQwrVby4y/sWoesJeOaFLbhytQvrGzusoLswMw3TUzYDzqgCZ5JYjxlrsju1GmuM1NDy7Sqw+hD97GCiZWICFysRAQumpIg3Na8Q5UIHoXWLE1a+MC1/ygSXpJDNUnt9DnqRdor1fgSMQfjzXFmiFLOYEy6QpYjGzIDH5juS/CnKdctLNeRLSL5TyA9kEnjES8uSebTYWSk2Fr3kypNQVUc0BlNRHtGuHlEHPCKaNi8OmQKi/xzGHXJcKrh0eVYj8gNMrodo8zauByKOdqHX7UMXQykvpN2V4CQEVamwilkbE+8eepVavQWLSzXYN+/A0YMLeGwu7KCnCa067HS81ZmTd/7td73rXz1YqH/DYPdxPMTK2NX4gYdYiYG4gz5U6zOweOgO/c9pAuXge/7JW+/3+ztLiwst0wqHMIPhVCDqcGGtC5fXe7C1MUS3OgCrImEajWFm1kZjqMTsFDQsFdo8uEPyvpVKxJ16GuklsR/LsfFnhxtI3HU3rdjIrHhon3QfDMOOHyfBeTPWW5SpYKYyrkikcPCAaJAYkWooPJAyPhkq47B4BiNFF2cjPiXMJfr43nh9KZ9YJx5Bid+kYUukiN9ERv6WDCCJjMU2+bt0OEyhj+PPj3LeJG7EKR1yrorYPAUqWBCJFcZ5sZqmy2X3wCehyxH+YQV3eMwTyBDcAHd4FxNmCxc5eo4gYEyU52ESHXjMk0thdOBLcCmxjmIGRNs0mKYnUpuUKwm27oPbG8EgFKwPuLjYgIXZGizNNWHfQhXftwfdvg99rwIbG1vDUe3IO00RPfK+X/+dZ/OG8ZT6rqKXzUAseBlvO2vfhNnl25JvvIP3p37909+8/m2vP3TXMDA/VTdGdbO1aEbuDhxGQ7jh4BHYRjd7+UoHtrtD3BVcOLeGuwo+X8M8pVGz2XM0qyaQjomwiClvBDHZd8Q6peTeiaGeuLwpT4lM0naP4rFf2g0xzyE6/AoaEikNSWaut5jYODJG/Doi747n2WPkMM05EwMSzaNIJo2OmOLSwAVCVRCpJCLikrCdTh8aCYsLS3oJTcouCZ0irRStdczTVIKWphXXeHBxMa+s0oyMuPoSKDi6kcFARAxTV1tajMDl7rOZhoH0dEh8zIR/IoHM0EvITWNl1yju4oWkAUu8tnRNQ4J7mFxN8tEDcE+KDQQXfYQJc2ByE8/HDYWMJOJpvihWjY2SyhYZhGTtcXqaJMT7fQ9cj2b+8HHMK1x8ojWzD1aOzkOjIaBVRY+xjFECGueg58LGLkA/ci61B87Xr673f+Mjn3jwWwAPhmPg2hz5yMt3e1kNhHa0y2e/DAeu++H0sW30iZ/+YptIuVZOHp677kffeviW6XDwgUOHZg2vO5gKvA4cWgzgxP4mus552NwOYXdzCEP0vdtdNJZoAFuYiPX7A8blSNJmxwXmGFVc9D5ufriYrSpU0ds57EEscKnhTuOihgsWLRQKH3BR0CL0hc8D/2YQGwh5jc5OAANvwMUBYuOLcBHQTLNjOwyUo45uxazhvcLkFKwlSWzj+DVboZlhjZgPrMLHQEZlJJOUvFuTkqrJgR2XtXnHjitAzCHGcT2auRnx4qFFzijYKGIvSOEU8Y8R0TIdAC1mUvXCQBRs/PuAdMZJZCgko/bikimVRXER44VlQCCVR4k/mRY5cdSy0BFB4PH8SLePRJJwFbO3XpxrgFN38LV4DSt0XA08ng5zoRHuhnlv6VgwbHK9CMNnDwz0GjaeO0bS+FlKStyICTwC/Js6h8tNjDaAlZxIr7zZsmF+rgUNPA4ypsFoxK0ATzrPedL+5mY7+MP3f/h/fwXfarMYSjGeqn2a+3V/VreXNcSqOBb0u92U8r+3exWuu/ntY0b5quPzKyM/WvjRv/TG2w81+r9q1xpmAxN0KxzVcFvhnZQcsB8ELCEwxPNvt/GzBj70PKpo4JfpxxQwBGALI4tVg1w3YFbHGMZAbtbDPdVh/XYDFxzlN8AD/yE4uBjZ21j4XKWJrj7CHcxkw7S4GWfguY3A4PDNRq9GMbfLi6pZtaBqU9MJjQYNoTsa8sQl6TbSdu/jou73Ii4pRLgTUkjCSkdkTBT2oeEFI9ptXajWSGZCcDhSRSMkgyGVVvaKFN4YxHvsgSOq8S6Ox1+rxIvUpQ2D5FpZys7Cf/xYDEYETMZMZVE6lSlc6KA070lr0iXVYuo/URhEKFrCxEmSP47wvOp4vmjk6Im2trfZi5DniMClAhVvAAwexPeiTck24mGl2lQdc0PycmgoVqwuVndimQFa11U74Gvm0GZmA/MVmDaGdvh7D42i75pbXuQ86Ufi8fWd3n0f+G/3PQQxV1Xp6h/sPIobYy1FCme51Z/jEKvs9uxD/xNOvv4d+kPBE89vXcR/L37wNz9HWnC/87bbjr764i54d97xyruWZqb3QTi86dy5K2++4dhCe7op5mqWD815PO9pwbBnktqixmNA4U1EO6mNixANCRcqxcHE00rGQ9GIJ2OIfIz/wZ3XByaXoN2cm7vDAUxP10HaFitjzeNjNi7AABde5NfAruHuaVdwd6swcnQNE8hG1cEQMA7VDEdCzbPxojfwO7EZW+ZiojoYCXBDOg4KFxxcDCYak+AAKkQjGmDcbTkNMDF8HBFmDo+n7pDaEe7AwQh3ZY9juECSQWCeZcc6GQ7pQLoe49oqeGxDPFdvGHDXmTYWEpGhnMEHn0OshdYcNJrED9WAVt3C5/vQqC/CTm8IGzsjrigRjo/kAIgcuurYXLvooteeajagWq2it8DjNpt4XdBDWA6HfHgJ8BpY7B1sNIRWkyCoIW9cNEFqEmzd5G0OjdJiyJIgksFKM3JD40JvFF7pdcONgQuXttviq7/23z/7NepBK9zUxAGNJ776EZhbOQQ/qJv1g/iQF779aXbrtKeevPWn9afI9He+8M3zf0K/PHn6PA1nzeC9ro6t8lM/ceerl2abr2rWzIXDB2Zr++ampt1ud7/Xby8Phu6SF/rTBi4SYtWrVgPc0UwFXSdC7iqjNChZp8ejuPiIiX6VxBnZQ1652oFzF9uwszlgnI+g8guNbuLrmrgKDu2f56pKDUMAGh/2+iF00EtSmYZp99E7TDdsWF6YQk8awGjYha12bKgmLbiKBQtLDdaVJyJ9EiXaWN3FZWNwQhq6scwxJdShj54NvVgF861edwS7uIhpanNhXwsITE16FxTMD9GTkmHYPoVOBoaIfZhDA7CIE4DLppIR1VX6fFL/QsNZXd+BDaCyaRtmWzVA5wjraKR0RQwau6bQEz9guulBe9iHGp77na9bwuMNOKegsNHkaj5dR4fHFoDzFAKd4jXHsNEQjmc3WxuWJXfsamPLqjTWnbq55lRreJ9Z+/LXnpCf/fw3Nh957LkXKPpWeWp7L4Og2+N/8mEuujCGC36wtz/zECvCHYp2ncRADKEmzXD9n3rD33spXayGuq+d+dY90Nl+Ht+/bY8G4aHRsH9se3Nt0fPCWUzLFzC3mA9DY84MoI7LqRqFsomhSDUIw2oojbpJm7Ipa7hjtkIpKpu7JMuF52NLcIcu5zQ0LRn6BoYnFW5SkbuhxT4a4T5pBuyhJO66vvRYLGi2VeWEm0grQrTKwCNlLoyr8Ro2ari40BP0XSI4i7jqQ6GHo0IQTll9iv1Dnq8h3UgSR+UaGoYrdkVCHV2Ii4uV4Bakz0LJNC5IakNzFSji0EvE7Ccy9pCk72dbMQC0Mxihp4m4wlQjSiZWX0KLNdGLeBZpUVJw1686Vq9Wb7ZnZ6qDVg2zisj28BjdSBgjvC4jNMK2YRkdw3R66Gk76Bl3hVnZsg1xGY3orDDNTZNCPaeFx475hh33UcKoAq+88x/tx6NZB6XgtNftsa98MB6aI+Nj+tAE5GiyB5lZ2P//R4i11+25hz8Z9yU4HPI4oT31hp+Z1AbvqTu4/S7Gzi45IA/D3rPSsc9WcDcmZLBtUFjU4AtpUOyPOUDg1nARdLmSIz0HFwOm8XLkBL5VtczgUKvpnaxXPFNgknLwFcs1bzCqrl3uV2TFq7reqLYyO+VYNcvu7wyc+XpAXJJVYdYdIQMH4/WaH8mqMEIHF6Y5x9yXGAMKXJ0hLiWqyVaIVco3a7Zp1BZrBkYxgr4kAiPh3+BdcvHZp+KAdAwhKrgeJQ2ShhguYZowikaeH5pm4DenLIy6TFznuKKDfuD6YTgza4VB5IWe5/uVijkKonCIW//AHDUGoiJHgRkNW5bbly17eOTU8eFgsNWP2v7IMM3h7OJyZ+PyYHdtJ9ixRLszt9wcXH/9q4NB2IYOeqLp+j7c+NSkv1Fj/0H5DR0/rUfa0ZkthcycxG1oahC/Sybr4MZ6pCiReJ7oyqS18MiX388VPAc/I6DCwp+T2/8TYADzPom+5ZB52gAAAABJRU5ErkJggg==")';
							setTimeout(function(){
								element.style.backgroundImage = '';
								element.className += ' menu_selected';
							},500);
						},50);
					},50);
				},100);
			},100);
			
			setTimeout(function(){
				element.style.animationName = "";
				element.style.animationDuration = "";
				element.style.webkitAnimationName = "";	
				element.style.webkitAnimationDuration = "";	
			},1000);

			//Start the elevator
			window.pageContent.startElevator();
			window.pageLoaded = false;


			// Locate page
			window.location.hash = "#"+element.innerHTML.toLowerCase();
			window.locate();
			this.actualPos = element.innerHTML;
		}	
	}
}


//////////////////////////////////////////
//		%CROSSBROWSING FUNCTIONS		//
//////////////////////////////////////////
function addEvent(elemento,evento,funcion,bubbling){

	if (elemento.attachEvent){
    	elemento.attachEvent('on'+evento,funcion);
		//window.event.cancelBubble = !bubbling;
    	return true;
  	}else  if (elemento.addEventListener){
    	elemento.addEventListener(evento,funcion,bubbling);
      	return true;
    }else{
      return false;
	}
}
function trigger(e){
	if ( window.event && window.event.srcElement){
		return window.event.srcElement;
	}else if ( e.target ) {
		return e.target;
	}
}
function mouseCoord(e){
	var coordm={x:0,y:0};
	
  if (window.event)
  {
	 
 	if ( e.targetTouches && e.targetTouches.length === 1 ) {
  		var touch = event.targetTouches[0];
		coordm.x=touch.pageX;
		coordm.y=touch.pageY; 
		
	}else if(document.documentElement){
    	coordm.x=window.event.clientX + document.documentElement.scrollLeft
      									+ document.body.scrollLeft;
    	coordm.y=window.event.clientY + document.documentElement.scrollTop
      									+ document.body.scrollTop;
	}else{
		coordm.x=event.clientX + window.scrollX;
      	coordm.y=event.clientY + window.scrollY; 
	}
  }else if( e.clientX ){ 
	coordm.x=e.clientX;
	coordm.y=e.clientY;
  }
	
	return coordm;
}
function rightBtn(e){
	var val = false;
	if (!e) 
		var e = window.event;
	if (e.which) 
		val = (e.which == 3);
	else if (e.button) 
		val = (e.button == 2);
	return val;
}
function screenDim(){
	var height = ( window.screen.height || window.screen.availHeight );
	var width = ( window.screen.width || window.screen.availWidth );
	return {height:height,width:width};
}
function documentDim(){
	var height = ( document.body.offsetHeight ||document.documentElement.offsetHeight );
	var width = (  document.body.offsetWidth || document.documentElement.offsetWidth );
	return {height:height,width:width};	
}
function viewPortDim(){
	var height = ( window.innerHeight || window.document.documentElement.clientHeight );
	var width = (  window.innerWidth || window.document.documentElement.clientWidth );
	return {height:height,width:width};		
}
function scrollBarOffset(){
	var y = ( window.pageYOffset || document.documentElement.scrollTop );
	var x = ( window.pageXOffset || document.documentElement.scrollLeft );
	return {x:x,y:y};		
}
function getChildNodeIndex(node){
	var i=0;
	while ( node = node.previousSibling ){
		i++;
	}
	return i;
}
function findPos(obj) {
      var curleft = curtop = 0;
      if (obj && obj.offsetParent) {
        do {
            curleft += obj.offsetLeft;
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
      }
      return {left:curleft,top:curtop};
  }

// RequestAnimFrame: a browser API for getting smooth animations
window.cancelRequestAnimFrame = ( function() {
    return window.cancelAnimationFrame          ||
        window.webkitCancelRequestAnimationFrame    ||
        window.mozCancelRequestAnimationFrame       ||
        window.oCancelRequestAnimationFrame     ||
        window.msCancelRequestAnimationFrame        ||
        clearTimeout
} )();
window.requestAnimFrame = (function (){
 
	var originalWebkitMethod = undefined,
		callback = undefined,
		geckoVersion = 0,
		userAgent = navigator.userAgent,
		index = 0,
		self = this;

		//	Code for Chrome Bug, where the time 
		//	is not passed into the animation function.
		
		if ( window.webkitRequestAnimationFrame ){
			
			//Defining the wrapper
			wrapper = function (time){
				if (time === undefined ){
					time = +new Date();		
				}
				self.callback(time);
			
			};
			
			//Make the switch
			originalWebkitMethod = window.wevkitRequestAnimationFrame;
			
			window.webkitRequestAnimationFrame = 
			function ( callback, element ){
				self.callback = callback;
				
				// Browsers calls wrapper; wrapper calls callback
				
				originalWebkitMethod(wrapper, element);				
				
			}
		}
		
		// Code to fix the Gecko 2.0 (Firefox 4.0) bug, where the fps are restricted to 30-40.
		if ( window.mozRequestAnimationFrame ){
			index = userAgent.indexOf('rv:');
			
			if ( userAgent.indexOf('Gecko') != -1 ){
				geckoVersion = userAgent.substr(index + 3,3);
				
				if ( geckoVersion === '2.0' ){
					window.mozRequestAnimationFrame = undefined;	
				}
			}
				
		}
		
	return window.requestAnimationFrame       || 
		window.webkitRequestAnimationFrame || 
		window.mozRequestAnimationFrame    || 
		window.oRequestAnimationFrame      || 
		window.msRequestAnimationFrame     || 
		function(callback,element){// Element used in Chrome. Assumed as visible in otherwise.
			var self = this,
			start,
			finish;
			
			window.setTimeout( function (){
								
			  start = +new Date();
			  callback(start);
			  finish = +new Date();
			  
			  self.timeout = 1000/60 - (finish - start);
			  
			  }, self.timeout);
		};
})();
	
//////////////////////////
/*     %Image Manager 	*/
//////////////////////////
function ImageManager(){
	this.self;
	this.successCount = 0;	//Success image counter
	this.errorCount = 0;	//Error image counter
	this.ImageCollection = [];	//Loaded images list.
	this.ImageList = [];	// To load images list (id, path).

	this.isDone = function(){
		return 	(this.ImageList.length == (this.successCount + this.errorCount));
	}
	
	this.preload = function(callback){	
		var ImageManager = this;	
		if(this.ImageList.length === 0){
			callback();	
		}
		//For every image into list
		for(var i=0; i<this.ImageList.length;i++){
	
			var img = new Image(), 
				id = this.ImageList[i].id,	
				path = this.ImageList[i].path;
				
				// SUCCESS
				addEvent(img,'load',function(){
	
					ImageManager.successCount +=1;
					if(ImageManager.isDone()){
						callback();	
					}
				},false);
				
				// ERROR
				addEvent(img,'error',function(){
					ImageManager.errorCount +=1;
					console.info("ERROR al cargar imagen: "+this.ImageList[i].id);
					if (ImageManager.isDone()){
						callback();	
					}
				},false);
				
				img.src = path;
				this.ImageCollection[i] = {id:id,image:img};
		}
	}
}
ImageManager.prototype.addImage = function(id,path){
	this.ImageList.push({id:id,path:path});
}
ImageManager.prototype.resetQueue = function (){
	this.ImageCollection = [];
	this.ImageList = [];
	this.successCount = 0;
	this.errorCount = 0;	
}
ImageManager.prototype.getImage = function(id){
	return this.ImageCollection[id];
}
ImageManager.prototype.getProgress = function(){
	return ((this.successCount + this.errorCount)*100/this.ImageList.length);	
}


//////////////////////////////
//							//
//		%INITIALIZATION 	//
//							//
//////////////////////////////

var Config = function(){
	self = this;
	var iconConfig = {image:new Image(),ang:0,inc:0.01,shadow:10,alpha:1};
	var iconAudio = {image:new Image(),shadow:0};
	var iconEar = {image:new Image()};
	var canvas = document.getElementById('config');
	var ctx = canvas.getContext('2d');
	var shadowColor = '#99F';
	var backgrdColor = {r:255,g:255,b:255,a:0};
	var mouse = {over:false,cliked:false,oX:0,oY:0};
	var info = {active:true,color:'#FFF',shadow:'#99F',mg:0,messages:['Show me info messages so','i can understand what is this','Keep the annoying messages','out of my sight!']};
	var volume = {value:0.8,perc:'80%',string:'))))))))',color:'#FFF',shadow:'#99F'};

	iconConfig.image.src = 'images/config/icon_config.png';
	iconAudio.image.src = 'images/config/icon_speaker.png';
	iconEar.image.src = 'images/config/icon_ear.png';

	this.getCanvas = function(){
		return canvas;
	}

	this.update = function(){
		
		//Config icon
		var pIconConfig = iconConfig;
		pIconConfig.ang = ( pIconConfig.ang > 0.79) ? 0 : pIconConfig.ang+pIconConfig.inc;

		//MouseOver reaction
		if ( mouse.over && backgrdColor.a < 0.35){
			backgrdColor.a += 0.005;
		}else if ( !mouse.over && backgrdColor.a > 0 ){
			backgrdColor.a -= 0.005;
		}else if ( mouse.over && backgrdColor.a >= 0.1){

		}

	}

	this.draw = function(){
		var pIconConfig = iconConfig;
		var pIconAudio = iconAudio;
		var pIconEar = iconEar;
		var pBgrdClr = backgrdColor;
		var pInfo = info;

		ctx.clearRect(0,0,canvas.width,canvas.height);

		self.update();

		// Background
		ctx.save()
			ctx.fillStyle = 'rgba('+pBgrdClr.r+','+pBgrdClr.g+','+pBgrdClr.b+','+pBgrdClr.a+')';
			ctx.shadowColor = '#FFF';
			ctx.shadowBlur = 20;
			ctx.shadowOffsetX = -10;
			ctx.shadowOffsetY = -10;
			ctx.fillRect(27.5,27.5,canvas.width,canvas.height);
		ctx.restore();
		
		// Config ICON
		if ( pIconConfig.image.complete ){
			ctx.save();

				ctx.shadowBlur = pIconConfig.shadow;

				ctx.save();
					ctx.shadowColor = '#99F';
			    	ctx.globalAlpha = 0.8;
					window.rotateFrom (ctx,pIconConfig.ang,27.5,27.5);
					ctx.drawImage(pIconConfig.image,10,10,35,35);
				ctx.restore();
				if ( mouse.over && backgrdColor.a >= 0.15){
					
					// VOLUME Button
					ctx.shadowColor = volume.shadow;
					ctx.drawImage(pIconAudio.image,50,50,40,40);
					ctx.drawImage(pIconEar.image,230,50,25,40);

					//var grad = ctx.createRadialGradient(90,200,5,100,200,140);
					var grad = ctx.createRadialGradient(90,70,5,90,70,140);
					grad.addColorStop(0,"rgba(255,255,255,0.8)");
					grad.addColorStop(0.3,"rgba(100,100,255,0.7)");
					grad.addColorStop(1,"rgba(255,100,255,0.5)");
					ctx.fillStyle = grad;
					ctx.shadowColor = grad;
					ctx.shadowBlur = 10;
					ctx.shadowOffsetX = 0;
					ctx.shadowOffsetY = 0;
					ctx.font=" bolder 40px Arial";
					ctx.fillText(volume.string,90,80);
					ctx.font=" bolder 25px Arial";
					ctx.fillStyle = "#FFF";
					ctx.fillText(volume.perc,120,80);

					// INFO Button
					ctx.shadowColor = pInfo.shadow;
					ctx.fillStyle = pInfo.color;
					ctx.font=" bolder 40px Arial";
					ctx.fillText("!",50,140);
					ctx.font="bold 14px Arial";
					ctx.fillText(pInfo.messages[pInfo.mg],80,122);
					ctx.fillText(pInfo.messages[pInfo.mg+1],80,137);
				}
			ctx.restore();	
		}

	}
	this.infoToggle = function(){
		if ( info.active ){
			info.color = '#000';
			info.shadow = '#F99';
			info.mg=2;
		}else{
			info.color = '#FFF';
			info.shadow = '#00F';
			info.mg=0;
		}
		info.active = !info.active;
	}

	this.isOnAudio = function(x,y){
		ctxBackg.beginPath();	
		ctxBackg.rect(50,50,255,40);
		return ctxBackg.isPointInPath(x,y);
	}
	this.isOnInfo = function(x,y){
		ctxBackg.beginPath();	
		ctxBackg.rect(40,120,255,40);
		return ctxBackg.isPointInPath(x,y);
	}
	this.onClick = function(e){
		var absCoord = mouseCoord(e);
		var coord = coordIntoCanvas(canvas,absCoord.x,absCoord.y);
		if ( self.isOnAudio(coord.x,coord.y) ){
			mouse.oX = coord.x;
			mouse.oY = coord.y;
			mouse.clicked = true;
		}
		if ( self.isOnInfo(coord.x,coord.y) ){
			self.infoToggle();
		}
	}
	this.changeVolume = function(delta){
		if ( volume.value < 1 && delta > 0 ){
			volume.value += 0.1;
			volume.shadow = '#99F';
			volume.string +=')';
		}

		if ( volume.value > 0 && delta < 0 ){
			volume.value -= 0.1;
			volume.shadow = '#99F';
			volume.string = volume.string.substring(0,volume.string.length-1);
		}
		if ( volume.value < 0.1 ){
			volume.value = 0;
			volume.shadow = '#F00';
		}
		if ( volume.value > 0.9 )
			volume.value = 1;

		var percentage =  Math.round(volume.value*100);
		if ( percentage === 0 )
			volume.perc = 'Muted';
		else
			volume.perc = percentage+'%';
	}

	this.getMainVolume = function(){
		return volume.value;
	}

	this.onMove = function(e){
		if ( mouse.clicked){
			var absCoord = mouseCoord(e);
			var coord = coordIntoCanvas(canvas,absCoord.x,absCoord.y);
			var mX = coord.x - mouse.oX;

			if ( mX > 12){
				mouse.oX = coord.x;
				mouse.oY = coord.y;
				self.changeVolume(0.1);
			}

			if ( mX < -12){
				mouse.oX = coord.x;
				mouse.oY = coord.y;
				self.changeVolume(-0.1);
			}
		}
	}
	this.onScroll = function(e){
		var value = e.detail; 
		if ( value < 0)
			send = 0.1;
		else
			send = -0.1;
		self.changeVolume(send);
	}
	this.onMouseWheel = function(e){
		var value = e.wheelDelta;
		if ( value > 0)
			send = 0.1;
		else
			send = -0.1;
		self.changeVolume(send);
	}
	this.onUp = function(){
		if ( mouse.clicked ){
			mouse.clicked = false;
		}
	}
	this.mouseOver = function(e){
		mouse.over = true;
	}
	this.mouseOut = function(e){
		mouse.over = false;
		mouse.clicked = false;
		mouse.oX = 0;
		mouse.oY = 0;
	}

}


function initMain(){

	//Audio API Init
	try{
		window.AudioContext = window.AudioContext || widnow.webkitAudioContext;
		window.audioCtx = new AudioContext();
	}catch(erError){
		console.info('Web audio API is not supported in this browser');
	}

	window.config = new Config;
	var configCanvas = window.config.getCanvas();
	addEvent(configCanvas,'mouseover',window.config.mouseOver,false);
	addEvent(configCanvas,'mouseout',window.config.mouseOut,false);
	addEvent(configCanvas,'mousemove',window.config.onMove,false);
	addEvent(configCanvas,'mousedown',window.config.onClick,false);
	addEvent(configCanvas,'mouseup',window.config.onUp,false);
	addEvent(configCanvas,'mousewheel',window.config.onMouseWheel,false);
	addEvent(configCanvas,'DOMMouseScroll',window.config.onScroll,false);

	window.pageContent = new PageContent;
	window.onresize = function(){
		window.screenDim = window.viewPortDim();
		pageContent.sizeElements(window.screenDim.height);

		cancelRequestAnimFrame(window.requestedFrame);
		canvasResize(window.canvasBackg);
		canvasResize(window.canvasOffset);
								
		window.animationCenter();
	};

	window.pageContent.sizeElements(window.screenDim.height);
	window.pageContent.element.appendChild(window.canvasBackg);

	canvasRemoveSmooth(ctxBackg);
	canvasRemoveSmooth(ctxOffset);
	canvasResize(canvasBackg);
	canvasResize(canvasOffset);

	window.locate();

}

addEvent(window,'load',initMain,false);


