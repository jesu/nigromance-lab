// JavaScript Document

( function homeFunctions(){

	function getImages(){
		window.nextBackgImg = window.imageLoader.getImage(0).image;
	}

	// Draw Home Function
	function drawHome(){
		//	BACKGROUND
		window.ctxBackg.clearRect(0,0,window.canvasBackg.width,window.canvasBackg.height);

		window.ctxBackg.drawImage(window.backgImg.image,window.backgImg.X+0.5,0,window.backgImg.W,window.backgImg.H);
		
		if ( pageContent.speed !== 0 ){
			document.getElementById("proyectors").style.bottom = ((-1)*pageContent.pageOffset)+"px";
		}
	}

	function turnOnCards(){
		var card_01 = document.getElementById("card_01");

		card_01.style.display = "block";
		setTimeout(function(){card_01.style.display = "none";},300);
		setTimeout(function(){card_01.style.display = "block";},350);
		setTimeout(function(){card_01.style.display = "none";},450);
		setTimeout(function(){card_01.style.display = "block";},530);
		setTimeout(function(){card_01.style.display = "none";},850);
		setTimeout(function(){card_01.style.display = "block";},1000);
	}


	function initNavPageHome(){
		turnOnCards();
	}

	window.imageLoader.resetQueue();
	window.imageLoader.addImage("background","images/home/home_fondo.jpg");
	window.imageLoader.addImage("shield_HTML","images/home/shield_HTML5.png");
	window.imageLoader.addImage("shield_JS","images/home/shield_CSS3.png");
	window.imageLoader.addImage("shield_CSS","images/home/shield_JS.png");
	window.imageLoader.addImage("shield_REQUIRED","images/home/shield_REQUIRED.png");
	window.imageLoader.addImage("shield_HANDLERBARS","images/home/shield_HANDLERBARS.png");


	window.imageLoader.preload(function(){

		getImages();
		window.nextDrawFuncPointer = drawHome;
		window.pageLoaded = true;
		window.initNavPage = initNavPageHome;
		window.pageHTML = homeTemplate();

		if ( !window.isAnimated ){
			
			window.isAnimated=true;
			window.drawFuncPointer = window.nextDrawFuncPointer;
			window.backgImg.image = window.nextBackgImg;
			window.pageContent.sizeElements(window.screenDim.height);

			pageContent.pageElements.innerHTML =window.pageHTML;

			window.animationCenter();

			initNavPageHome();
		}	

		if ( !document.getElementById("menu_container") ){
			addMenu();	
		}	
	});
		
}
)();